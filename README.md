# AUA - ASM Utils and Analysis

AUA is a lightweight library that provides basic and advanced functionality for byte code analyses that are based on the [ASM byte code manipulation and analysis framework](http://asm.ow2.org/).

### Utils and Basic Transformations

In its current form it provides the following useful utilities and byte code transformations:

1. an algorithm to produce the control flow graph of a method out of basic blocks,
2. a graphical representation of the control flow graph along with the byte code using graphviz's dot language,
3. a byte code transformation into SSA form and
4. local variable information retrieval for byte code that was compiled without debug information flags turned on. 

### Analysis Algorithms 

In addition, the library provides the following byte code analysis algorithms:

5. an escape analysis algorithm for functions for references from the objects scope escaping via the returned reference or global references,
6. a reference analysis algorithm to compute the reference set backwards starting from a given root reference (prototypical).

Additionally, the project provides a rich set of test cases that marks a starting point to better evaluate byte code analysis algorithms, like the above, for correctness and precision.

### The Knowledge Base

Most analysis algorithms become complex beasts very quickly because of programming concepts such as recursion/recursive data structures or programming concepts such as abstract and native functions, interfaces or reflection.
However, a programmer can instantaneously answer questions like: Does this function return state?
This is due to the knowledge and experience of the programmer with these concepts. We would like to share that knowledge with the compiler and thereby make it smarter. Our approach is to introduce a knowledge base that can be used by the analysis algorithms and can be enriched with this information.
An entry in the knowledge base has the following [edn](https://github.com/edn-format/edn) format:
```
#!clojure
; schema: {interface {function #{annotations}}}
#aua/interfaces {java.util.Collection  
                 {remove #{aua.analysis.qual.Untainted}
                  add #{aua.analysis.qual.SideEffect}
                  get #{aua.analysis.qual.Tainted }}}
; schema: {class {field #{annotations}}}                    
#aua/fields {java.lang.System {out #{aua.analysis.qual.Untainted} 
                               err #{aua.analysis.qual.Untainted}}
             java.util.HashMap {EMPTY_TABLE #{aua.analysis.qual.Untainted}}}
``` 
Note that the knowledge base is quite powerful and inherits properties from interface or abstract functions to their concrete implementations such as in case of the ```java.util.Collection``` interface.

**Please feel free to add to this knowledge base and therewith make analysis algorithms easier and more powerful!**

Alternatively, you can just use this library and put a project specific knowledge base file into the ```META-INF/analysis``` directory of your project

## Usage

Please have a look at the different test cases.

## Development

I would be happy if people find this library interesting and devote some time to contribute and grow those ideas. Please contact me!

## Projects That Use This Library

[Ohua](https://bitbucket.org/sertel/ohua) - Ohua is an implementation of the stateful functional programming model. It mixes functional and imperative (object-oriented) programming to provide truely transparent parallel execution of programs. It uses AUA to enforce data race freedom at compile-time.

## License

Copyright © 2015 Sebastian Ertel

Distributed under the Eclipse Public License version 1.0 (see the accompanying LICENSE.txt file.)
