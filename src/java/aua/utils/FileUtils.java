/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.utils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

public class FileUtils {
  public static List<Path> loadMetaInfFilesFromClassPath(String folder, String regex) {
    List<Path> result = new ArrayList<>();
    try {
      Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources("META-INF");
      while(resources.hasMoreElements()) {
        URL url = resources.nextElement();
        String[] fsAndFile = url.toURI().toString().split("!");
        URI uri = URI.create(fsAndFile[0]);
        if(fsAndFile.length == 2) {
          try {
            // don't close the file system! we might want to read from it and we can't once it
            // is closed!
            FileSystem fs = FileSystems.getFileSystem(uri);
            result.addAll(findURLs(fs, fsAndFile[1], folder, regex));
          }
          catch(FileSystemNotFoundException n) {
            // don't close the file system! we might want to read from it and we can't once it
            // is closed!
            FileSystem fs = FileSystems.newFileSystem(uri, new HashMap<>());
            result.addAll(findURLs(fs, fsAndFile[1], folder, regex));
          }
        } else {
          // don't close the file system! we might want to read from it and we can't once it is
          // closed! (can't close default fs anyways)
          FileSystem fs = FileSystems.getDefault();
          result.addAll(findURLs(fs, uri.getPath(), folder, regex));
        }
      }
    }
    catch(IOException | URISyntaxException ioe) {
      throw new RuntimeException(ioe);
    }
    return result;
  }
  
  private static List<Path>
      findURLs(FileSystem fs, String metaInfPath, String folder, String regex) throws IOException
  {
    Path metaInfDir = fs.getPath(metaInfPath);
    List<Path> found = new ArrayList<>();
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(metaInfDir, folder)) {
      for(Path entry : stream) {
        try (DirectoryStream<Path> fileStream = Files.newDirectoryStream(entry, regex)) {
          for(Path file : fileStream)
            found.add(file);
        }
      }
    }
    return found;
  }

}
