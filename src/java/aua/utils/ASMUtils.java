/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.ParameterNode;
import org.objectweb.asm.tree.VarInsnNode;

public abstract class ASMUtils {
  
  // FIXME this needs the instruction at which the variable is supposed to be queried! the code
  // needs to check for that to find the right local variable.
  public static int translateCodeIndexToParameterIndex(int codeIndex, List<LocalVariableNode> locals) {
    for(int paramIndex = 0; paramIndex < locals.size(); paramIndex++) {
      if(locals.get(paramIndex) != null && locals.get(paramIndex).index == codeIndex) return paramIndex;
    }
    return -1;
  }
  
  @Deprecated
  public static int translateParameterIndexToCodeIndex(int parameterIndex, List<LocalVariableNode> locals) {
    return locals.get(parameterIndex).index;
  }
  
  public static List<LocalVariableNode> computeLocalVariableInfo(String descriptor, MethodNode mn) {
    return computeLocalVariableInfo(descriptor, mn, Collections.emptyList());
  }
  
  /**
   * When classes were compiled without debug information then the list of local variables will
   * not be available. This method recomputes this information from the given byte code.<br>
   * Note that there are two indices to be preserved:
   * <ul>
   * <li>the index in the array of the parameters to the functions -> location in the 'present'
   * array.
   * <li>the index in the array of local variables as used in the byte code -> stored in
   * LocalVariableNode.index.
   * </ul>
   * 
   * @param insns
   * @return
   */
  // as a matter of fact local variable slots for variables created inside the code are assigned
  // according to code regions. therefore, local variable slots may be used by another local
  // variable in a different block. hence, to identify the reference to a local variable one
  // needs the current instruction. this is primarily interesting for catch blocks and functions
  // that define multiple execution branches in the control flow.
  public static List<LocalVariableNode> computeLocalVariableInfo(String ownerDesc, MethodNode mn,
                                                                 List<LocalVariableNode> present)
  {
    AbstractInsnNode[] insns = mn.instructions.toArray();
    ArrayList<LocalVariableNode> locals = new ArrayList<>();
    
    // transfer what is already present
    for(LocalVariableNode p : present) {
      locals.add(p);
      // for(int i = locals.size(); i <= p.index; i++)
      // locals.add(null);
      // locals.set(p.index, p);
    }
    
    // create the locals that come from the function itself
    int start = nextLabelNode(0, insns);
    if(ownerDesc != null && locals.isEmpty()) {
      assert start > -1;
      locals.add(new LocalVariableNode("this", ownerDesc, null, (LabelNode) insns[start], null, locals.size()));
    }
    
    // note that even parameter names are not stored in the raw byte code format
    @SuppressWarnings("unchecked") List<ParameterNode> params = mn.parameters;
    if(params == null) {
      params = createParameterList(mn, locals.size());
      Type[] argTypes = Type.getArgumentTypes(mn.desc);
      assert params.size() == argTypes.length;
      // this is an all-or-nothing thing: either the parameters have been computed before or
      // not.
      for(int i = 0; i < argTypes.length; i++) {
        LocalVariableNode l =
            new LocalVariableNode(params.get(i).name,
                                  argTypes[i].getDescriptor(),
                                  null,
                                  (LabelNode) insns[start],
                                  null,
                                  locals.size());
        locals.add(l);
      }
    } else {
      // the params were properly computed
      assert params.size() <= locals.size();
    }
    
    List<LocalVariableNode> assignStart = new ArrayList<>();
    List<LocalVariableNode> assignStop = new ArrayList<>();
    // go forward over the byte code to find the start of the local variable usages
    for(int i = 0; i < insns.length; i++) {
      switch(insns[i].getType()) {
        case AbstractInsnNode.LABEL:
          for(LocalVariableNode lvn : assignStart)
            lvn.start = (LabelNode) insns[i];
          assignStart.clear();
          for(LocalVariableNode lvn : assignStop)
            lvn.end = (LabelNode) insns[i];
          assignStop.clear();
          break;
        case AbstractInsnNode.VAR_INSN:
          VarInsnNode varNode = (VarInsnNode) insns[i];
          String typeDesc = null;
          switch(varNode.getOpcode()) {
            case Opcodes.ILOAD:
              typeDesc = Type.getDescriptor(Integer.class);
              //$FALL-THROUGH$
            case Opcodes.LLOAD:
              typeDesc = Type.getDescriptor(Long.class);
              //$FALL-THROUGH$
            case Opcodes.FLOAD:
              typeDesc = Type.getDescriptor(Float.class);
              //$FALL-THROUGH$
            case Opcodes.DLOAD:
              typeDesc = Type.getDescriptor(Double.class);
              //$FALL-THROUGH$
            case Opcodes.ALOAD:
              // the type information is not easy to derive from the byte code. part of it is
              // lost when not compiled with debug information. not even the computed frames
              // have the concrete type information.
              typeDesc = Type.getDescriptor(Object.class);
              LocalVariableNode l = locals.get(translateCodeIndexToParameterIndex(varNode.var, locals));
              // if it is loaded then it must be defined already either as parameter or via a
              // store operation.
              assert l != null;
              if(l.start == null) assignStart.add(l);
              if(l.desc == null) l.desc = typeDesc;
              // assignStop.add(l);
              break;
            case Opcodes.ISTORE:
              typeDesc = Type.getDescriptor(Integer.class);
              //$FALL-THROUGH$
            case Opcodes.LSTORE:
              typeDesc = Type.getDescriptor(Long.class);
              //$FALL-THROUGH$
            case Opcodes.FSTORE:
              typeDesc = Type.getDescriptor(Float.class);
              //$FALL-THROUGH$
            case Opcodes.DSTORE:
              typeDesc = Type.getDescriptor(Double.class);
              //$FALL-THROUGH$
            case Opcodes.ASTORE:
              // the type information is not easy to derive from the byte code. part of it is
              // lost when not compiled with debug information. not even the computed frames
              // have the concrete type information.
              typeDesc = Type.getDescriptor(Object.class);
              
              // can be (new) local variables
              
              // the name information of internal local variables is essentially lost! so we
              // have to create names.
              if(varNode.var >= locals.size() || translateCodeIndexToParameterIndex(varNode.var, locals) == -1) {
                fillGap(locals, varNode.var);
                l = new LocalVariableNode("synthetic$var" + varNode.var, typeDesc, null, null, null, varNode.var);
                // FIXME this code below exists only because the access to the local variable
                // table has not been fixed yet.
                if(locals.get(varNode.var) != null) locals.add(l);
                else locals.set(varNode.var, l);
                assignStart.add(l);
              }
              
              l = locals.get(translateCodeIndexToParameterIndex(varNode.var, locals));
              // assignStop.add(l);
              break;
          }
          break;
        default:
          // don't care
      }
    }
    
    // seems like ASM defines the end as the scope of the local variable
    // FIXME handle branches properly!
    int end = previousLabelNode(insns.length - 1, insns);
    assert end != -1;
    // there exist situations where local variables slots are not occupied. (iterator in
    // for-loop)
    for(LocalVariableNode l : locals.stream().filter(p -> p != null).collect(Collectors.toList()))
      l.end = (LabelNode) insns[end];
    
    // verify consecutiveness and insert nodes for unused locals (might be unused input
    // parameters)
    
    return locals;
  }
  
  private static void fillGap(ArrayList<LocalVariableNode> locals, int index) {
    if(index > locals.size() - 1) {
      // this is a new variable that was not a parameter. we have to fill the gaps in between
      // for now.
      while(locals.size() - 1 < index) {
        locals.add(null);
      }
    }
  }
  
  private static List<ParameterNode> createParameterList(MethodNode mn, int j) {
    Type[] types = Type.getArgumentTypes(mn.desc);
    List<ParameterNode> params = new ArrayList<>();
    for(int i = 0; i < types.length; i++)
      params.add(new ParameterNode("synthetic$var" + (j + i), Opcodes.ACC_SYNTHETIC));
    return params;
  }
  
  private static int nextLabelNode(int start, AbstractInsnNode[] insns) {
    for(int i = start; i < insns.length; i++) {
      if(insns[i].getType() == AbstractInsnNode.LABEL) return i;
    }
    return -1;
  }
  
  private static int previousLabelNode(int start, AbstractInsnNode[] insns) {
    for(int i = start; i > -1; i--) {
      if(insns[i].getType() == AbstractInsnNode.LABEL) return i;
    }
    return -1;
  }
}
