/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

import java.io.IOException;
import java.io.PushbackReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import aua.utils.FileUtils;
import clojure.java.api.Clojure;
import clojure.lang.AFn;
import clojure.lang.EdnReader;
import clojure.lang.IFn;
import clojure.lang.IMapEntry;
import clojure.lang.IPersistentMap;
import clojure.lang.Keyword;
import clojure.lang.PersistentHashMap;
import clojure.lang.PersistentHashSet;
import clojure.lang.Symbol;

/**
 * This class should represent the first step towards making compilers learn.
 * 
 * @author sertel
 *         
 */
public class KnowledgeBase {
  public static boolean REPORT_LACK_OF_KNOWLEDGE = false;
  
  // script for doing the merge properly
  private static final String mergeScript =
      "(do (fn [a b] (merge-with (partial merge-with clojure.set/union) a b)))";
  private static IFn _merge = null;
  
  private IPersistentMap _iFaceKnowledge = PersistentHashMap.EMPTY;
  private IPersistentMap _fieldKnowledge = PersistentHashMap.EMPTY;
  
  private static KnowledgeBase _base = new KnowledgeBase();
  
  static {
    Clojure.var("clojure.core", "require").invoke(Clojure.read("clojure.set"));
    _merge = (IFn) Clojure.var("clojure.core", "eval").invoke(Clojure.read(mergeScript));
    
    _base.loadCompilerKnowledge();
  }
  
  public static KnowledgeBase getInstance() {
    return _base;
  }
  
  private KnowledgeBase() {
    // singleton
  }
  
  private void loadCompilerKnowledge() {
    try {
      List<Path> registries = FileUtils.loadMetaInfFilesFromClassPath("analysis**", "*.edn");
      for(Path reg : registries) {
        final PushbackReader r = new PushbackReader(Files.newBufferedReader(reg));
        
        // the protocol for these readers seems to be: if you return the reader then it will
        // continue to parse the file otherwise it will just stop and return the tag that it
        // just read and then you have to call EdnReader.read() again.
        AFn interfaceReader = new AFn() {
          public Object invoke(Object arg1) {
            _iFaceKnowledge = (IPersistentMap) _merge.invoke(_iFaceKnowledge, arg1);
            return r;
          }
        };
        AFn fieldReader = new AFn() {
          public Object invoke(Object arg1) {
            _fieldKnowledge = (IPersistentMap) _merge.invoke(_fieldKnowledge, arg1);
            return r;
          }
        };
        Object o =
            EdnReader.read(r,
                           PersistentHashMap.create(Keyword.intern(null, "readers"),
                                                    PersistentHashMap.create(Symbol.create("aua", "interfaces"),
                                                                             interfaceReader,
                                                                             Symbol.create("aua", "fields"),
                                                                             fieldReader),
                                                    Keyword.intern(null, "eof"),
                                                    true));
        assert (boolean) o;
      }
    }
    catch(IOException ioe) {
      throw new RuntimeException(ioe);
    }
  }
  
  public String[] getAnnotations(String cls, String methodName) {
    Symbol clsSym = Symbol.create(null, cls);
    if(_iFaceKnowledge.containsKey(clsSym)) {
      IPersistentMap functions = (IPersistentMap) _iFaceKnowledge.valAt(clsSym);
      Symbol methodSym = Symbol.create(null, methodName);
      if(functions.containsKey(methodSym)) {
        PersistentHashSet annos = (PersistentHashSet) functions.valAt(methodSym);
        String[] result = new String[annos.count()];
        int i = 0;
        for(Object anno : annos) {
          result[i++] = anno.toString();
        }
        return result;
      } else {
        return new String[0];
      }
    } else {
      return new String[0];
    }
  }
  
  public List<String> reflect(Class<?> cls, String methodName) {
    List<String> discovered = new ArrayList<>();
    for(Object iFace : _iFaceKnowledge) {
      String iFaceName = ((IMapEntry) iFace).getKey().toString();
      try {
        Class<?> iFaceCls = Class.forName(iFaceName);
        if(iFaceCls.isAssignableFrom(cls)) {
          IPersistentMap funcs = (IPersistentMap) ((IMapEntry) iFace).getValue();
          Symbol methodSym = Symbol.create(null, methodName);
          if(funcs.containsKey(methodSym)) {
            discovered.add(iFaceName);
          }
        }
      }
      catch(ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return discovered;
  }
  
  public boolean isFieldUntainted(String cls, String field) {
    Symbol clsSym = Symbol.create(null, cls);
    if(_fieldKnowledge.containsKey(clsSym)) {
      IPersistentMap fields = (IPersistentMap) _fieldKnowledge.valAt(clsSym);
      Symbol fieldSym = Symbol.create(null, field);
      if(fields.containsKey(fieldSym)) {
        return ((PersistentHashSet) fields.valAt(fieldSym)).contains(Symbol.create(null,
                                                                                   Untainted.class.getName()));
      } else {
        if(REPORT_LACK_OF_KNOWLEDGE) System.out.println("No field knowledge present: " + cls + "." + field);
        return false;
      }
    } else {
      if(REPORT_LACK_OF_KNOWLEDGE) System.out.println("No field knowledge present: " + cls + "." + field);
      return false;
    }
  }
  
  public void clear() {
    _fieldKnowledge = (IPersistentMap) _fieldKnowledge.empty();
    _iFaceKnowledge = (IPersistentMap) _iFaceKnowledge.empty();
  }
  
  public boolean isFunctionResultUntainted(String cls, String method,
                                           String methodDesc) throws ClassNotFoundException
  {
    List<String> iFacesWithMethod = reflect(Class.forName(cls), method);
    for(String iFaceWithMethod : iFacesWithMethod) {
      PersistentHashSet annos = getAnnos(iFaceWithMethod, method);
      if(annos.contains(Symbol.create(null, Untainted.class.getName()))) return true;
    }
    return false;
  }
  
  private PersistentHashSet getAnnos(String cls, String method) {
    Symbol clsSym = Symbol.create(null, cls);
    if(_iFaceKnowledge.containsKey(clsSym)) {
      IPersistentMap functions = (IPersistentMap) _iFaceKnowledge.valAt(clsSym);
      Symbol methodSym = Symbol.create(null, method);
      
      if(functions.containsKey(methodSym)) {
        return (PersistentHashSet) functions.valAt(methodSym);
      } else {
        return null;
      }
    } else return null;
  }
}
