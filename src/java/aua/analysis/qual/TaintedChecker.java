/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.MethodNode;

public class TaintedChecker {
  private Set<String> _taintedFunctions = new HashSet<>();
  private Set<String> _untaintedFunctions = new HashSet<>();
  
  public void registerTainted(String reference) {
    _taintedFunctions.add(reference);
  }
  
  public void registerUntainted(String reference) {
    _untaintedFunctions.add(reference);
  }
  
  @SuppressWarnings("unchecked")
  public Annotation getTaintedAnnotation(MethodNode method) {
    for(AnnotationNode anno : ((List<AnnotationNode>) method.visibleAnnotations)) {
      // System.out.println("Method: " + method.name + "Annotation detected: " + anno.desc);
      Type t = Type.getType(anno.desc);
      String[] qualified = t.getClassName().split("\\.");
      String simpleClassName = qualified[qualified.length - 1];
      if(simpleClassName.equals("Tainted")) return newTaintedInstance();
      else if(simpleClassName.equals("Untainted")) return newUntaintedInstance();
    }
    return null;
  }
    
  public Annotation getTaintedAnnotation(String cls, String methodName) {
    String[] annotations = KnowledgeBase.getInstance().getAnnotations(cls, methodName);
    for(String annotation : annotations) {
      String[] qualified = annotation.trim().split("\\.");
      String simpleClassName = qualified[qualified.length - 1];
      if(simpleClassName.equals("Tainted")) return newTaintedInstance();
      else if(simpleClassName.equals("Untainted")) return newUntaintedInstance();
    }
    return null;
  }
  
  public Annotation getTaintedAnnotation(Method method) {
    if(method.isAnnotationPresent(Tainted.class)) {
      return newTaintedInstance();
    } else if(method.isAnnotationPresent(Untainted.class)) {
      return newUntaintedInstance();
    } else {
      List<String> candidates = KnowledgeBase.getInstance().reflect(method.getDeclaringClass(), method.getName());
      for(String candidate : candidates) {
        String[] annotations = KnowledgeBase.getInstance().getAnnotations(candidate, method.getName());
        for(String annotation : annotations) {
          String[] qualified = annotation.trim().split("\\.");
          String simpleClassName = qualified[qualified.length - 1];
          if(simpleClassName.equals("Tainted")) return newTaintedInstance();
          else if(simpleClassName.equals("Untainted")) return newUntaintedInstance();
        }
      }
      return null;
    }
  }
  
  private Annotation newTaintedInstance() {
    return new Tainted() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return Tainted.class;
      }
    };
  }
  
  private Annotation newUntaintedInstance() {
    return new Untainted() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return Untainted.class;
      }
    };
  }
  
}
