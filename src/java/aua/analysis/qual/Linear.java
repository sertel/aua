/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

/**
 * This marks a field which is only incremented linearly. It is meant to help the static
 * analysis to understand that a (index) field used to access an array or collection never
 * reaches the same value. That means it is either always decremented or always incremented. If
 * this is the case then the analysis will not mark the value retrieved from the collection as a
 * leak.
 * 
 * @author sertel
 * 
 */
// TODO their is also a linear annotation in the checker framework. maybe we can somehow derive
// from that.
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.PARAMETER})
public @interface Linear {
  // no params
}
