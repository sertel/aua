/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

/**
 * A tainted reference represents sensitive data that is not to be leaked.<br>
 * If specified on a method, it refers to the returned result.<br>
 * By default, all fields are tainted.
 * 
 * @author sertel
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.PARAMETER, java.lang.annotation.ElementType.METHOD})
public @interface Tainted {
  
}
