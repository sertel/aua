/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

/**
 * An untainted reference is released from the state of an operator and its only reference
 * resides in local variables or on the stack. It is therefore save to leak such an object.<br>
 * If specified on a method, it refers to the returned result.<br>
 * It is possible to untaint fields manually but it is not recommended to do so!
 * 
 * @author sertel
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value = { java.lang.annotation.ElementType.PARAMETER,
                                      java.lang.annotation.ElementType.FIELD,
                                      java.lang.annotation.ElementType.LOCAL_VARIABLE,
                                      java.lang.annotation.ElementType.METHOD,
                                      java.lang.annotation.ElementType.TYPE_USE,
                                      java.lang.annotation.ElementType.TYPE_PARAMETER})
public @interface Untainted {
  // sadly, there is no way of untainting a local variable. the annotation processing even in
  // Java 8 does not store any annotation information of local variables in the binary
  // representation.
}
