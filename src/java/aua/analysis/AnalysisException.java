/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis;

public class AnalysisException extends Exception {

  public AnalysisException(Exception e) {
    super(e);
  }
}
