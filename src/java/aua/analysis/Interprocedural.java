/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.util.TraceClassVisitor;

public abstract class Interprocedural {
  
  public interface TargetExtension {
    ClassVisitor createClassVisitor(ClassVisitor target) throws IOException;
  }
  
  public static ClassNode charge(String owner) throws IOException {
    return charge(owner, new TargetExtension() {
      @Override
      public ClassVisitor createClassVisitor(ClassVisitor target) throws IOException {
        return target;
      }
    });
  }
  
  protected static ClassNode charge(String owner, TargetExtension ext) throws IOException {
    try (InputStream s = loadClassFile(owner)) {
      return loadClassFile(s, ext);
    }
  }
  
  protected static InputStream loadClassFile(String owner) {
    InputStream s = null;
    try {
      // load it such that it can be found
      Class<?> clz = Class.forName(owner);
      URL url = clz.getResource(owner.substring(owner.lastIndexOf(".") + 1) + ".class");
      s = url.openStream();
    }
    catch(Exception e) {
      throw new RuntimeException(e);
    }
    return s;
  }
  
  protected static ClassNode loadClassFile(InputStream f, TargetExtension ext) throws IOException {
    // load the class file
    ClassNode node = new ClassNode();
    ClassVisitor c = ext.createClassVisitor(node);
    ClassReader cr = new ClassReader(f);
    cr.accept(c, ClassReader.EXPAND_FRAMES);
    return node;
  }
  
  public static MethodNode
      loadMethod(String clz, String method, Class<?> returnType, Class<?>... args) throws IOException
  {
    return loadMethod(Interprocedural.charge(clz),
                      method,
                      Interprocedural.constructMethodDescriptor(returnType, args));
  }
  
  /**
   * Does so only if the method is referencing a method of the class node. Otherwise it returns
   * null and the loadMethod function must be used.
   * 
   * @param cn
   * @param mn
   * @return
   */
  protected static MethodNode loadMethodFromClassNode(ClassNode cn, MethodInsnNode mn) {
    return cn.name == mn.owner ? loadMethod(cn, mn.name, mn.desc) : null;
  }
  
  protected static MethodNode loadMethod(ClassNode cn, String methodName, String descriptor) {
    MethodNode algMethod = null;
    for(Object obj : cn.methods) {
      MethodNode methodNode = (MethodNode) obj;
      if(methodNode.name.equals(methodName)) {
        if(descriptor == null) {
          algMethod = methodNode;
          break;
        } else {
          if(descriptor.equals(methodNode.desc)) {
            algMethod = methodNode;
            break;
          } else {
            // continue
          }
        }
      }
    }
    
    return algMethod;
  }
  
  /**
   * This method tries everything that is possible to find and load this method including
   * looking up the hierarchy.
   * 
   * @param cn
   * @param mn
   * @return
   * @throws IOException
   */
  public static Object[] loadMethod(ClassNode cn, MethodInsnNode mn) throws IOException {
    MethodNode nMn = loadMethodFromClassNode(cn, mn);
    if(nMn != null) {
      return new Object[] { cn,
                           nMn };
    } else {
      ClassNode nCn = charge(Type.getObjectType(mn.owner).getClassName());
      nMn = loadMethod(nCn, mn.name, mn.desc);
      if(nMn != null) {
        // all good
      } else {
        // look up the hierarchy
        Method m = classLoadMethod(mn);
        nCn = charge(m.getDeclaringClass().getName());
        nMn = loadMethod(nCn, mn.name, mn.desc);
      }
      return new Object[] { nCn,
                           nMn };
    }
  }
  
  /**
   * 
   * @param methodInvocation
   * @return
   */
  public static Method classLoadMethod(MethodInsnNode methodInvocation) {
    try {
      Class<?> cls = Class.forName(Type.getObjectType(methodInvocation.owner).getClassName());
      Type[] paramTypes = Type.getArgumentTypes(methodInvocation.desc);
      Class<?>[] types = new Class[paramTypes.length];
      for(int i = 0; i < paramTypes.length; i++)
        types[i] = convertPrimitiveType(paramTypes[i].getClassName());
      return findMethodInHierarchy(cls, methodInvocation.name, types);
    }
    catch(ClassNotFoundException | SecurityException e) {
      throw new RuntimeException(e);
    }
  }
  
  private static Method findMethodInHierarchy(Class<?> cls, String name, Class<?>[] argTypes) {
    Class<?> owner = cls;
    Method m = null;
    // first check the class hierarchy
    while(m == null && owner != null) {
      try {
        m = owner.getDeclaredMethod(name, argTypes);
      }
      catch(NoSuchMethodException | SecurityException e) {
        owner = owner.getSuperclass();
      }
    }
    
    // ... then check the interfaces recursively.
    Class<?>[] itfs = cls.getInterfaces();
    for(int i=0; i<itfs.length && m == null; i++ ){
      m = findMethodInHierarchy(itfs[i], name, argTypes);
    }
    
    assert m != null;
    return m;
  }
  
  private static Class<?> convertPrimitiveType(String className) {
    if(className.endsWith("[]")) {
      return java.lang.reflect.Array.newInstance(getType(className.substring(0, className.length() - 2)), 0).getClass();
    } else {
      return getType(className);
    }
  }
  
  private static Class<?> getType(String className) {
    switch(className) {
      case "char":
        return Character.TYPE;
      case "int":
        return Integer.TYPE;
      case "double":
        return Double.TYPE;
      case "float":
        return Float.TYPE;
      case "short":
        return Short.TYPE;
      case "long":
        return Long.TYPE;
      case "boolean":
        return Boolean.TYPE;
      case "byte":
        return Byte.TYPE;
      default:
        try {
          return Class.forName(className);
        }
        catch(ClassNotFoundException e) {
          throw new RuntimeException(e);
        }
    }
  }
  
  protected static String constructMethodDescriptor(Class<?> returnType, Class<?>... args) {
    Type[] argTypes = new Type[args.length];
    for(int i = 0; i < args.length; i++)
      argTypes[i] = Type.getType(args[i]);
    return Type.getMethodDescriptor(Type.getType(returnType), argTypes);
  }
  
  public static Frame[] computeStackMapFrames(Analyzer analyzer, String owner, MethodNode algMethod) {
    // perform the analysis here
    try {
      analyzer.analyze(owner, algMethod);
    }
    catch(AnalyzerException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return analyzer.getFrames();
  }
  
  public static void printInstructions(File f) throws FileNotFoundException, IOException {
    printInstructions(new FileInputStream(f));
  }
  
  public static void printInstructions(InputStream f) throws FileNotFoundException, IOException {
    ClassReader cr = new ClassReader(f);
    cr.accept(new TraceClassVisitor(new PrintWriter(System.out)), ClassReader.EXPAND_FRAMES);
  }
  
  public static void printInstructions(String className) throws FileNotFoundException, IOException {
    printInstructions(loadClassFile(className));
  }
  
}
