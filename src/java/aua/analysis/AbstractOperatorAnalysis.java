/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;
import org.objectweb.asm.util.TraceClassVisitor;

public abstract class AbstractOperatorAnalysis {
  public static final Set<String> NUMBER_TYPES = new HashSet<>();
  static {
    NUMBER_TYPES.add("long");
    NUMBER_TYPES.add("java.lang.Long");
    NUMBER_TYPES.add("int");
    NUMBER_TYPES.add("java.lang.Integer");
    NUMBER_TYPES.add("float");
    NUMBER_TYPES.add("java.lang.Float");
    NUMBER_TYPES.add("double");
    NUMBER_TYPES.add("java.lang.Double");
    NUMBER_TYPES.add("short");
    NUMBER_TYPES.add("java.lang.Short");
  }
  
  public static final Set<String> IMMUTABLE_TYPES = new HashSet<>();
  static {
    IMMUTABLE_TYPES.addAll(NUMBER_TYPES);
    IMMUTABLE_TYPES.add("boolean");
    IMMUTABLE_TYPES.add("java.lang.Boolean");
    IMMUTABLE_TYPES.add("java.lang.String");
    IMMUTABLE_TYPES.add("enum");
  }
  
  protected ClassNode _node = null;
  
  public void printInstructions(File f) throws FileNotFoundException, IOException {
    printInstructions(new FileInputStream(f));
  }
  
  public void printInstructions(InputStream f) throws FileNotFoundException, IOException {
    ClassReader cr = new ClassReader(f);
    cr.accept(new TraceClassVisitor(new PrintWriter(System.out)), ClassReader.EXPAND_FRAMES);
  }
  
  public void printInstructions(String className) throws FileNotFoundException, IOException {
    printInstructions(loadClassFile(className));
  }

  // FIXME remove this code! Use Interprocedural class!
  protected final InputStream loadClassFile(String owner) {
    InputStream s = null;
    try {
      // load it such that it can be found
      Class<?> clz = Class.forName(owner);
      URL url = clz.getResource(owner.substring(owner.lastIndexOf(".") + 1) + ".class");
      s = url.openStream();
    }
    catch(Exception e) {
      throw new RuntimeException(e);
    }
    return s;
  }

  protected final void loadClassFile(InputStream f) throws IOException {
    // load the class file
    _node = new ClassNode();
    ClassVisitor c = createClassVisitor(_node);
    ClassReader cr = new ClassReader(f);
    cr.accept(c, ClassReader.EXPAND_FRAMES);
  }
  
  protected final void charge(String owner) throws IOException{
    try (InputStream s = loadClassFile(owner)){
      loadClassFile(s);
    }
  }
  
  protected ClassVisitor createClassVisitor(ClassVisitor target) throws IOException {
    return target;
  }
  
  protected final MethodNode loadMethod(String methodName, String descriptor) {
    // TODO check for the arguments to make sure it is the method that we want to analyze!
    MethodNode algMethod = null;
    for(Object obj : _node.methods) {
      MethodNode methodNode = (MethodNode) obj;
      if(methodNode.name.equals(methodName)) {
        if(descriptor == null) {
          algMethod = methodNode;
          break;
        } else {
          if(descriptor.equals(methodNode.desc)) {
            algMethod = methodNode;
            break;
          } else {
            // continue
          }
        }
      }
    }
    
    // TODO throw proper exception here!
    if(algMethod == null) throw new RuntimeException("Unable to find method '" + methodName + "' in class '"
                                                     + _node.name + "'!");
    else return algMethod;
  }
  
  protected final String constructMethodDescriptor(Class<?> returnType, Class<?> ... args){
    Type[] argTypes  = new Type[args.length];
    for(int i = 0; i< args.length; i++) argTypes[i] = Type.getType(args[i]);
    return Type.getMethodDescriptor(Type.getType(returnType), argTypes);
  }
  
  protected final Frame[] computeStackMapFrames(MethodNode algMethod){
    // perform the analysis here
    Analyzer a = getAnalyzer(getInterpreter());
    try {
      a.analyze(_node.name, algMethod);
    }
    catch(AnalyzerException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return a.getFrames();
  }
  
  protected Interpreter getInterpreter(){
    return new BasicInterpreter();
  }
  
  protected Analyzer getAnalyzer(Interpreter interpreter){
    return new Analyzer(interpreter);
  }
}
