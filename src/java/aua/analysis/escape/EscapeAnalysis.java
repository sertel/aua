/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Frame;

import aua.analysis.AbstractOperatorAnalysis;

/**
 * This analysis is performed on the byte code.
 * 
 * @author sertel
 * 
 */
// TODO it can be that hierarchies are very deeply nested. therefore, we might need to turn this
// into a state machine to avoid stack overflow errors.
public class EscapeAnalysis extends AbstractOperatorAnalysis {
  public static interface AnalysisBoundary {
    public boolean isAnalyzeMethod(String owner, String method);
  }
  
  public static class LeakDetectionException extends Exception {
    public String _fieldLeaked = null;
    protected int _leakedTaintedLocal = -1;
    
    public String getLeak() {
      return _fieldLeaked;
    }
    
    public String getMessage() {
      return "Leaked field: " + _fieldLeaked;
    }
  }
  
  private LinearityAnalysis _linearity = null;
  private LocalVariableAnalysis _localVariableAnalysis = null;
  private AnalysisBoundary _boundary = null;
  
  public EscapeAnalysis() {
    // default
  }
  
  public EscapeAnalysis(AnalysisBoundary boundary) {
    _boundary = boundary;
  }
  
  public Set<String> analyze(Method method, int[] taintedArgs) throws FileNotFoundException, IOException,
                                                              LeakDetectionException
  {
    String desc = super.constructMethodDescriptor(method.getReturnType(), method.getParameterTypes());
    Map<Integer, String> taintedLocals = buildTaintedArgsMap(taintedArgs);
    return analyze(method.getDeclaringClass().getName(), method.getName(), desc, taintedLocals);
  }
  
  private Map<Integer, String> buildTaintedArgsMap(int[] taintedArgs) {
    HashMap<Integer, String> taintedLocals = new HashMap<>();
    taintedLocals.put(0, "this");
    for(int taintedLocal : taintedArgs)
      taintedLocals.put(taintedLocal, null);
    return taintedLocals;
  }
  
  /**
   * Loads the class file from the class path.
   * 
   * @param fullName
   * @param method
   * @throws FileNotFoundException
   * @throws LeakDetectionException
   */
  public Set<String> analyze(String fullName, String method) throws FileNotFoundException, IOException,
                                                            LeakDetectionException
  {
    return analyzeMethod(fullName, method, null, Collections.singletonMap(0, "this"));
  }
  
  public Set<String> analyze(String fullName, String method, Class<?>[] descriptor) throws FileNotFoundException,
                                                                                   IOException,
                                                                                   LeakDetectionException
  {
    return analyze(fullName, method, descriptor, Collections.singletonMap(0, "this"));
  }
  
  public Set<String> analyze(File f, String methodName) throws FileNotFoundException, IOException,
                                                       LeakDetectionException
  {
    return analyze(new FileInputStream(f), methodName, null, Collections.singletonMap(0, "this"));
  }
  
  public Set<String> analyze(File f, String methodName, int[] taintedArgs) throws FileNotFoundException,
                                                                          IOException, LeakDetectionException
  {
    Map<Integer, String> taintedLocals = buildTaintedArgsMap(taintedArgs);
    return analyze(new FileInputStream(f), methodName, null, taintedLocals);
  }
  
  private Set<String> analyze(String owner, String methodName, Class<?>[] methodDescriptor,
                              Map<Integer, String> taintedLocals) throws FileNotFoundException, IOException,
                                                                 LeakDetectionException
  {
    String desc =
        super.constructMethodDescriptor(methodDescriptor[0],
                                        Arrays.copyOfRange(methodDescriptor, 1, methodDescriptor.length));
    return analyzeMethod(owner, methodName, desc, taintedLocals);
  }
  
  protected Set<String> analyze(String owner, String methodName, String methodDescriptor,
                                Map<Integer, String> taintedLocals) throws FileNotFoundException, IOException,
                                                                   LeakDetectionException
  {
    return analyzeMethod(owner, methodName, methodDescriptor, taintedLocals);
  }
  
  private Set<String> analyzeMethod(String owner, String methodName, String methodDescriptor,
                                    Map<Integer, String> taintedLocals) throws FileNotFoundException, IOException,
                                                                       LeakDetectionException
  {
    try(InputStream s = loadClassFile(owner)) {
      return analyze(s, methodName, methodDescriptor, taintedLocals);
    }
  }
  
  private Set<String> analyze(InputStream f, String methodName, String methodDescriptor,
                              Map<Integer, String> trackedArguments) throws FileNotFoundException, IOException,
                                                                    LeakDetectionException
  {
    // load the class file
    super.loadClassFile(f);
    
    // find the desired method
    MethodNode algMethod = super.loadMethod(methodName, methodDescriptor);
    
    // find the names for the tracked args
    trackedArguments = completeTrackedInfo(algMethod, trackedArguments);
    
    // analyze the function
    return analyze(algMethod, trackedArguments);
  }
  
  private Map<Integer, String> completeTrackedInfo(MethodNode m, Map<Integer, String> trackedArguments) {
    HashMap<Integer, String> updated = new HashMap<>();
    for(Map.Entry<Integer, String> entry : trackedArguments.entrySet()) {
      if(entry.getValue() == null) updated.put(entry.getKey(),
                                               ((LocalVariableNode) m.localVariables.get(entry.getKey())).name);
      else updated.put(entry.getKey(), entry.getValue());
    }
    return updated;
  }
  
  private Set<String>
      analyze(MethodNode algMethod, Map<Integer, String> localsToTrack) throws LeakDetectionException
  {
    // perform the analysis here
    Frame[] frames = super.computeStackMapFrames(algMethod);
    /**
     * The symmetry is: the i-th instruction is applied to the i-th frame resulting in the
     * i+1-th frame.
     */
    AbstractInsnNode[] instructions = algMethod.instructions.toArray();
    assert localsToTrack.size() > 0;
    
    AnalysisBoundary boundary = _boundary != null ? _boundary : getDefaultBoundary(_node.name);
    EscapeAnalysisCore core =
        new EscapeAnalysisCore(new InvocationTaintAnalysis(boundary), new ReleaseAnalysis(_localVariableAnalysis));
    Set<String> skipped =
        core.analyzeMethod(_node.name, algMethod.name, algMethod.desc, instructions, frames, localsToTrack, _linearity);
    // System.out.println("Done with analysis of method: " + _node.name + "." + algMethod.name);
    return skipped;
  }
  
  private AnalysisBoundary getDefaultBoundary(final String operatorCls) {
    /**
     * Don't analyze anything outside the realm of this operator.
     */
    return new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        String opPackage = operatorCls.substring(0, operatorCls.lastIndexOf("/"));
        String methodOwnerPackage = owner.substring(0, owner.lastIndexOf("/"));
        return opPackage.equals(methodOwnerPackage);
      }
    };
  }
  
  protected ClassVisitor createClassVisitor(ClassVisitor target) throws IOException {
    _linearity = new LinearityAnalysis(Opcodes.ASM5, target);
    _localVariableAnalysis = new LocalVariableAnalysis(Opcodes.ASM5, _linearity);
    return _localVariableAnalysis;
  }
  
}
