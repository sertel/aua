/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.MethodInsnNode;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.Interprocedural;
import aua.analysis.escape.EscapeAnalysis.AnalysisBoundary;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.escape.EscapeAnalysisCore.Context;
import aua.analysis.qual.Tainted;
import aua.analysis.qual.TaintedChecker;
import aua.analysis.qual.Untainted;

public class InvocationTaintAnalysis extends AbstractAnalysisCore {
  
  interface ResultValue {
    String get();
  }
  
  protected enum Result {
    SAFE,
    UNSAFE,
    SKIPPED;
    
    private String _result = null;
    private Set<String> _skipped = null;
    
    protected void attachResult(String result) {
      _result = result;
    }
    
    protected void attachSkipped(Set<String> skipped) {
      _skipped = skipped;
    }
    
    protected String getResult() {
      String result = _result;
      _result = null;
      return result;
    }
    
    protected Set<String> getSkipped() {
      Set<String> s = _skipped;
      _skipped = null;
      return s;
    }
  }
  
  private AnalysisBoundary _boundary = null;
  
  public InvocationTaintAnalysis(AnalysisBoundary boundary) {
    _boundary = boundary;
  }
  
  protected Result handleINVOKE(Context ctxt, int topStackSlotIdx, MethodInsnNode methodInvocation, int argCount) {
    boolean taintedInputs = false;
    for(int i = 0; i < argCount; i++) {
      taintedInputs |= ctxt.taintedStackSlots.contains(topStackSlotIdx - i);
    }
    if(methodInvocation.getOpcode() != Opcodes.INVOKESTATIC) {
      taintedInputs |= ctxt.taintedStackSlots.contains(topStackSlotIdx - argCount);
    }
    if(!taintedInputs) return Result.SAFE;
    else return analyzeInvocation(ctxt, topStackSlotIdx, methodInvocation, argCount);
  }
  
  @SuppressWarnings("unchecked")
  private Result
      analyzeInvocation(Context ctxt, int topStackSlotIdx, MethodInsnNode methodInvocation, int argCount)
  {
    // System.out.println("Detected method: " + methodInvocation.owner + "." +
    // methodInvocation.name);
    if(methodInvocation.name.equals("<init>")) {
      // FIXME constructors leak the created object and therefore need to be treated just as any
      // other method!
      return Result.SAFE;// skip constructors for now
    } else {
      // only care for functions with mutable return values and parameters
      Type methodType = Type.getMethodType(methodInvocation.desc);
      if(isImmutableMethod(methodType)) {
        // System.out.println("Skipping method invocation with immutable return type and immutable arguments: "
        // + methodInvocation.owner + "." + methodInvocation.name);
        return Result.SAFE;
      }
      
      // skip native methods
      if(isNativeMethod(methodInvocation, argCount)) {
        // System.out.println("Skipping native method invocation: " + methodInvocation.owner +
        // "."
        // + methodInvocation.name);
        return Result.SAFE;
      }
      
      // FIXME there is a subtle difference between the knowledge base handling here and the
      // annotations. annotations are on types and respectively the result of a function is
      // untainted/tainted. however, this does not mean to skip the whole analysis of the
      // function as there might be still a leak by accessing a global variable! it seems that
      // the knowledge base should be consulted after the analysis of the function to understand
      // whether it leaks something through its return value rather than skipping the whole
      // analysis of the function. however, it should be possible to skip a function!
      
      // check the knowledge base
      Annotation annotation = consultKnowledgeBase(methodInvocation);
      String ownerClassName = Type.getObjectType(methodInvocation.owner).getClassName();
      if(annotation != null) {
        if(annotation instanceof Tainted) {
          Result result = Result.UNSAFE;
          result.attachResult(ownerClassName + "." + methodInvocation.name);
          return result;
        } else if(annotation instanceof Untainted) return Result.SAFE;
        else assert false;
      }
      
      // interface methods are being skipped yet because we would have to find the
      // implementation for it (maybe this is as easy as checking the types on the stack?!)
      if(methodInvocation.itf) return Result.SKIPPED;
      
      // the same accounts for abstract methods
      if(isAbstractMethod(methodInvocation, argCount)) return Result.SKIPPED;
      
      // we consider this as safe. our byte code analysis will investigate the "halting" branch
      // and this is the only place where something can leak.
      if(isDirectRecursion(ctxt, methodInvocation)) return Result.SAFE;
      
      // restrict investigation to local packages but report an uncertainty as an analysis
      // result
      if(_boundary.isAnalyzeMethod(methodInvocation.owner, methodInvocation.name)) {
        Object[] r = performByteCodeAnalysis(ctxt, topStackSlotIdx, methodInvocation, argCount);
        String leakedField = (String) r[0];
        Result result = null;
        if(leakedField == null) result = Result.SAFE;
        else {
          result = Result.UNSAFE;
          result.attachResult(leakedField);
        }
        result.attachSkipped((Set<String>) r[1]);
        return result;
      } else {
        return Result.SKIPPED;
      }
    }
  }
  
  private boolean isDirectRecursion(Context ctxt, MethodInsnNode methodInvocation) {
    return ctxt.owner.equals(methodInvocation.owner) && ctxt.methodName.equals(methodInvocation.name)
           && ctxt.methodDesc.equals(methodInvocation.desc);
  }
  
  private boolean isImmutableMethod(Type methodType) {
    for(Type argType : methodType.getArgumentTypes()) {
      if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(argType.getClassName())) {
        return false;
      }
    }
    return AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(methodType.getReturnType().getClassName());
  }
  
  private Annotation consultKnowledgeBase(MethodInsnNode methodInvocation) {
    String ownerClassName = Type.getObjectType(methodInvocation.owner).getClassName();
    TaintedChecker checker = new TaintedChecker();
    // try first without reflection
    Annotation annotation = checker.getTaintedAnnotation(ownerClassName, methodInvocation.name);
    if(annotation != null) return annotation;
    else {
      // use reflection
      Method method = Interprocedural.classLoadMethod(methodInvocation);
      if(method != null) {
        annotation = checker.getTaintedAnnotation(method);
        if(annotation != null) return annotation;
      }
    }
    return null;
  }
  
  /**
   * All args will be tainted such that we detect when a write to an arg object reference
   * occurs!
   * 
   * @param localsOnStack
   * @param pureObjectsOnStack
   * @param loadedFields
   * @param taintedStackSlots
   * @param topStackSlotIdx
   * @param methodInvocation
   * @param argCount
   * @return
   */
  private Object[] performByteCodeAnalysis(Context ctxt, int topStackSlotIdx, MethodInsnNode methodInvocation,
                                           int argCount)
  {
    String leakedField = null;
    int origin;
    Map<Integer, String> taintedArgs = new HashMap<>();
    taintedArgs.put(0, "this");
    if(argCount > 0) {
      int firstArgSlot = topStackSlotIdx - argCount + 1;
      for(int i = firstArgSlot; i <= topStackSlotIdx; i++) {
        if(ctxt.taintedStackSlots.contains(i)) {
          int argSlot = i - firstArgSlot + 1;
          int originSlot = findOrigin(ctxt.pureObjectsOnStack, i);
          assert originSlot > -1;
          taintedArgs.put(argSlot, ctxt.loadedFields.get(originSlot)._field);
        }
      }
    }
    Set<String> skippedMethods = null;
    try {
      Method m = Interprocedural.classLoadMethod(methodInvocation);
      EscapeAnalysis analysis = new EscapeAnalysis(_boundary);
      // TODO this is overhead because it means even if the function is essentially a part of
      // the class that we are currently working, we nevertheless load that thing again through
      // the ASM library.
      skippedMethods =
          analysis.analyze(m.getDeclaringClass().getName(),
                           methodInvocation.name,
                           methodInvocation.desc,
                           taintedArgs);
    }
    catch(LeakDetectionException e) {
      if(e._fieldLeaked != null) leakedField = e._fieldLeaked;
      else {
        int stackSlot = topStackSlotIdx - argCount + e._leakedTaintedLocal;
        origin = findOrigin(ctxt.pureObjectsOnStack, stackSlot);
        leakedField = ctxt.loadedFields.get(origin)._field;
      }
    }
    catch(IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return new Object[] { leakedField,
                         skippedMethods };
  }
  
  private boolean isNativeMethod(MethodInsnNode methodInvocation, int argCount) {
    // skip native methods
    // TODO use ASM instead of reflection and class loading
    Method method = Interprocedural.classLoadMethod(methodInvocation);
    return Modifier.isNative(method.getModifiers());
  }
  
  private boolean isAbstractMethod(MethodInsnNode methodInvocation, int argCount) {
    // skip abstract methods
    // TODO use ASM instead of reflection and class loading
    Method method = Interprocedural.classLoadMethod(methodInvocation);
    return Modifier.isAbstract(method.getModifiers());
  }  
}
