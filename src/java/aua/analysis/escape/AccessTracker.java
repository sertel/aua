/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

// TODO Currently we track only the stack. Eventually we might also want to track the locals that a field was stored to.
public class AccessTracker implements ByteCodeAnalysis
{
  private Map<Integer, String> _intFieldsOnStack = new HashMap<>();
  private Map<Integer, Integer> _intLocalsOnStack = new HashMap<>();
  
  protected AccessTracker() {
    // restrict visibility
  }
  
  protected String getStackSlotField(int stackSlotIdx) {
    return _intFieldsOnStack.get(stackSlotIdx);
  }
  
  public int getStackSlotLocal(int stackSlotIdx) {
    return _intLocalsOnStack.containsKey(stackSlotIdx) ? _intLocalsOnStack.get(stackSlotIdx) : -1;
  }
  
  public void handleInstruction(AbstractInsnNode instruction, int topStackSlotIdx, int j) {
    sweep(topStackSlotIdx, _intFieldsOnStack);
    sweep(topStackSlotIdx, _intLocalsOnStack);
    
    switch(instruction.getOpcode())
    {
      case Opcodes.ACONST_NULL:
        // .. -> NULL (value of type Object)
        // loads a NULL object onto the stack
        break;
      case Opcodes.ICONST_M1:
      case Opcodes.ICONST_0:
      case Opcodes.ICONST_1:
      case Opcodes.ICONST_2:
      case Opcodes.ICONST_3:
      case Opcodes.ICONST_4:
      case Opcodes.ICONST_5:
      case Opcodes.LCONST_0:
      case Opcodes.LCONST_1:
      case Opcodes.FCONST_0:
      case Opcodes.FCONST_1:
      case Opcodes.FCONST_2:
      case Opcodes.DCONST_0:
      case Opcodes.DCONST_1:
      case Opcodes.BIPUSH:
      case Opcodes.SIPUSH:
      case Opcodes.LDC:
        // ... -> value
        // these instructions just load a value to the stack.
        break;
      case Opcodes.ILOAD:
        // ... -> value/object_ref
        // load int value from a local onto the stack.
        _intLocalsOnStack.put(topStackSlotIdx + 1, ((VarInsnNode) instruction).var);
        break;
      case Opcodes.LLOAD:
      case Opcodes.FLOAD:
      case Opcodes.DLOAD:
        // ... -> value/object_ref
        // load primitive values from a local onto the stack.
        break;
      case Opcodes.ALOAD:
        // ... -> value/object_ref
        // loads reference from local onto stack
        break;
      case Opcodes.IALOAD:
      case Opcodes.LALOAD:
      case Opcodes.FALOAD:
      case Opcodes.DALOAD:
        // array_ref idx -> value
        // these instructions load primitive value from an array. the top-most stack value marks
        // the index in the array.
        break;
      case Opcodes.AALOAD:
        // array_ref idx -> object_ref
        // these instructions load data from an array. the top-most stack value marks the
        // index in the array.
        break;
      case Opcodes.BALOAD:
      case Opcodes.CALOAD:
      case Opcodes.SALOAD:
        // array_ref idx -> value
        // these instructions load primitive value from an array. the top-most stack value marks
        // the
        // index in the array.
        break;
      case Opcodes.ISTORE:
      case Opcodes.LSTORE:
      case Opcodes.FSTORE:
      case Opcodes.DSTORE:
        // value -> ...
        // simple store operation to a local.
        break;
      case Opcodes.ASTORE:
        // object_ref -> ...
        // simple store operation to a local.
        break;
      case Opcodes.IASTORE:
      case Opcodes.LASTORE:
      case Opcodes.FASTORE:
      case Opcodes.DASTORE:
        // array_ref index value -> ...
        // store operation to an array of a primitive value.
        break;
      case Opcodes.AASTORE:
        // array_ref index object_ref -> ...
        // store operation to an array of an object reference.
        break;
      case Opcodes.BASTORE:
      case Opcodes.CASTORE:
      case Opcodes.SASTORE:
        // array_ref index value -> ...
        // store operation to an array of a primitive value.
        break;
      case Opcodes.POP:
        // value/object_ref -> ...
        // change the stack
        break;
      case Opcodes.POP2:
        // value/object_ref value/object_ref -> ...
        // change the stack
        break;
      case Opcodes.DUP:
        // value/object_ref -> value/object_ref value/object_ref
        // duplicates object reference.
        if(_intFieldsOnStack.containsKey(topStackSlotIdx)) _intFieldsOnStack.put(topStackSlotIdx + 1,
                                                                                 _intFieldsOnStack.get(topStackSlotIdx));
        if(_intLocalsOnStack.containsKey(topStackSlotIdx)) _intLocalsOnStack.put(topStackSlotIdx + 1,
                                                                                 _intLocalsOnStack.get(topStackSlotIdx));
        break;
      case Opcodes.DUP_X1:
        // value2 value1 -> value1 value2 value1
        // inserts 'value1' two slots below on the stack.
        
        handleDUP_X1(topStackSlotIdx, _intFieldsOnStack);
        handleDUP_X1(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.DUP_X2:
        // value3 value2 value1 -> value1 value3 value2 value1
        // same as above
        
        handleDUP_X2(topStackSlotIdx, _intFieldsOnStack);
        handleDUP_X2(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.DUP2:
        // value2 value1 -> value2 value1 value2 value1
        // duplicates the two top-most slots.
        
        handleDUP2(topStackSlotIdx, _intFieldsOnStack);
        handleDUP2(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.DUP2_X1:
        // value3 value2 value1 -> value2 value1 value3 value2 value1
        // same as above
        
        handleDUP2_X1(topStackSlotIdx, _intFieldsOnStack);
        handleDUP2_X1(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.DUP2_X2:
        // value4 value3 value2 value1 -> value2 value1 value4 value3 value2 value1
        // same as above
        
        handleDUP2_X2(topStackSlotIdx, _intFieldsOnStack);
        handleDUP2_X2(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.SWAP:
        // value2 value1 -> value1 value2
        // stack size does not change but the slot of interest 'value2' just moved to the
        // slot below.
        
        handleSWAP(topStackSlotIdx, _intFieldsOnStack);
        handleSWAP(topStackSlotIdx, _intLocalsOnStack);
        
        break;
      case Opcodes.IADD:
      case Opcodes.LADD:
      case Opcodes.FADD:
      case Opcodes.DADD:
      case Opcodes.ISUB:
      case Opcodes.LSUB:
      case Opcodes.FSUB:
      case Opcodes.DSUB:
      case Opcodes.IMUL:
      case Opcodes.LMUL:
      case Opcodes.FMUL:
      case Opcodes.DMUL:
      case Opcodes.IDIV:
      case Opcodes.LDIV:
      case Opcodes.FDIV:
      case Opcodes.DDIV:
      case Opcodes.IREM:
      case Opcodes.LREM:
      case Opcodes.FREM:
      case Opcodes.DREM:
        // value value -> result
        break;
      case Opcodes.INEG:
      case Opcodes.LNEG:
      case Opcodes.FNEG:
      case Opcodes.DNEG:
        // value -> result
        break;
      case Opcodes.ISHL:
      case Opcodes.LSHL:
        // value1 value2 -> result
        // shift left
        break;
      case Opcodes.ISHR:
      case Opcodes.LSHR:
      case Opcodes.IUSHR:
      case Opcodes.LUSHR:
        // value1 value2 -> result
        // shift right
        break;
      case Opcodes.IAND:
      case Opcodes.LAND:
      case Opcodes.IOR:
      case Opcodes.LOR:
      case Opcodes.IXOR:
      case Opcodes.LXOR:
        // value1 value2 -> result
        // boolean operations
        break;
      case Opcodes.IINC:
        // increments local variable with constant. no stack change
        break;
      case Opcodes.I2L:
      case Opcodes.I2F:
      case Opcodes.I2D:
      case Opcodes.L2I:
      case Opcodes.L2F:
      case Opcodes.L2D:
      case Opcodes.F2I:
      case Opcodes.F2L:
      case Opcodes.F2D:
      case Opcodes.D2I:
      case Opcodes.D2L:
      case Opcodes.D2F:
      case Opcodes.I2B:
      case Opcodes.I2C:
      case Opcodes.I2S:
        // value -> result
        // type conversion for primitive types
        break;
      case Opcodes.LCMP:
      case Opcodes.FCMPL:
      case Opcodes.FCMPG:
      case Opcodes.DCMPL:
      case Opcodes.DCMPG:
        // value value -> result
        // comparison operations for primitive types
        break;
      case Opcodes.IFEQ:
      case Opcodes.IFNE:
      case Opcodes.IFLT:
      case Opcodes.IFGE:
      case Opcodes.IFGT:
      case Opcodes.IFLE:
        // value -> ...
        // conditional jump
        break;
      case Opcodes.IF_ICMPEQ:
      case Opcodes.IF_ICMPNE:
      case Opcodes.IF_ICMPLT:
      case Opcodes.IF_ICMPGE:
      case Opcodes.IF_ICMPGT:
      case Opcodes.IF_ICMPLE:
      case Opcodes.IF_ACMPEQ:
      case Opcodes.IF_ACMPNE:
        // value value -> ...
        // conditional jump
        break;
      case Opcodes.GOTO:
        // [no stack change]
        // unconditional jump
        // nothing to be done here. (afterwards a stack frame instruction will follow)
        break;
      case Opcodes.JSR:
        // ... -> address
        // jump to sub routine
        break;
      case Opcodes.RET:
        // [no stack change]
        // continues execution from address in local variable
        break;
      case Opcodes.TABLESWITCH:
      case Opcodes.LOOKUPSWITCH:
        // index/key ->
        // jump to instruction of the address looked up
        // seems like this does not matter to us because we always want to go throw all frames
        // of the switch statement
        break;
      case Opcodes.IRETURN:
      case Opcodes.LRETURN:
      case Opcodes.FRETURN:
      case Opcodes.DRETURN:
        // [primitive value] -> empty
        break;
      case Opcodes.ARETURN:
        // [value] -> empty
        break;
      case Opcodes.RETURN:
        // -> empty
        // void return
        break;
      case Opcodes.GETSTATIC:
        // -> value
        // load value from static field
        break;
      case Opcodes.PUTSTATIC:
        // value ->
        // store value to static field
        break;
      case Opcodes.GETFIELD:
        // object_ref -> value
        // load value from object field -> important.
        Type t = Type.getType(((FieldInsnNode) instruction).desc);
        if(t.getClassName().equals("int") || t.getClassName().equals("java.lang.Integer")) {
          _intFieldsOnStack.put(topStackSlotIdx, ((FieldInsnNode) instruction).name);
        }
        break;
      case Opcodes.PUTFIELD:
        // object_ref value ->
        // store value to object field
        break;
      case Opcodes.INVOKEVIRTUAL:
      case Opcodes.INVOKESPECIAL:
      case Opcodes.INVOKEINTERFACE:
        // object_ref[args] -> result
        // function invocation
        break;
      case Opcodes.INVOKESTATIC:
        // [args] -> result
        // function invocation
        break;
      case Opcodes.INVOKEDYNAMIC:
        // [args] -> result
        // function invocation
        break;
      case Opcodes.NEW:
        // -> objectref
        // object creation
        break;
      case Opcodes.NEWARRAY:
      case Opcodes.ANEWARRAY:
        // count -> array_ref
        // array creation
        break;
      case Opcodes.ARRAYLENGTH:
        // array_ref -> length
        break;
      case Opcodes.ATHROW:
        // object_ref -> [empty] object_ref
        // treated like a 'return' statement.
        break;
      case Opcodes.CHECKCAST:
        // object_ref -> object_ref
        break;
      case Opcodes.INSTANCEOF:
        // object_ref -> result
        break;
      case Opcodes.MONITORENTER:
      case Opcodes.MONITOREXIT:
        // object_ref ->
        break;
      case Opcodes.MULTIANEWARRAY:
        // [count1 count2] -> array_ref
        // creates n-dimensional array
        break;
      case Opcodes.IFNULL:
      case Opcodes.IFNONNULL:
        // value ->
        // conditional jump
        break;
      default:
        assert false : "Discovered unknown instruction. Opcode: " + instruction.getOpcode() + " type: "
                             + instruction.getType();
    }
  }
  
  private <T extends Object> void handleSWAP(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    if(value1 != null) registry.put(topStackSlotIdx - 1, value1);
    if(value2 != null) registry.put(topStackSlotIdx - 2, value2);
  }
  
  private <T extends Object> void handleDUP2_X2(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    T value3 = registry.containsKey(topStackSlotIdx - 2) ? registry.get(topStackSlotIdx - 2) : null;
    T value4 = registry.containsKey(topStackSlotIdx - 3) ? registry.get(topStackSlotIdx - 3) : null;
    if(value1 != null) {
      registry.put(topStackSlotIdx + 2, value1);
      registry.put(topStackSlotIdx - 2, value1);
    }
    if(value2 != null) {
      registry.put(topStackSlotIdx + 1, value2);
      registry.put(topStackSlotIdx - 3, value2);
    }
    if(value3 != null) registry.put(topStackSlotIdx, value3);
    if(value4 != null) registry.put(topStackSlotIdx - 1, value4);
  }
  
  private <T extends Object> void handleDUP2_X1(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    T value3 = registry.containsKey(topStackSlotIdx - 2) ? registry.get(topStackSlotIdx - 2) : null;
    if(value1 != null) {
      registry.put(topStackSlotIdx + 2, value1);
      registry.put(topStackSlotIdx - 1, value1);
    }
    if(value2 != null) {
      registry.put(topStackSlotIdx + 1, value2);
      registry.put(topStackSlotIdx - 2, value2);
    }
    if(value3 != null) registry.put(topStackSlotIdx, value3);
  }
  
  private <T extends Object> void handleDUP2(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    if(value1 != null) {
      registry.put(topStackSlotIdx + 2, value1);
      registry.put(topStackSlotIdx, value1);
    }
    if(value2 != null) {
      registry.put(topStackSlotIdx + 1, value2);
      registry.put(topStackSlotIdx - 1, value2);
    }
  }
  
  private <T extends Object> void handleDUP_X2(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    T value3 = registry.containsKey(topStackSlotIdx - 2) ? registry.get(topStackSlotIdx - 2) : null;
    
    if(value1 != null) {
      registry.put(topStackSlotIdx + 1, value1);
      registry.put(topStackSlotIdx - 2, value1);
    }
    if(value2 != null) registry.put(topStackSlotIdx, value2);
    if(value3 != null) registry.put(topStackSlotIdx - 1, value3);
  }
  
  private <T extends Object> void handleDUP_X1(int topStackSlotIdx, Map<Integer, T> registry) {
    // make sure we do not overwrite anybody
    T value1 = registry.containsKey(topStackSlotIdx) ? registry.get(topStackSlotIdx) : null;
    T value2 = registry.containsKey(topStackSlotIdx - 1) ? registry.get(topStackSlotIdx - 1) : null;
    
    if(value1 != null) {
      registry.put(topStackSlotIdx + 1, value1);
      registry.put(topStackSlotIdx - 1, value1);
    }
    if(value2 != null) registry.put(topStackSlotIdx, value2);
  }
    
  private void sweep(int topStackSlot, Map<Integer, ?> registry) {
    Set<Integer> toDelete = new HashSet<>();
    for(Map.Entry<Integer, ?> entry : registry.entrySet())
      if(entry.getKey() > topStackSlot) toDelete.add(entry.getKey());
    
    for(Integer i : toDelete)
      registry.remove(i);
  }
  
}
