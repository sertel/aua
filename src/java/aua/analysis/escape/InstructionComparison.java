/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

public class InstructionComparison
{
  private InstructionComparison() {}
  
  public static boolean equal(AbstractInsnNode one, AbstractInsnNode two) {
    if(one.getType() != two.getType()) return false;
    
    switch(one.getType())
    {
      case AbstractInsnNode.LINE:
        // not interesting for now
        break;
      case AbstractInsnNode.LABEL:
        // not interesting for now
        break;
      case AbstractInsnNode.FRAME:
        // not interesting for now
        break;
      default:
        return instructionEqual(one, two);
    }
    
    return false;
  }
  
  /**
   * Covers the most important instructions for comparison.
   * 
   * @param one
   * @param two
   * @return
   */
  private static boolean instructionEqual(AbstractInsnNode one, AbstractInsnNode two) {
    if(one.getOpcode() != two.getOpcode()) return false;
    
    if(one instanceof InsnNode) {
      // no additional information to compare here (the opcode verification is all comparison
      // that is needed)
      return true;
    }
    else if(one instanceof IntInsnNode) {
      return ((IntInsnNode) one).operand == ((IntInsnNode) two).operand;
    }
    else if(one instanceof FieldInsnNode) {
      if(!((FieldInsnNode) one).desc.equals(((FieldInsnNode) two).desc)) return false;
      if(!((FieldInsnNode) one).name.equals(((FieldInsnNode) two).name)) return false;
      return ((FieldInsnNode) one).owner.equals(((FieldInsnNode) two).owner);
    }
    else if(one instanceof MultiANewArrayInsnNode) {
      if(!((MultiANewArrayInsnNode) one).desc.equals(((MultiANewArrayInsnNode) two).desc)) return false;
      return ((MultiANewArrayInsnNode) one).dims == ((MultiANewArrayInsnNode) two).dims;
    }
    else if(one instanceof JumpInsnNode) {
      return instructionEqual(((JumpInsnNode) one).label, ((JumpInsnNode) two).label);
    }
    else if(one instanceof LabelNode) {
      return one.equals(two);
    }
    else if(one instanceof TypeInsnNode) {
      return ((TypeInsnNode) one).desc.equals(((TypeInsnNode) two).desc);
    }
    else if(one instanceof VarInsnNode) {
      return ((VarInsnNode) one).var == ((VarInsnNode) two).var;
    }
    else {
      return true;
    }
  }
}
