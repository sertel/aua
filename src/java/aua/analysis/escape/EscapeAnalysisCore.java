/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FrameNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.Frame;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.DFACommons;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.escape.InvocationTaintAnalysis.Result;
import aua.analysis.qual.KnowledgeBase;

public class EscapeAnalysisCore extends AbstractAnalysisCore {
  public static boolean REPORT_ERROR_MSG = false;
  public class LoadContext {
    protected AbstractInsnNode _inst;
    protected String _field;
    protected int _frameInst;
    
    LoadContext(AbstractInsnNode inst, String field, int frameInst) {
      _inst = inst;
      _field = field;
      _frameInst = frameInst;
    }
  }
  
  protected class Context {
    // reference to the latest frame instruction
    int currentFrame = 0;
    
    // key: locals ID values: source stack slot
    Map<Integer, Integer> localsOnStack = new HashMap<>();
    // key: source stack slot values: stack slots
    Map<Integer, Set<Integer>> pureObjectsOnStack = new HashMap<>();
    
    // key: source stack slot values: loaded field names
    Map<Integer, LoadContext> loadedFields = new HashMap<>();
    // key: locals ID value: load context
    Map<Integer, LoadContext> loadedFieldsToLocals = new HashMap<>();
    
    // we only trace origins
    Set<Integer> taintedStackSlots = new HashSet<>();
    Set<Integer> taintedLocals = new HashSet<>();
    
    // NULL trace
    Set<Integer> nullsOnStack = new HashSet<>();
    
    // array slot release tracking. -> tracks all AALOAD instructions.
    Set<InsnNode> arrayLoadInstructions = new HashSet<>();
    
    String owner = null;
    String methodName = null;
    String methodDesc = null;
    
    // these labels are collected from jump instructions and mark the start of new blocks in the
    // CFG.
    Set<LabelNode> blockEntries = new HashSet<>();
    // NOTE: in case of a do-while loop, the loop entry is not detected. but this seems ok for
    // now.
    LabelNode currentBlock = null;
  }
  
  private AccessTracker _accessTracker = new AccessTracker();
  private ReleaseAnalysis _releaseAnalysis = null;
  private InvocationTaintAnalysis _invokeAnalysis = null;
  
  protected EscapeAnalysisCore(InvocationTaintAnalysis invocationTaintAnalysis, ReleaseAnalysis releaseAnalysis) {
    _invokeAnalysis = invocationTaintAnalysis;
    _releaseAnalysis = releaseAnalysis;
  }
  
  /**
   * This version tracks all objects per default.
   * 
   * @throws LeakDetectionException
   */
  protected
      Set<String>
      analyzeMethod(String owner, String methodName, String methodDesc, AbstractInsnNode[] instructions,
                    Frame[] frames, Map<Integer, String> localsToTrack, LinearityAnalysis linearity)
                                                                                                    throws LeakDetectionException
  {
    Set<String> skippedMethods = new HashSet<>();
    Context ctxt = new Context();
    ctxt.owner = owner;
    ctxt.methodName = methodName;
    ctxt.methodDesc = methodDesc;
    for(Map.Entry<Integer, String> entry : localsToTrack.entrySet()) {
      LoadContext load = new LoadContext(instructions[0], entry.getValue(), 0);
      ctxt.loadedFieldsToLocals.put(entry.getKey(), load);
    }
    ctxt.taintedLocals.addAll(localsToTrack.keySet());
    
    @SuppressWarnings("unused")// for debugging just set a conditional breakpoint into the loop
    // below
    int lineNumber = 0;
    
    for(int j = 0; j < instructions.length; j++) {
      AbstractInsnNode instruction = instructions[j];
      // this function carries the wrong name. it actually returns the index of the top slot
      int topStackSlotIdx = frames[j] != null ? frames[j].getStackSize() : 0;
      // if this thing says 0 then the stack is empty! so the first stack position is actually
      // 1.
      
      switch(instruction.getType()) {
        case AbstractInsnNode.LINE:
          // TODO line numbers for error reporting
          lineNumber = ((LineNumberNode) instruction).line;
          break;
        case AbstractInsnNode.LABEL:
          handleLabel(ctxt, (LabelNode) instruction, j);
          break;
        case AbstractInsnNode.FRAME:
          /**
           * Frame information is just meta data on the types. so our algorithm should not rely
           * on it. it must work even without this information. For more information see here:<br>
           * http://stackoverflow.com/questions/25109942/is-there-a-better-explanation-of-stack-
           * map-frames<br>
           * or here:<br>
           * http://chrononsystems.com/blog/java-7-design-flaw-leads-to-huge-backward-step-for-
           * the-jvm
           * <p>
           * It turns out that ASM and Java > 5 have some requirements here. In
           * org.objectweb.asm.tree.FrameNode the documentation says: <br>
           * A node that represents a stack map frame. These nodes are pseudo instruction nodes
           * in order to be inserted in an instruction list. In fact these nodes must(*) be
           * inserted <i>just before</i> any instruction node <b>i</b> that follows an
           * unconditionnal branch instruction such as GOTO or THROW, that is the target of a
           * jump instruction, or that starts an exception handler block. The stack map frame
           * types must describe the values of the local variables and of the operand stack
           * elements <i>just before</i> <b>i</b> is executed. <br>
           * <br>
           * (*) this is mandatory only for classes whose version is greater than or equal to
           * {@link Opcodes#V1_6 V1_6}.
           * <p>
           * That means we have to be sensitive to it at least for those instructions!
           */
          handleStackMapFrame(ctxt, (FrameNode) instruction, topStackSlotIdx);
          break;
        default:
          try {
            skippedMethods.addAll(handleInstruction(ctxt, instruction, topStackSlotIdx, j, linearity));
            _accessTracker.handleInstruction(instruction, topStackSlotIdx, j);
          }
          catch(LeakDetectionException l) {
            throw l;
          }
          catch(Throwable t) {
            if(REPORT_ERROR_MSG)
              System.err.println("Caught an exception when analyzing function '" + ctxt.owner + "."
                               + ctxt.methodName + "' at instruction " + j);
            throw t;
          }
      }
    }
    return skippedMethods;
  }
  
  private void handleStackMapFrame(Context ctxt, FrameNode instruction, int topStackSlotIdx) {
    if(instruction.getPrevious() != null) {
      AbstractInsnNode previousIns = null;
      // assumption: at most one LabelNode in between!
      switch(instruction.getPrevious().getType()) {
        case AbstractInsnNode.LABEL:
          if(instruction.getPrevious().getPrevious() != null) previousIns =
              instruction.getPrevious().getPrevious();
          break;
        default:
          previousIns = instruction.getPrevious();
      }
      
      if(previousIns != null
         && (previousIns.getOpcode() == Opcodes.GOTO || previousIns.getOpcode() == Opcodes.ATHROW))
      {
        // adjust the stack
        // TODO maybe we have to do that for the locals here too
        
        /*
         * I do not know what triggers really the stack change because after the previous
         * instruction the topStackSlotIdx is already changed accordingly even before this frame
         * instruction is executed.
         */
        if(instruction.stack.isEmpty()) wipeStack(ctxt);
        else cleanup(ctxt, instruction.stack.size() + 1, getMaxSlot(ctxt.pureObjectsOnStack.keySet()));
      }
      
    }
  }
  
  private void handleLabel(Context ctxt, LabelNode labelNode, int currentIndex) {
    _releaseAnalysis.notifyLabel(ctxt, labelNode);
    
    // register the first block
    if(ctxt.currentBlock == null) ctxt.currentBlock = labelNode;
    
    // the retrieval of labels in ASM is on object level (LabelNode objects are shared)
    if(ctxt.blockEntries.contains(labelNode)) {
      _releaseAnalysis.notifyBlockEntry(ctxt, ctxt.currentBlock, labelNode);
      ctxt.currentBlock = labelNode;
      ctxt.blockEntries.remove(labelNode);
    }
  }
  
  private Set<String>
      handleInstruction(Context ctxt, AbstractInsnNode instruction, int topStackSlotIdx, int currentIndex,
                        LinearityAnalysis linearity) throws LeakDetectionException
  {
    if(instruction.visibleTypeAnnotations != null)
      System.out.println(instruction.getOpcode() + ": " + Arrays.deepToString(instruction.visibleTypeAnnotations.toArray()));

    assert topStackSlotIdx >= getMaxSlot(ctxt.taintedStackSlots);
    Set<String> skippedMethods = new HashSet<>();
    switch(instruction.getOpcode()) {
      case Opcodes.ACONST_NULL:
        // .. -> NULL (value of type Object)
        // loads a NULL object onto the stack -> important because used to release state.
        ctxt.nullsOnStack.add(topStackSlotIdx + 1);
        ctxt.pureObjectsOnStack.put(topStackSlotIdx + 1,
                                    new HashSet<Integer>(Collections.singleton(topStackSlotIdx + 1)));
        break;
      case Opcodes.ICONST_M1:
      case Opcodes.ICONST_0:
      case Opcodes.ICONST_1:
      case Opcodes.ICONST_2:
      case Opcodes.ICONST_3:
      case Opcodes.ICONST_4:
      case Opcodes.ICONST_5:
      case Opcodes.LCONST_0:
      case Opcodes.LCONST_1:
      case Opcodes.FCONST_0:
      case Opcodes.FCONST_1:
      case Opcodes.FCONST_2:
      case Opcodes.DCONST_0:
      case Opcodes.DCONST_1:
      case Opcodes.BIPUSH:
      case Opcodes.SIPUSH:
      case Opcodes.LDC:
        // ... -> value
        // these instructions just load a value to the stack. -> we don't care.
        break;
      case Opcodes.ILOAD:
      case Opcodes.LLOAD:
      case Opcodes.FLOAD:
      case Opcodes.DLOAD:
        // ... -> value/object_ref
        // load primitive values from a local onto the stack. -> don't care.
        break;
      case Opcodes.ALOAD:
        // ... -> value/object_ref
        // loads reference from local onto stack -> important.
        VarInsnNode loadInstruction = (VarInsnNode) instruction;
        ctxt.localsOnStack.put(loadInstruction.var, topStackSlotIdx + 1);
        ctxt.pureObjectsOnStack.put(topStackSlotIdx + 1, new HashSet<Integer>());
        ctxt.pureObjectsOnStack.get(topStackSlotIdx + 1).add(topStackSlotIdx + 1);
        if(ctxt.taintedLocals.contains(loadInstruction.var)) {
          ctxt.taintedStackSlots.add(topStackSlotIdx + 1);
          if(ctxt.loadedFieldsToLocals.containsKey(loadInstruction.var)) {
            LoadContext ldCtxt =
                new LoadContext(instruction,
                                ctxt.loadedFieldsToLocals.get(loadInstruction.var)._field,
                                ctxt.currentFrame);
            ctxt.loadedFields.put(topStackSlotIdx + 1, ldCtxt);
          }
        }
        break;
      case Opcodes.IALOAD:
      case Opcodes.LALOAD:
      case Opcodes.FALOAD:
      case Opcodes.DALOAD:
        // array_ref idx -> value
        // these instructions load primitive value from an array. the top-most stack value marks
        // the index in the array. -> don't care.
        
        // we have to nevertheless wipe the tracking here because we might have tracked
        // the array_ref because it is a mutable object!
        cleanup(ctxt, topStackSlotIdx-1, topStackSlotIdx);
        break;
      case Opcodes.AALOAD:
        // array_ref idx -> object_ref
        // these instructions load data from an array. the top-most stack value marks the
        // index in the array. -> important.
        
        ctxt.arrayLoadInstructions.add((InsnNode) instruction);
        
        int arrayOrigin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx - 1);
        if(ctxt.taintedStackSlots.contains(arrayOrigin)) {
          if(isLinear(topStackSlotIdx, ctxt.methodName, linearity, _accessTracker)) {
            // don't taint but just sweep the stack slots as in the else branch -> TODO
            // refactoring needed!
            sweepEntries(ctxt.taintedStackSlots, topStackSlotIdx - 1, topStackSlotIdx);
          } else {
            ctxt.taintedStackSlots.add(topStackSlotIdx - 1);
            ctxt.pureObjectsOnStack.put(topStackSlotIdx - 1,
                                        new HashSet<Integer>(Collections.singleton(topStackSlotIdx - 1)));
            ctxt.loadedFields.put(topStackSlotIdx - 1, ctxt.loadedFields.get(arrayOrigin));
          }
        } else {
          sweepEntries(ctxt.taintedStackSlots, topStackSlotIdx - 1, topStackSlotIdx);
        }
        
        sweepEntries(ctxt.localsOnStack, topStackSlotIdx - 1, topStackSlotIdx);
        // we adjusted topStackSlotIdx-1 in the above statements
        sweepStackSlots(ctxt.pureObjectsOnStack, topStackSlotIdx, topStackSlotIdx);
        sweepEntries(ctxt.loadedFields, topStackSlotIdx, topStackSlotIdx);
        sweepEntries(ctxt.nullsOnStack, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.BALOAD:
      case Opcodes.CALOAD:
      case Opcodes.SALOAD:
        // array_ref idx -> value
        // these instructions load primitive value from an array. the top-most stack value marks
        // the
        // index in the array. -> don't care.
        break;
      case Opcodes.ISTORE:
      case Opcodes.LSTORE:
      case Opcodes.FSTORE:
      case Opcodes.DSTORE:
        // value -> ...
        // simple store operation to a local. -> don't care.
        break;
      case Opcodes.ASTORE:
        // object_ref -> ...
        // simple store operation to a local. -> important
        
        // if the object_ref is actually a loaded field then we have to track that local
        // now and we have to register the local field name!
        int origin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx);
        if(origin > -1 && ctxt.taintedStackSlots.contains(origin)) {
          // keep the operation that initially tainted this local. it allows us to handle
          // release operations on branches.
          if(!ctxt.taintedLocals.contains(((VarInsnNode) instruction).var)) {
            ctxt.taintedLocals.add(((VarInsnNode) instruction).var);
            LoadContext ldCtxt =
                new LoadContext(instruction, ctxt.loadedFields.get(origin)._field, ctxt.currentFrame);
            ctxt.loadedFieldsToLocals.put(((VarInsnNode) instruction).var, ldCtxt);
          }
        }
        
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.IASTORE:
      case Opcodes.LASTORE:
      case Opcodes.FASTORE:
      case Opcodes.DASTORE:
        // array_ref index value -> ...
        // store operation to an array of a primitive value.
        
        // the array is an object and therefore we have to clean up
        cleanup(ctxt, topStackSlotIdx - 2, topStackSlotIdx);
        break;
      case Opcodes.AASTORE:
        // array_ref index object_ref -> ...
        // store operation to an array of an object reference. -> important.
        
        origin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx);
        arrayOrigin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx - 2);
        
        // when the object ref is a null then we are releasing state here. we have to
        // untaint all references on the stack and in the locals.
        if(ctxt.taintedStackSlots.contains(arrayOrigin) && ctxt.nullsOnStack.contains(origin)) {
          _releaseAnalysis.releaseOnAASTORE(ctxt, (InsnNode) instruction, arrayOrigin);
        } else
        // if the object_ref is tainted then we have to taint all references of this array now!
        if(ctxt.taintedStackSlots.contains(origin)) {
          // stack
          arrayOrigin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx - 2);
          ctxt.taintedStackSlots.addAll(ctxt.pureObjectsOnStack.get(arrayOrigin));
          ctxt.loadedFields.put(arrayOrigin, ctxt.loadedFields.get(origin));
          
          // locals
          int arrayOriginLocal = findOriginLocal(ctxt.localsOnStack, arrayOrigin);
          if(arrayOriginLocal > -1) {
            ctxt.taintedLocals.add(arrayOriginLocal);
            LoadContext ldCtxt =
                new LoadContext(instruction, ctxt.loadedFields.get(origin)._field, ctxt.currentFrame);
            ctxt.loadedFieldsToLocals.put(arrayOriginLocal, ldCtxt);
          }
        }
        
        cleanup(ctxt, topStackSlotIdx - 2, topStackSlotIdx);
        break;
      case Opcodes.BASTORE:
      case Opcodes.CASTORE:
      case Opcodes.SASTORE:
        // array_ref index value -> ...
        // store operation to an array of a primitive value.
        
        // the array is an object and therefore we have to clean up
        cleanup(ctxt, topStackSlotIdx - 2, topStackSlotIdx);
        break;
      case Opcodes.POP:
        // value/object_ref -> ...
        // change the stack -> important.
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.POP2:
        // value/object_ref value/object_ref -> ...
        // change the stack -> important.
        cleanup(ctxt, topStackSlotIdx - 1, topStackSlotIdx);
        break;
      case Opcodes.DUP:
        // value/object_ref -> value/object_ref value/object_ref
        // duplicates object reference. -> important (if value is non-primitive).
        
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet())
          if(entry.getValue().contains(topStackSlotIdx)) entry.getValue().add(topStackSlotIdx + 1);
        
        // no need to handle map keys (of ctxt.pureObjectsOnStack etc.) as the origins don't
        // change
        if(ctxt.taintedStackSlots.contains(topStackSlotIdx)) ctxt.taintedStackSlots.add(topStackSlotIdx + 1);
        if(ctxt.nullsOnStack.contains(topStackSlotIdx)) ctxt.nullsOnStack.add(topStackSlotIdx + 1);
        
        break;
      case Opcodes.DUP_X1:
        // value2 value1 -> value1 value2 value1
        // inserts 'value1' two slots below on the stack. -> important.
        
        Set<Integer> value1StackSlots = null;
        Set<Integer> value2StackSlots = null;
        
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) {
          value1StackSlots.remove(topStackSlotIdx);
          value1StackSlots.add(topStackSlotIdx - 1);
          value1StackSlots.add(topStackSlotIdx + 1);
        }
        
        if(value2StackSlots != null) {
          value2StackSlots.remove(topStackSlotIdx - 1);
          value2StackSlots.add(topStackSlotIdx);
        }
        
        // in case the keys are changing
        trackDUP_X1(ctxt.pureObjectsOnStack, topStackSlotIdx);
        trackDUP_X1(ctxt.loadedFields, topStackSlotIdx);
        DFACommons.trackDUP_X1(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackDUP_X1(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.DUP_X2:
        // value3 value2 value1 -> value1 value3 value2 value1
        // same as above -> important.
        
        value1StackSlots = null;
        value2StackSlots = null;
        Set<Integer> value3StackSlots = null;
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 2)) value3StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) {
          value1StackSlots.remove(topStackSlotIdx);
          value1StackSlots.add(topStackSlotIdx - 2);
          value1StackSlots.add(topStackSlotIdx + 1);
        }
        
        if(value2StackSlots != null) {
          value2StackSlots.remove(topStackSlotIdx - 1);
          value2StackSlots.add(topStackSlotIdx);
        }
        
        if(value3StackSlots != null) {
          value3StackSlots.remove(topStackSlotIdx - 2);
          value3StackSlots.add(topStackSlotIdx - 1);
        }
        
        // in case the keys are changing
        trackDUP_X2(ctxt.pureObjectsOnStack, topStackSlotIdx);
        trackDUP_X2(ctxt.loadedFields, topStackSlotIdx);
        DFACommons.trackDUP_X2(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackDUP_X2(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.DUP2:
        // value2 value1 -> value2 value1 value2 value1
        // duplicates the two top-most slots. -> important.
        
        value1StackSlots = null;
        value2StackSlots = null;
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) value1StackSlots.add(topStackSlotIdx + 2);
        if(value2StackSlots != null) value2StackSlots.add(topStackSlotIdx + 1);
        
        // no need to handle map keys as the origins don't change
        DFACommons.trackDUP2(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackDUP2(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.DUP2_X1:
        // value3 value2 value1 -> value2 value1 value3 value2 value1
        // same as above -> important.
        
        value1StackSlots = null;
        value2StackSlots = null;
        value3StackSlots = null;
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 2)) value3StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) {
          value1StackSlots.remove(topStackSlotIdx);
          value1StackSlots.add(topStackSlotIdx - 1);
          value1StackSlots.add(topStackSlotIdx + 2);
        }
        
        if(value2StackSlots != null) {
          value2StackSlots.remove(topStackSlotIdx - 1);
          value2StackSlots.add(topStackSlotIdx - 2);
          value2StackSlots.add(topStackSlotIdx + 1);
        }
        
        if(value3StackSlots != null) {
          value3StackSlots.remove(topStackSlotIdx - 2);
          value3StackSlots.add(topStackSlotIdx);
        }
        
        // in case the keys are changing
        trackDUP2_X1(ctxt.pureObjectsOnStack, topStackSlotIdx);
        trackDUP2_X1(ctxt.loadedFields, topStackSlotIdx);
        DFACommons.trackDUP2_X1(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackDUP2_X1(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.DUP2_X2:
        // value4 value3 value2 value1 -> value2 value1 value4 value3 value2 value1
        // same as above -> important.
        
        value1StackSlots = null;
        value2StackSlots = null;
        value3StackSlots = null;
        Set<Integer> value4StackSlots = null;
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 2)) value3StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 3)) value4StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) {
          value1StackSlots.remove(topStackSlotIdx);
          value1StackSlots.add(topStackSlotIdx - 2);
          value1StackSlots.add(topStackSlotIdx + 2);
        }
        
        if(value2StackSlots != null) {
          value2StackSlots.remove(topStackSlotIdx - 1);
          value2StackSlots.add(topStackSlotIdx - 3);
          value2StackSlots.add(topStackSlotIdx + 1);
        }
        
        if(value3StackSlots != null) {
          value3StackSlots.remove(topStackSlotIdx - 2);
          value3StackSlots.add(topStackSlotIdx);
        }
        
        if(value4StackSlots != null) {
          value4StackSlots.remove(topStackSlotIdx - 3);
          value4StackSlots.add(topStackSlotIdx - 1);
        }
        
        // in case the keys are changing
        trackDUP2_X2(ctxt.pureObjectsOnStack, topStackSlotIdx);
        trackDUP2_X2(ctxt.loadedFields, topStackSlotIdx);
        DFACommons.trackDUP2_X2(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackDUP2_X2(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.SWAP:
        // value2 value1 -> value1 value2
        // stack size does not change but the slot of interest 'value2' just moved to the
        // slot below. -> important.
        
        value1StackSlots = null;
        value2StackSlots = null;
        // search first
        for(Map.Entry<Integer, Set<Integer>> entry : ctxt.pureObjectsOnStack.entrySet()) {
          if(entry.getValue().contains(topStackSlotIdx)) value1StackSlots = entry.getValue();
          if(entry.getValue().contains(topStackSlotIdx - 1)) value2StackSlots = entry.getValue();
        }
        
        if(value1StackSlots != null) {
          value1StackSlots.remove(topStackSlotIdx);
          value1StackSlots.add(topStackSlotIdx - 1);
        }
        if(value2StackSlots != null) {
          value2StackSlots.remove(topStackSlotIdx - 1);
          value2StackSlots.add(topStackSlotIdx);
        }
        
        // in case the keys are changing
        trackSWAP(ctxt.pureObjectsOnStack, topStackSlotIdx);
        trackSWAP(ctxt.loadedFields, topStackSlotIdx);
        DFACommons.trackSWAP(ctxt.taintedStackSlots, topStackSlotIdx);
        DFACommons.trackSWAP(ctxt.nullsOnStack, topStackSlotIdx);
        
        break;
      case Opcodes.IADD:
      case Opcodes.LADD:
      case Opcodes.FADD:
      case Opcodes.DADD:
      case Opcodes.ISUB:
      case Opcodes.LSUB:
      case Opcodes.FSUB:
      case Opcodes.DSUB:
      case Opcodes.IMUL:
      case Opcodes.LMUL:
      case Opcodes.FMUL:
      case Opcodes.DMUL:
      case Opcodes.IDIV:
      case Opcodes.LDIV:
      case Opcodes.FDIV:
      case Opcodes.DDIV:
      case Opcodes.IREM:
      case Opcodes.LREM:
      case Opcodes.FREM:
      case Opcodes.DREM:
        // value value -> result
        // none-object operation -> don't care.
        break;
      case Opcodes.INEG:
      case Opcodes.LNEG:
      case Opcodes.FNEG:
      case Opcodes.DNEG:
        // value -> result
        // none-object operation -> don't care.
        break;
      case Opcodes.ISHL:
      case Opcodes.LSHL:
        // value1 value2 -> result
        // shift left
        // none-object operation -> don't care.
        break;
      case Opcodes.ISHR:
      case Opcodes.LSHR:
      case Opcodes.IUSHR:
      case Opcodes.LUSHR:
        // value1 value2 -> result
        // shift right
        // none-object operation -> don't care.
        break;
      case Opcodes.IAND:
      case Opcodes.LAND:
      case Opcodes.IOR:
      case Opcodes.LOR:
      case Opcodes.IXOR:
      case Opcodes.LXOR:
        // value1 value2 -> result
        // boolean operations
        // none-object operation -> don't care.
        break;
      case Opcodes.IINC:
        // increments local variable with constant. no stack change
        // none-object operation -> don't care.
        break;
      case Opcodes.I2L:
      case Opcodes.I2F:
      case Opcodes.I2D:
      case Opcodes.L2I:
      case Opcodes.L2F:
      case Opcodes.L2D:
      case Opcodes.F2I:
      case Opcodes.F2L:
      case Opcodes.F2D:
      case Opcodes.D2I:
      case Opcodes.D2L:
      case Opcodes.D2F:
      case Opcodes.I2B:
      case Opcodes.I2C:
      case Opcodes.I2S:
        // value -> result
        // type conversion for primitive types
        // none-object operation -> don't care.
        break;
      case Opcodes.LCMP:
      case Opcodes.FCMPL:
      case Opcodes.FCMPG:
      case Opcodes.DCMPL:
      case Opcodes.DCMPG:
        // value value -> result
        // comparison operations for primitive types
        // none-object operation -> don't care.
        break;
      case Opcodes.IFEQ:
      case Opcodes.IFNE:
      case Opcodes.IFLT:
      case Opcodes.IFGE:
      case Opcodes.IFGT:
      case Opcodes.IFLE:
        // value -> ...
        // conditional jump
        
        ctxt.currentBlock = findLabelForInstruction(instruction);
        ctxt.blockEntries.add(((JumpInsnNode) instruction).label);
        
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.IF_ICMPEQ:
      case Opcodes.IF_ICMPNE:
      case Opcodes.IF_ICMPLT:
      case Opcodes.IF_ICMPGE:
      case Opcodes.IF_ICMPGT:
      case Opcodes.IF_ICMPLE:
      case Opcodes.IF_ACMPEQ:
      case Opcodes.IF_ACMPNE:
        // value value -> ...
        // conditional jump
        
        ctxt.currentBlock = findLabelForInstruction(instruction);
        ctxt.blockEntries.add(((JumpInsnNode) instruction).label);
        
        // just sweep the stack
        cleanup(ctxt, topStackSlotIdx - 1, topStackSlotIdx);
        break;
      case Opcodes.GOTO:
        // [no stack change]
        // unconditional jump
        // nothing to be done here. (afterwards a stack frame instruction will follow)
        
        ctxt.blockEntries.add(((JumpInsnNode) instruction).label);
        break;
      case Opcodes.JSR:
        // ... -> address
        // jump to sub routine
        assert false : "Not sure what to do here. Let me see an example.";
        break;
      case Opcodes.RET:
        // [no stack change]
        // continues execution from address in local variable
        // nothing seems to change -> don't care.
        wipeStack(ctxt);
        break;
      case Opcodes.TABLESWITCH:
      case Opcodes.LOOKUPSWITCH:
        // index/key ->
        // jump to instruction of the address looked up
        // seems like this does not matter to us because we always want to go throw all frames
        // of the switch statement
        
        // TODO handle blocks here!
        
        break;
      case Opcodes.IRETURN:
      case Opcodes.LRETURN:
      case Opcodes.FRETURN:
      case Opcodes.DRETURN:
        // [primitive value] -> empty
        
        wipeStack(ctxt);
        break;
      case Opcodes.ARETURN:
        // [value] -> empty
        // important.
        
        if(ctxt.taintedStackSlots.contains(topStackSlotIdx)) {
          LeakDetectionException e = new LeakDetectionException();
          origin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx);
          if(ctxt.loadedFields.containsKey(origin)) e._fieldLeaked = ctxt.loadedFields.get(origin)._field;
          // it must have been a tainted local
          else e._leakedTaintedLocal = findOriginLocal(ctxt.localsOnStack, origin);
          throw e;
        }
        
        wipeStack(ctxt);
        break;
      case Opcodes.RETURN:
        // -> empty
        // void return
        
        // nothing to be done. when can only leak through the arguments and therefore we would
        // have never arrived here if that was the case.
        
        wipeStack(ctxt);
        break;
      case Opcodes.GETSTATIC:
        // -> value
        // load value from static field -> important.
        
        // TODO we analyze this here via class loading and reflection but if we parse this whole
        // class as a class node in the first place then we should not need to do so!
        try {
          String internalName = ((FieldInsnNode) instruction).owner;
          Class<?> cls = Class.forName(Type.getObjectType(internalName).getClassName());
          // also support calling fields from super classes
          Field f = getField(((FieldInsnNode) instruction).name, cls);
          String fieldType = Type.getType(((FieldInsnNode) instruction).desc).getClassName();
          // Synthetic fields are primarily used by the compiler for switch cases etc. The
          // compiler turns a switch statement into some sort of mapping which is stored with
          // the class file. There are various compiler differences when it comes to synthetic
          // fields. Some use $SWITCH_TABLE$ variables and others use $SwitchMap$. Let's just
          // skip all that hassle!
          if(f.isSynthetic()
             || (Modifier.isFinal(f.getModifiers()) && cls.isEnum() || (AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(fieldType) || KnowledgeBase.getInstance().isFieldUntainted(cls.getName(),
                                                                                                                                                                                     f.getName()))))
          {
            // read access to an immutable final field is ok and does not need to be tracked.
          } else {
            LeakDetectionException leak = new LeakDetectionException();
            leak._fieldLeaked = cls.getName() + "." + ((FieldInsnNode) instruction).name;
            throw leak;
          }
        }
        catch(ClassNotFoundException | NoSuchFieldException | SecurityException e1) {
          e1.printStackTrace();
          // TODO turn this into a proper analysis exception
          throw new RuntimeException(e1);
        }
        break;
      case Opcodes.PUTSTATIC:
        // value ->
        // store value to static field -> important.
        
        // we do not support an put access to a static field of any type
        LeakDetectionException leak = new LeakDetectionException();
        leak._fieldLeaked = ((FieldInsnNode) instruction).name;
        throw leak;
        
      case Opcodes.GETFIELD:
        // object_ref -> value
        // load value from object field -> important.
        
        Type t = Type.getType(((FieldInsnNode) instruction).desc);
        if(ctxt.taintedStackSlots.contains(topStackSlotIdx)) {
          // if the loaded field is not an object then just don't track that stack location
          // anymore.
          
          if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName())) {
            String field = ((FieldInsnNode) instruction).name;
            LoadContext ldCtxt = new LoadContext(instruction, field, ctxt.currentFrame);
            ctxt.loadedFields.put(topStackSlotIdx, ldCtxt);
            ctxt.pureObjectsOnStack.put(topStackSlotIdx,
                                        new HashSet<Integer>(Collections.singleton(topStackSlotIdx)));
          } else {
            // it's an immutable type so it's ok to kick off all info
            cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
          }
        } else {
          // we can kick this off only if it turns into an immutable type!
          if(AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName())) sweepStackSlots(ctxt.pureObjectsOnStack,
                                                                                                  topStackSlotIdx,
                                                                                                  topStackSlotIdx);
          sweepEntries(ctxt.localsOnStack, topStackSlotIdx, topStackSlotIdx);
          sweepEntries(ctxt.loadedFields, topStackSlotIdx, topStackSlotIdx);
        }
        
        break;
      case Opcodes.PUTFIELD:
        // object_ref value ->
        // store value to object field -> important.
        
        t = Type.getType(((FieldInsnNode) instruction).desc);
        if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName())) {
          
          int valueOrigin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx);
          assert valueOrigin > -1 : "Because all objects should be tracked.";
          
          // maybe this is a release operation
          _releaseAnalysis.releaseOnPUTFIELD(ctxt, (FieldInsnNode) instruction, valueOrigin);
          
          // FIXME if this is a tainted argument object than writing to it is just as
          // returning a tainted object. hence, we leak state here and should therefore throw a
          // LeakDetectionException right away!
          int objectOrigin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx - 1);
          // (the object origin should better not be NULL -> NPE)
          
          // check the value first ... (taint the target object)
          if(ctxt.taintedStackSlots.contains(valueOrigin)) {
            ctxt.taintedStackSlots.addAll(ctxt.pureObjectsOnStack.get(objectOrigin));
            int objectOriginLocal = findOriginLocal(ctxt.localsOnStack, objectOrigin);
            if(objectOriginLocal > -1) {
              ctxt.taintedLocals.add(objectOriginLocal);
              LoadContext ldCtxt =
                  new LoadContext(instruction, ctxt.loadedFields.get(valueOrigin)._field, ctxt.currentFrame);
              ctxt.loadedFieldsToLocals.put(objectOriginLocal, ldCtxt);
            }
            ctxt.loadedFields.put(objectOrigin, ctxt.loadedFields.get(valueOrigin));
          }
          
          // ... then the target object. (case of a side-effect to local state that is later
          // shared!)
          if(ctxt.taintedStackSlots.contains(objectOrigin)) {
            ctxt.taintedStackSlots.addAll(ctxt.pureObjectsOnStack.get(valueOrigin));
            int valueOriginLocal = findOriginLocal(ctxt.localsOnStack, valueOrigin);
            if(valueOriginLocal > -1) {
              ctxt.taintedLocals.add(valueOriginLocal);
              LoadContext ldCtxt =
                  new LoadContext(instruction, ((FieldInsnNode) instruction).name, ctxt.currentFrame);
              ctxt.loadedFieldsToLocals.put(valueOriginLocal, ldCtxt);
            }
            LoadContext ldCtxt =
                new LoadContext(instruction, ((FieldInsnNode) instruction).name, ctxt.currentFrame);
            ctxt.loadedFields.put(valueOrigin, ldCtxt);
          }
        }
        
        // stack cleanup
        cleanup(ctxt, topStackSlotIdx - 1, topStackSlotIdx);
        break;
      case Opcodes.INVOKEVIRTUAL:
      case Opcodes.INVOKESPECIAL:
      case Opcodes.INVOKEINTERFACE:
        // object_ref[args] -> result
        // function invocation -> important.
        MethodInsnNode methodInvocation = (MethodInsnNode) instruction;
        int argCount = Type.getArgumentTypes(methodInvocation.desc).length;
        Result result = _invokeAnalysis.handleINVOKE(ctxt, topStackSlotIdx, methodInvocation, argCount);
        String leakedField = null;
        switch(result) {
          case SAFE: // all good
            break;
          case SKIPPED:
            skippedMethods.add(methodInvocation.owner + "." + methodInvocation.name);
            break;
          case UNSAFE:
            leakedField = result.getResult();
            break;
        }
        Set<String> skipped = result.getSkipped();
        if(skipped != null) skippedMethods.addAll(skipped);
        
        // the object_ref also gets kicked off the stack not only the args!
        cleanup(ctxt, topStackSlotIdx - argCount, topStackSlotIdx);
        // ... but put the result onto the stack (in case it is a mutable object)
        Type retType = Type.getReturnType(methodInvocation.desc);
        if(retType != Type.VOID_TYPE && !AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(retType.getClassName()))
        {
          // TODO this has no origin. what if somebody asks for it?!
          ctxt.pureObjectsOnStack.put(topStackSlotIdx - argCount,
                                      new HashSet<>(Collections.singleton(topStackSlotIdx - argCount)));
        }
        
        if(leakedField != null && retType != Type.VOID_TYPE) {
          if(argCount == 1 && isLinear(topStackSlotIdx, ctxt.methodName, linearity, _accessTracker)) {
            // don't taint it if it only used a linear parameter!
            // TODO this function should probably also be tagged as pure!
          } else {
            LoadContext ldCtxt = new LoadContext(instruction, leakedField, ctxt.currentFrame);
            ctxt.loadedFields.put(topStackSlotIdx - argCount, ldCtxt);
            ctxt.taintedStackSlots.add(topStackSlotIdx - argCount);
            ctxt.pureObjectsOnStack.put(topStackSlotIdx - argCount,
                                        new HashSet<>(Collections.singleton(topStackSlotIdx - argCount)));
          }
        }
        break;
      case Opcodes.INVOKESTATIC:
        // [args] -> result
        // function invocation -> important.
        methodInvocation = (MethodInsnNode) instruction;
        argCount = Type.getArgumentTypes(methodInvocation.desc).length;
        result = _invokeAnalysis.handleINVOKE(ctxt, topStackSlotIdx, methodInvocation, argCount);
        leakedField = null;
        switch(result) {
          case SAFE: // all good
            break;
          case SKIPPED:
            skippedMethods.add(methodInvocation.owner + "." + methodInvocation.name);
            break;
          case UNSAFE:
            leakedField = result.getResult();
            break;
        }
        skipped = result.getSkipped();
        if(skipped != null) skippedMethods.addAll(skipped);
        
        // kick the args off the stack
        cleanup(ctxt, topStackSlotIdx - argCount + 1, topStackSlotIdx);
        // ... but put the result onto the stack (in case it is a mutable object)
        retType = Type.getReturnType(methodInvocation.desc);
        if(retType != Type.VOID_TYPE && !AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(retType.getClassName()))
        {
          // TODO this has no origin. what if somebody asks for it?!
          ctxt.pureObjectsOnStack.put(topStackSlotIdx - argCount + 1,
                                      new HashSet<>(Collections.singleton(topStackSlotIdx - argCount + 1)));
        }
        
        if(leakedField != null && retType != Type.VOID_TYPE) {
          if(argCount == 1 && isLinear(topStackSlotIdx, ctxt.methodName, linearity, _accessTracker)) {
            // don't taint it if it only used a linear parameter!
            // TODO this function should probably also be tagged as pure!
          } else {
            LoadContext ldCtxt = new LoadContext(instruction, leakedField, ctxt.currentFrame);
            ctxt.loadedFields.put(topStackSlotIdx - argCount + 1, ldCtxt);
            ctxt.taintedStackSlots.add(topStackSlotIdx - argCount + 1);
            ctxt.pureObjectsOnStack.put(topStackSlotIdx - argCount + 1,
                                        new HashSet<>(Collections.singleton(topStackSlotIdx - argCount + 1)));
          }
        }
        break;
      case Opcodes.INVOKEDYNAMIC:
        // [args] -> result
        // function invocation -> important.
        assert false : "unsupported operation";
        break;
      case Opcodes.NEW:
        // -> objectref
        // object creation -> important.
        
        ctxt.pureObjectsOnStack.put(topStackSlotIdx + 1,
                                    new HashSet<Integer>(Collections.singleton(topStackSlotIdx + 1)));
        break;
      case Opcodes.NEWARRAY:
      case Opcodes.ANEWARRAY:
        // count -> array_ref
        // array creation -> important.
        
        ctxt.pureObjectsOnStack.put(topStackSlotIdx, new HashSet<Integer>(Collections.singleton(topStackSlotIdx)));
        break;
      case Opcodes.ARRAYLENGTH:
        // array_ref -> length
        // this swaps the array reference for its length. we don't track the length integer but
        // the array is tracked and might have been tainted. so do the stack cleanup.
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.ATHROW:
        // object_ref -> [empty] object_ref
        // treated like a 'return' statement.
        
        if(ctxt.taintedStackSlots.contains(topStackSlotIdx)) {
          LeakDetectionException e = new LeakDetectionException();
          origin = findOrigin(ctxt.pureObjectsOnStack, topStackSlotIdx);
          if(ctxt.loadedFields.containsKey(origin)) e._fieldLeaked = ctxt.loadedFields.get(origin)._field;
          // it must have been a tainted local
          else e._leakedTaintedLocal = findOriginLocal(ctxt.localsOnStack, origin);
          throw e;
        }
        break;
      case Opcodes.CHECKCAST:
        // object_ref -> object_ref
        // (no stack change) -> don't care.
        if(_releaseAnalysis.isUntainted(instruction)) ctxt.taintedStackSlots.remove(topStackSlotIdx);
        break;
      case Opcodes.INSTANCEOF:
        // object_ref -> result
        // clean the stack because we do not track the boolean result!
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.MONITORENTER:
      case Opcodes.MONITOREXIT:
        // object_ref ->
        // clean the stack!
        cleanup(ctxt, topStackSlotIdx, topStackSlotIdx);
        break;
      case Opcodes.MULTIANEWARRAY:
        // [count1 count2] -> array_ref
        // creates n-dimensional array -> important.
        int arrayRefSlot = topStackSlotIdx - ((MultiANewArrayInsnNode) instruction).dims + 1;
        ctxt.pureObjectsOnStack.put(arrayRefSlot, new HashSet<Integer>(arrayRefSlot));
        break;
      case Opcodes.IFNULL:
      case Opcodes.IFNONNULL:
        // value ->
        // conditional jump -> important.
        
        ctxt.currentBlock = findLabelForInstruction(instruction);
        ctxt.blockEntries.add(((JumpInsnNode) instruction).label);
        
        sweepStackSlots(ctxt.pureObjectsOnStack, topStackSlotIdx, topStackSlotIdx);
        sweepEntries(ctxt.localsOnStack, topStackSlotIdx, topStackSlotIdx);
        sweepEntries(ctxt.loadedFields, topStackSlotIdx, topStackSlotIdx);
        sweepEntries(ctxt.taintedStackSlots, topStackSlotIdx, topStackSlotIdx);
        break;
      default:
        assert false : "Discovered unknown instruction. Opcode: " + instruction.getOpcode() + " type: "
                             + instruction.getType();
    }
    return skippedMethods;
  }
  
  private int getMaxSlot(Set<Integer> slots) {
    int max = -1;
    for(Integer slot : slots)
      if(slot > max) max = slot;
    return max;
  }
  
  private LabelNode findLabelForInstruction(AbstractInsnNode instruction) {
    AbstractInsnNode prev = instruction.getPrevious();
    while(prev != null) {
      if(prev instanceof LabelNode) return (LabelNode) prev;
      else prev = prev.getPrevious();
    }
    assert false : "No label found for instruction: " + instruction.getType();
    return null;
  }
  
  // value2 value1 -> value1 value2
  private <T> void trackSWAP(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx - 1, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx, value2StackSlots);
    }
  }
    
  // value4 value3 value2 value1 -> value2 value1 value4 value3 value2 value1
  private <T> void trackDUP2_X2(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    T value3StackSlots = registry.remove(topStackSlotIdx - 2);
    T value4StackSlots = registry.remove(topStackSlotIdx - 3);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx + 2, value1StackSlots);
      registry.put(topStackSlotIdx - 2, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx + 1, value2StackSlots);
      registry.put(topStackSlotIdx - 3, value2StackSlots);
    }
    
    if(value3StackSlots != null) {
      registry.put(topStackSlotIdx, value3StackSlots);
    }
    
    if(value4StackSlots != null) {
      registry.put(topStackSlotIdx - 1, value4StackSlots);
    }
  }
    
  // value3 value2 value1 -> value2 value1 value3 value2 value1
  private <T> void trackDUP2_X1(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    T value3StackSlots = registry.remove(topStackSlotIdx - 2);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx + 2, value1StackSlots);
      registry.put(topStackSlotIdx - 1, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx + 1, value2StackSlots);
      registry.put(topStackSlotIdx - 2, value2StackSlots);
    }
    
    if(value3StackSlots != null) {
      registry.put(topStackSlotIdx, value3StackSlots);
    }
  }
      
  // value3 value2 value1 -> value1 value3 value2 value1
  private <T> void trackDUP_X2(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    T value3StackSlots = registry.remove(topStackSlotIdx - 2);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx + 1, value1StackSlots);
      registry.put(topStackSlotIdx - 2, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx, value2StackSlots);
    }
    
    if(value3StackSlots != null) {
      registry.put(topStackSlotIdx - 1, value3StackSlots);
    }
  }
      
  // value2 value1 -> value1 value2 value1
  private <T> void trackDUP_X1(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx - 1, value1StackSlots);
      registry.put(topStackSlotIdx + 1, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx, value2StackSlots);
    }
  }
  
  private boolean isLinear(int stackSlot, String methodName, LinearityAnalysis linearity, AccessTracker tracker) {
    String idxField = tracker.getStackSlotField(stackSlot);
    int localIdx = tracker.getStackSlotLocal(stackSlot);
    return (idxField != null && linearity.getLinearFields().contains(idxField))
           || (localIdx > -1 && linearity.getLinearParameters(methodName).contains(localIdx));
  }
  
  private void sweepStackSlots(Map<Integer, Set<Integer>> registry, int start, int stop) {
    for(int i = start; i <= stop; i++)
      for(Map.Entry<Integer, Set<Integer>> entry : registry.entrySet())
        entry.getValue().remove(i);
  }
  
  private void sweepEntries(Map<Integer, ?> registry, int start, int stop) {
    for(int i = start; i <= stop; i++)
      registry.remove(i);
  }
  
  private void sweepEntries(Set<Integer> registry, int start, int stop) {
    for(int i = start; i <= stop; i++)
      registry.remove(i);
  }
  
  private void wipeStack(Context ctxt) {
    ctxt.localsOnStack.clear();
    ctxt.pureObjectsOnStack.clear();
    ctxt.taintedStackSlots.clear();
    ctxt.nullsOnStack.clear();
  }
  
  private void cleanup(Context ctxt, int start, int stop) {
    sweepStackSlots(ctxt.pureObjectsOnStack, start, stop);
    sweepEntries(ctxt.localsOnStack, start, stop);
    sweepEntries(ctxt.loadedFields, start, stop);
    sweepEntries(ctxt.taintedStackSlots, start, stop);
    sweepEntries(ctxt.nullsOnStack, start, stop);
  }
  
  /**
   * Checks all classes up the hierarchy for all fields, not only public ones.
   * @param name
   * @param clz
   * @return
   * @throws NoSuchFieldException
   */
  private Field getField(String name, Class<?> clz) throws NoSuchFieldException {
    try {
      return clz.getDeclaredField(name);
    }
    catch(NoSuchFieldException n) {
      Class<?> parent = clz.getSuperclass();
      if(parent == null) throw new NoSuchFieldException(name);
      else return getField(name, clz.getSuperclass());
    }
  }
}
