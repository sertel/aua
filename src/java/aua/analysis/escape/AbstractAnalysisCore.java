/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.Map;
import java.util.Set;

public abstract class AbstractAnalysisCore
{ 
  protected final int findOrigin(Map<Integer, Set<Integer>> registry, int slot) {
    for(Map.Entry<Integer, Set<Integer>> entry : registry.entrySet())
      if(entry.getValue().contains(slot)) return entry.getKey();
    
    return -1;
  }

  protected final int findOriginLocal(Map<Integer, Integer> registry, int slot) {
    for(Map.Entry<Integer, Integer> entry : registry.entrySet())
      if(entry.getValue() == slot) return entry.getKey();
    
    return -1;
  }

}
