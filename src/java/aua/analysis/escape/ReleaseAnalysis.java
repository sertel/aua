/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.TypeAnnotationNode;

import aua.analysis.escape.EscapeAnalysisCore.Context;
import aua.analysis.escape.EscapeAnalysisCore.LoadContext;
import aua.analysis.escape.LocalVariableAnalysis.LocalVariableAnnotation;
import aua.analysis.qual.Untainted;

/**
 * The context of this analysis is based on the line of operation in the source code. The byte
 * code creates labels for each line. As soon as the label (line number instruction) of the
 * current instruction sequence is arrived, the analysis is finished. Hence, the programmer is
 * required to write clean/comparable code when trying to release state.
 * 
 * @author sertel
 * 
 */
public class ReleaseAnalysis {
  
  private Set<Integer> _untaintedLocals = new HashSet<>();
  
  private LocalVariableAnalysis _localVariableAnnotations = null;
  
  protected ReleaseAnalysis(LocalVariableAnalysis localVariableAnnotations) {
    _localVariableAnnotations = localVariableAnnotations;
  }
  
  protected boolean isUntainted(AbstractInsnNode ins) {
    if(ins.visibleTypeAnnotations == null) return false;
    
    for(int i = 0; i < ins.visibleTypeAnnotations.size(); i++) {
      TypeAnnotationNode anno = (TypeAnnotationNode) ins.visibleTypeAnnotations.get(i);
      System.out.println("Annotation detected: " + Type.getType(anno.desc).getClassName());
      if(Type.getType(anno.desc).getClassName().equals(Untainted.class.getName())) {
        return true;
      }
    }
    return false;
  }
  
  protected void releaseOnAASTORE(Context ctxt, InsnNode instruction, int arrayOrigin) {
    // for that we need all references to the instructions that access this array
    for(InsnNode loadIns : ctxt.arrayLoadInstructions) {
      if(compare(loadIns, instruction)) {
        // untaint here! need to untaint a local here because it needs a separate
        // operation to take the item out of the array!
        // untaint stack slots/locals
        String ref = ctxt.loadedFields.get(arrayOrigin)._field;
        untaintStackSlots(ctxt, ref);
        untaintLocals(ctxt, ref);
        break;
      }
    }
  }
  
  protected void releaseOnPUTFIELD(Context ctxt, FieldInsnNode instruction, int valueOrigin) {
    if(ctxt.nullsOnStack.contains(valueOrigin) // explicit release
       || !ctxt.taintedStackSlots.contains(valueOrigin) // overwrite/update
    )
    {
      // untaint stack slots
      untaintStackSlots(ctxt, instruction.name);
      
      // untaint locals
      untaintLocals(ctxt, instruction.name);
    }
  }
  
  private void untaintStackSlots(Context ctxt, String ref) {
    for(Map.Entry<Integer, LoadContext> entry : ctxt.loadedFields.entrySet())
      if(entry.getValue()._field.equals(ref)) ctxt.taintedStackSlots.remove(entry.getKey());
  }
  
  private void untaintLocals(Context ctxt, String ref) {
    for(Map.Entry<Integer, LoadContext> entry : ctxt.loadedFieldsToLocals.entrySet())
      if(entry.getValue()._field.equals(ref)) {
        ctxt.taintedLocals.remove(entry.getKey());
        if(!ctxt.blockEntries.isEmpty()) {
          _untaintedLocals.add(entry.getKey());
        }
      }
  }
  
  /**
   * We taint the untainted locals again. With respect to the untainted stack slots, the
   * assumption is that on block switch the stack gets voided anyways.
   * 
   * @param ctxt
   * @param poppedBranch
   */
  public void notifyBlockEntry(Context ctxt, LabelNode poppedBranch, LabelNode newLabel) {
    // taint these locals again!
    ctxt.taintedLocals.addAll(_untaintedLocals);
    _untaintedLocals.clear();
  }
  
  private boolean compare(InsnNode loadIns, InsnNode storeIns) {
    AbstractInsnNode currentLoadIns = loadIns.getPrevious();
    AbstractInsnNode currentStoreIns = storeIns.getPrevious();
    // skip the null load
    assert currentStoreIns.getOpcode() == Opcodes.ACONST_NULL;
    currentStoreIns = currentStoreIns.getPrevious();
    while(currentLoadIns.getType() == AbstractInsnNode.LINE || currentStoreIns.getType() == AbstractInsnNode.LINE)
    {
      // basic comparisons first
      if(currentLoadIns.getType() != currentStoreIns.getType()) return false;
      if(currentLoadIns.getOpcode() != currentStoreIns.getOpcode()) return false;
      if(!deepCompare(currentLoadIns, currentStoreIns)) return false;
      
      currentLoadIns = currentLoadIns.getPrevious();
      currentStoreIns = currentStoreIns.getPrevious();
    }
    // make sure both are the line numbers
    return currentLoadIns.getType() == currentStoreIns.getType();
  }
  
  private boolean deepCompare(AbstractInsnNode currentLoadIns, AbstractInsnNode currentStoreIns) {
    // TODO whenever function calls occur then take release patterns into account! -> this
    // requires that our analysis expands beyond a single line.
    return InstructionComparison.equal(currentLoadIns, currentStoreIns);
  }
  
  public void notifyLabel(Context ctxt, LabelNode newLabel) {
    // manage local variable annotations (in ASM label objects are shared)
    for(int i = 0; i < _localVariableAnnotations._annotations.size(); i++) {
      LocalVariableAnnotation locVarAnn = _localVariableAnnotations._annotations.get(i);
      for(int j = 0; j < locVarAnn._start.length; j++)
        if(locVarAnn._start[j] == newLabel) ctxt.taintedLocals.remove(locVarAnn._index[j]);
      
      // we do not handle the end because the tainting is (re)done by the byte code itself.
    }
  }
  
}
