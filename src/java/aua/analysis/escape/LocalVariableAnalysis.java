/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.tree.LabelNode;

import aua.analysis.qual.Untainted;

public class LocalVariableAnalysis extends ClassVisitor {
  
  class LocalVariableAnnotation {
    int _typeRef;
    TypePath _typePath;
    LabelNode[] _start;
    LabelNode[] _end;
    int[] _index;
    String _desc;
    boolean _visible;
    
    public LocalVariableAnnotation(int typeRef,
                                   TypePath typePath,
                                   Label[] start,
                                   Label[] end,
                                   int[] index,
                                   String desc,
                                   boolean visible)
    {
      _typeRef = typeRef;
      _typePath = typePath;
      // the label are worth nothing. what we need are the label nodes which are shared such
      // that we can perform a comparison.
      _start = new LabelNode[start.length];
      for(int i = 0; i < start.length; i++) {
        assert start[i] != null && start[i].info instanceof LabelNode;
        _start[i] = (LabelNode) start[i].info;
//        System.out.println("start >> " + _start[i]);
      }
      _end = new LabelNode[end.length];
      for(int i = 0; i < end.length; i++) {
        assert end[i] != null && end[i].info instanceof LabelNode;
        _end[i] = (LabelNode) end[i].info;
//        System.out.println("end >> " + _end[i]);
      }
      _index = index;
      _desc = desc;
      _visible = visible;
    }
  }
  
  protected List<LocalVariableAnnotation> _annotations = new ArrayList<>();
  
  class TypeAnnotationExtration extends MethodVisitor {
    
    public TypeAnnotationExtration(int api, MethodVisitor mv) {
      super(api, mv);
    }
    
    public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start,
                                                          Label[] end, int[] index, String desc, boolean visible)
    {
      AnnotationVisitor visitor =
          super.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, desc, visible);
      if(desc.equals(Type.getDescriptor(Untainted.class))) {
        _annotations.add(new LocalVariableAnnotation(typeRef, typePath, start, end, index, desc, visible));
      }
      return visitor;
    }
  }
  
  public LocalVariableAnalysis(int api, ClassVisitor cv) {
    super(api, cv);
  }
  
  public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
    MethodVisitor target = super.visitMethod(access, name, desc, signature, exceptions);
    return new TypeAnnotationExtration(api, target);
  }
}
