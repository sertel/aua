/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.MethodNode;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.qual.Linear;


public class LinearityAnalysis extends ClassVisitor {
  // results
  private Set<String> _linearFields = new HashSet<>();
  private Map<String, Set<Integer>> _linearParameters = new HashMap<>();
  
  private List<String> _trackedFields = new ArrayList<>();
  
  private class FieldLinearityChecker extends FieldVisitor {
    private String _fieldName = null;
    
    public FieldLinearityChecker(int api, FieldVisitor fv, String fieldName) {
      super(api, fv);
      _fieldName = fieldName;
    }
    
    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
      if(desc.equals(Type.getDescriptor(Linear.class))) _linearFields.add(_fieldName);
      return super.visitAnnotation(desc, visible);
    }
  }
  
  private class MethodLinearityChecker extends MethodVisitor {
    
    public MethodLinearityChecker(int api, MethodVisitor mv) {
      super(api, mv);
    }
    
    /**
     * The pattern that we are looking for is:<br>
     * GETFIELD<br>
     * ICONST_X<br>
     * IADD or ISUB<br>
     * PUTFIELD<br>
     * All of this in the same block. This means that in this block the access is linear.
     */
    // Normally, we would also have to check for an if-condition where the same field
    // participates. Otherwise an overflow may appear.
    public void visitEnd() {
      super.visitEnd();
      // TODO evaluate the gathered instructions here to provide support for implicit linearity
      // detection!
      MethodNode node = (MethodNode) super.mv;
      _linearParameters.put(node.name, getLinearParameters(node));
    }
    
    private Set<Integer> getLinearParameters(MethodNode m) {
      Set<Integer> linearParams = new HashSet<>();
      if(m.visibleParameterAnnotations != null) {
        // if a parameter has no annotations then this array has a null entry
        for(int i = 0; i < m.visibleParameterAnnotations.length; i++) {
          @SuppressWarnings("unchecked") List<AnnotationNode> annos = m.visibleParameterAnnotations[i];
          if(annos != null) {
            for(AnnotationNode anno : annos) {
              if(anno.desc.equals(Type.getDescriptor(Linear.class))) {
                // parameters are registered right after "this" as local variables. afterwards
                // the other local variables are registered.
                linearParams.add(i + 1);
              }
            }
          }
        }
      }
      return linearParams;
    }
  }
  
  public LinearityAnalysis(int api, ClassVisitor cv) {
    super(api, cv);
  }
  
  public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
    Type t = Type.getType(desc);
    if(AbstractOperatorAnalysis.NUMBER_TYPES.contains(t.getClassName())) {
      _trackedFields.add(name);
      return new FieldLinearityChecker(super.api, super.visitField(access, name, desc, signature, value), name);
    } else {
      return super.visitField(access, name, desc, signature, value);
    }
  }
  
  public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
    MethodVisitor target = super.visitMethod(access, name, desc, signature, exceptions);
    MethodNode n =
        target instanceof MethodNode ? (MethodNode) target : new MethodNode(access,
                                                                            name,
                                                                            desc,
                                                                            signature,
                                                                            exceptions);
    return new MethodLinearityChecker(super.api, n);
  }
  
  protected Set<String> getLinearFields() {
    return _linearFields;
  }
  
  public Set<Integer> getLinearParameters(String methodName) {
    return _linearParameters.get(methodName);
  }
}
