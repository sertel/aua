/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import org.objectweb.asm.tree.AbstractInsnNode;

public interface ByteCodeAnalysis
{ 
  void handleInstruction(AbstractInsnNode instruction, int topStackSlotIdx, int j);
}
