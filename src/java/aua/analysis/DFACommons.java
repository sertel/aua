/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis;

import java.util.Map;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

/**
 * Common (propagation) functionality for a forward directed data flow analysis.
 * 
 * @author sertel
 *
 */
public abstract class DFACommons {
  
  /**
   * Make sure that this analysis is used on the basic block level. The outer code is
   * responsible for carrying over the tainted stack slots and locals to the predecessor
   * block(s).
   * 
   * @param instruction
   * @param topStackSlotIdx
   * @param stackSlots
   * @param localSlots
   */
  public static void trackForward(AbstractInsnNode instruction, int topStackSlotIdx, Set<Integer> stackSlots,
                                  Set<Integer> localSlots)
  {
    switch(instruction.getOpcode()) {
      case Opcodes.SWAP:
        trackSWAP(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP:
        trackDUP(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP_X1:
        trackDUP_X1(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP_X2:
        trackDUP_X2(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP2:
        trackDUP2(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP2_X1:
        trackDUP2_X1(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.DUP2_X2:
        trackDUP2_X2(stackSlots, topStackSlotIdx);
        break;
      case Opcodes.ALOAD:
        trackALOAD((VarInsnNode) instruction, stackSlots, localSlots, topStackSlotIdx);
        break;
      case Opcodes.ASTORE:
        trackASTORE((VarInsnNode) instruction, stackSlots, localSlots, topStackSlotIdx);
        break;
    }
  }
  
  public static void trackASTORE(VarInsnNode astore, Set<Integer> stackSlots, Set<Integer> localSlots,
                                 int topStackSlotIdx)
  {
    if(stackSlots.contains(topStackSlotIdx)) localSlots.add(astore.var);
    stackSlots.remove(topStackSlotIdx);
  }
  
  public static void trackALOAD(VarInsnNode aload, Set<Integer> stackSlots, Set<Integer> localSlots,
                                int topStackSlotIdx)
  {
    if(localSlots.contains(aload.var)) stackSlots.add(topStackSlotIdx + 1);
  }
  
  public static void cleanTrackedStackSlots(Set<Integer> stackSlots, int stackSizeBefore, int stackSizeAfter) {
    for(int t = stackSizeBefore; t > stackSizeAfter; t--)
      stackSlots.remove(t);
  }
  
  // value2 value1 -> value1 value2
  public static void trackSWAP(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    
    if(taintedValue1) {
      slots.add(topStackSlotIdx - 1);
      slots.remove(topStackSlotIdx);
    }
    
    if(taintedValue2) {
      slots.add(topStackSlotIdx);
      if(!taintedValue1) slots.remove(topStackSlotIdx - 1);
    }
  }
  
  // value4 value3 value2 value1 -> value2 value1 value4 value3 value2 value1
  public static void trackDUP2_X2(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    boolean taintedValue3 = slots.contains(topStackSlotIdx - 2);
    boolean taintedValue4 = slots.contains(topStackSlotIdx - 3);
    
    if(taintedValue1) {
      slots.add(topStackSlotIdx + 2);
      slots.remove(topStackSlotIdx);
      slots.add(topStackSlotIdx - 2);
    }
    
    if(taintedValue2) {
      slots.add(topStackSlotIdx + 1);
      slots.remove(topStackSlotIdx - 1);
      slots.add(topStackSlotIdx - 3);
    }
    
    if(taintedValue3) {
      slots.add(topStackSlotIdx);
      if(!taintedValue1) slots.remove(topStackSlotIdx - 2);
    }
    
    if(taintedValue4) {
      slots.add(topStackSlotIdx - 1);
      if(!taintedValue2) slots.remove(topStackSlotIdx - 3);
    }
  }
  
  // value3 value2 value1 -> value2 value1 value3 value2 value1
  public static void trackDUP2_X1(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    boolean taintedValue3 = slots.contains(topStackSlotIdx - 2);
    
    if(taintedValue1) {
      slots.add(topStackSlotIdx + 2);
      slots.remove(topStackSlotIdx);
      slots.add(topStackSlotIdx - 1);
    }
    
    if(taintedValue2) {
      slots.add(topStackSlotIdx + 1);
      if(!taintedValue1) slots.remove(topStackSlotIdx - 1);
      slots.add(topStackSlotIdx - 2);
    }
    
    if(taintedValue3) {
      slots.add(topStackSlotIdx);
      if(!taintedValue2) slots.remove(topStackSlotIdx - 2);
    }
  }
  
  public static void trackDUP2(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    
    if(taintedValue1) slots.add(topStackSlotIdx + 2);
    if(taintedValue2) slots.add(topStackSlotIdx + 1);
  }
  
  // value3 value2 value1 -> value1 value3 value2 value1
  public static void trackDUP_X2(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    boolean taintedValue3 = slots.contains(topStackSlotIdx - 2);
    
    if(taintedValue1) {
      slots.add(topStackSlotIdx + 1);
      slots.remove(topStackSlotIdx);
      slots.add(topStackSlotIdx - 2);
    }
    
    if(taintedValue2) {
      slots.add(topStackSlotIdx);
      slots.remove(topStackSlotIdx - 1);
    }
    
    if(taintedValue3) {
      slots.add(topStackSlotIdx - 1);
      if(!taintedValue1) slots.remove(topStackSlotIdx - 2);
    }
  }
  
  public static void trackDUP_X1(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    boolean taintedValue2 = slots.contains(topStackSlotIdx - 1);
    
    if(taintedValue1) {
      slots.add(topStackSlotIdx + 1);
      slots.remove(topStackSlotIdx);
      slots.add(topStackSlotIdx - 1);
    }
    
    if(taintedValue2) {
      slots.add(topStackSlotIdx);
      if(!taintedValue1) slots.remove(topStackSlotIdx - 1);
    }
  }
  
  public static void trackDUP(Set<Integer> slots, int topStackSlotIdx) {
    boolean taintedValue1 = slots.contains(topStackSlotIdx);
    if(taintedValue1) {
      slots.add(topStackSlotIdx + 1);
    }
  }
  
  // value2 value1 -> value1 value2 value1
  public static <T> void trackDUP_X1(Map<Integer, T> registry, int topStackSlotIdx) {
    T value1StackSlots = registry.remove(topStackSlotIdx);
    T value2StackSlots = registry.remove(topStackSlotIdx - 1);
    
    if(value1StackSlots != null) {
      registry.put(topStackSlotIdx - 1, value1StackSlots);
      registry.put(topStackSlotIdx + 1, value1StackSlots);
    }
    
    if(value2StackSlots != null) {
      registry.put(topStackSlotIdx, value2StackSlots);
    }
  }
  
}
