/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import jdk.nashorn.internal.codegen.CompilationException;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.AnalysisException;
import aua.analysis.DFACommons;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.reference.ReferenceCollector.Reference;
import aua.cfg.CFGAnalyzer;
import aua.cfg.CFGAnalyzer.BasicBlock;
import aua.cfg.SSA;
import aua.cfg.SSA.Var;
import aua.utils.ASMUtils;

/**
 * This data flow analysis iterates backwards and works in the following phases:
 * <ol>
 * <li>Find the local that is returned as the destructuring array.
 * <li>Recursively track all modifications to its elements.
 * </ol>
 * We will capture metadata such as the number of elements returned and their types for future
 * use in other analysis such as a typed algorithm compilation.
 * 
 * @author sertel
 *
 */
public class ReturnArrayReferenceAnalysis extends AbstractOperatorAnalysis {
  
  public static boolean PARALLEL = false;
  // debugging really wants to have good old loops otherwise it really gets complicated.
  public static boolean GOOD_OLD_LOOPS = true;
  
  public class ReferenceLeakException extends LeakDetectionException {
    protected Set<String> _leakedReferences = null;
  }
  
  static class Context {
    ClassNode _class;
    MethodNode _method;
    
    AbstractInsnNode[] _instructions;
    Frame[] _frames;
    SSA _ssa;
    
    Map<Var, Reference> _aliases = new HashMap<>();
    
    private Context(ClassNode cn, MethodNode mn, Frame[] frames, boolean initialize) {
      // just used for copying
      _class = cn;
      _method = mn;
      _instructions = _method.instructions.toArray();
      _frames = frames;
      if(initialize) initialize();
    }
    
    public Context(ClassNode cn, MethodNode mn, Frame[] frames) {
      this(cn, mn, frames, true);
    }
    
    // algMethod.localVariables
    @SuppressWarnings("unchecked")
    private void initialize() {
      _ssa = new SSA();
      
      // we have to go check on the local variables no matter what, even if classes are
      // compiled with this info. this is because syntactic sugar is being resolved later, it
      // seems and therefore constructs such as the iterator in a for-loop do not appear in the
      // local variable list.
      _method.localVariables = ASMUtils.computeLocalVariableInfo(_class.name, _method, _method.localVariables);
      
      _ssa.transform(_method.localVariables,
                     Arrays.stream(_frames).map(x -> (BasicBlock) x).collect(Collectors.toList()).toArray(new BasicBlock[_frames.length]),
                     _instructions);
    }
    
    protected Context shallowCopy() {
      Context copy = new Context(_class, _method, _frames, false);
      copy._ssa = _ssa; // shared this info, it's immutable
      copy._aliases = new HashMap<>(_aliases); // shallow copy on this one
      return copy;
    }
  }
  
  /**
   * This analysis is performed on the CFG.
   */
  protected Analyzer getAnalyzer(Interpreter interpreter) {
    return new CFGAnalyzer(interpreter);
  }
  
  public
      List<List<Reference>>
      analyze(String owner, String methodName, Class<?> returnType, Class<?>... args)
                                                                                     throws LeakDetectionException,
                                                                                     IOException,
                                                                                     AnalysisException
  {
    return analyze(owner, methodName, super.constructMethodDescriptor(returnType, args));
  }
  
  public List<List<Reference>>
      analyze(String owner, String methodName, String methodDescriptor) throws LeakDetectionException,
                                                                       IOException, AnalysisException
  {
    // preparation
    super.charge(owner);
    MethodNode algMethod = super.loadMethod(methodName, methodDescriptor);
    Frame[] frames = super.computeStackMapFrames(algMethod);
    // TODO this will change once the loading code got extracted from the super class
    return analyze(owner, methodName, methodDescriptor, new Context(_node, algMethod, frames));
  }
  
  protected List<List<Reference>>
      analyze(String owner, String methodName, String methodDesc, Context c) throws LeakDetectionException,
                                                                            AnalysisException
  {
    int ARETURNinst = findDestructuringArray(c);
    if(ARETURNinst < 0) {
      // we are done here because either the function does not return anything (void) or it just
      // does not return an array (no destructuring)
      return Collections.emptyList();
    } else {
      Set<Integer> trackedStackSlots = new HashSet<>();
      trackedStackSlots.add(c._frames[ARETURNinst].getStackSize());
      Set<Integer> arrayOrigins =
          OriginDiscovery.findOrigin(ARETURNinst, trackedStackSlots, c._instructions, c._frames);
      if(arrayOrigins.size() != 1) {
        assert false : "Please rewrite your function to define the destructuring array only a one single place.";
        return null;
      } else {
        int destArrayOrigin = arrayOrigins.iterator().next();
        // let's look at the instruction and evaluate whether we have to do any further analysis
        switch(c._instructions[destArrayOrigin].getOpcode()) {
          case Opcodes.NEWARRAY:
            // this is an array of basic type, so there is nothing left to investigate here
            return Collections.emptyList();
          case Opcodes.ALOAD:
            // the array was created in some other basic block. we take the local index
            // therefore
            // and track it. we need another pass to find the AASTORE statements that contribute
            // to it.
            
            // TODO
            assert false : "Creating the destructuring array in a different basic block is currently not allowed. "
                           + "Please create it as the very final step before returing from the call.";
            return null;
          case Opcodes.ANEWARRAY:
            // the typical case: we found the array creation now we need to find where the
            // internals come from. (tracing of the internals starts from the last basic block.)
            if(ReferenceCollector.isImmutable(destArrayOrigin, c))
            {
              return Collections.emptyList();
            } else {
              List<Integer> aaStoreInstructions = findElements(destArrayOrigin, c);
              if(!aaStoreInstructions.isEmpty()) {
                return performReferenceAnalysis(aaStoreInstructions, c);
              } else {
                return Collections.emptyList();
              }
            }
          default:
            assert false;
            return null;
        }
      }
    }
  }
  
  /**
   * For each of the AASTORE instructions we do a full tracking and gather all side-effects to
   * them in a context-sensitive way.
   * 
   * @param aaStoreInstructions
   * @param instructions
   * @param _frames
   * @throws LeakDetectionException
   * @throws CompilationException
   */
  private List<List<Reference>>
      performReferenceAnalysis(List<Integer> aaStoreInstructions, Context c) throws LeakDetectionException,
                                                                            AnalysisException
  {
    // TODO optimization: exclude references of immutable type first
    // TODO optimization: check how many items are actually needed. if only a single item/tree is required then just skip it.
    List<List<Reference>> referenceCollections = null;
    try {
      if(PARALLEL) {
        referenceCollections =
            aaStoreInstructions.parallelStream().map(aaStoreInst -> collectReferenceTree(aaStoreInst, c)).collect(Collectors.toList());
      } else {
        if(GOOD_OLD_LOOPS) {
          referenceCollections = new ArrayList<>();
          for(Integer aaStoreInst : aaStoreInstructions) {
            referenceCollections.add(collectReferenceTree(aaStoreInst, c));
          }
        } else {
          referenceCollections =
              aaStoreInstructions.stream().map(aaStoreInst -> collectReferenceTree(aaStoreInst, c)).collect(Collectors.toList());
        }
      }
    }
    catch(RuntimeException r) {
      if(r.getCause() != null && r.getCause() instanceof AnalysisException) throw (AnalysisException) r.getCause();
      else {
        throw r;
      }
    }
    
    List<Set<String>> refs = new ArrayList<>();
    for(List<Reference> refCol : referenceCollections)
      refs.add(refCol.stream().map(Reference::toString).collect(Collectors.toSet()));
    
    for(int i = 0; i < refs.size() - 1; i++) {
      Set<String> one = refs.get(i);
      Set<String> two = new HashSet<>(refs.get(i + 1));
      two.retainAll(one);
      if(!two.isEmpty()) {
        ReferenceLeakException lde = new ReferenceLeakException();
        String s = Arrays.deepToString(two.toArray());
        lde._fieldLeaked = "(" + s.substring(1, s.length() - 1) + ")";
        lde._leakedReferences = two;
        throw lde;
      }
    }
    return referenceCollections;
  }
  
  private List<Reference> collectReferenceTree(int aaStoreInstruction, Context c) {
    try {
      return ReferenceCollector.collectReferences(aaStoreInstruction, c, true);
    }
    catch(AnalysisException e) {
      throw new RuntimeException(e);
    }
  }
  
  /**
   * This part of the analysis is forward directed and will find all AASTORE instructions to the
   * given target array.
   * 
   * @param destArrayOrigin
   * @param instructions
   * @param _frames
   */
  private List<Integer> findElements(int destArrayOrigin, Context c) {
    Set<Integer> trackedLocals = new HashSet<>();
    Set<Integer> trackedStackSlots = new HashSet<>();
    trackedStackSlots.add(c._frames[destArrayOrigin].getStackSize());
    
    List<Integer> aaStoreInstructions = new ArrayList<>();
    
    for(int j = destArrayOrigin + 1; j < c._instructions.length; j++) {
      AbstractInsnNode instruction = c._instructions[j];
      // this function carries the wrong name. it actually returns the index of the top slot
      int topStackSlotIdx = c._frames[j] != null ? c._frames[j].getStackSize() : 0;
      // if this thing says 0 then the stack is empty! so the first stack position is actually
      // 1.
      
      DFACommons.cleanTrackedStackSlots(trackedStackSlots, c._frames[j - 1].getStackSize(), topStackSlotIdx);
      
      /*
       * All we need to do is to be sensitive to AASTORE and perform our (forward) propagation
       * properly.
       */
      switch(instruction.getType()) {
        case AbstractInsnNode.METHOD_INSN:
          // TODO
          break;
        case AbstractInsnNode.INSN:
          switch(instruction.getOpcode()) {
            case Opcodes.AASTORE:
              if(trackedStackSlots.contains(topStackSlotIdx - 2)) {
                aaStoreInstructions.add(j);
              }
              break;
          }
          break;
        default:
      }
      DFACommons.trackForward(instruction, topStackSlotIdx, trackedStackSlots, trackedLocals);
    }
    return aaStoreInstructions;
  }
  
  protected int findDestructuringArray(Context c) {
    for(int j = c._instructions.length - 1; j > -1; j--) {
      AbstractInsnNode instruction = c._instructions[j];
      if(instruction instanceof InsnNode) {
        if(instruction.getOpcode() == Opcodes.ARETURN) {
          return j;
        }
      } else {
        // TODO there is presumably nothing to do here
      }
    }
    // this can happen once we allow functions to not return anything (target operators!)
    return -1;
  }
}
