/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.Frame;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.AnalysisException;
import aua.analysis.Interprocedural;
import aua.analysis.qual.KnowledgeBase;
import aua.analysis.reference.ReturnArrayReferenceAnalysis.Context;
import aua.analysis.reference.ReferenceCollector.Reference;
import aua.cfg.CFGAnalyzer;
import aua.cfg.CFGVisualizer;
import aua.cfg.SSA.Var;

public abstract class InterproceduralReferenceAnalysis {
  /**
   * We need to understand the nature of tracking here: If any of the arguments is being tracked
   * then we have to analyze this invocation. On the other hand, if this function has been the
   * origin of a stack slot that we are tracking then we really have to analyze it.
   * 
   * @param j
   * @param topStackSlotIdx
   * @param methodInvocation
   * @param trackedStackSlots
   * @param c
   * @return
   * @throws AnalysisException
   */
  protected static List<Reference> handleFunctionInvocation(int j, int topStackSlotIdx,
                                                            MethodInsnNode methodInvocation,
                                                            Set<Integer> trackedStackSlots, Context c,
                                                            boolean isOrigin) throws AnalysisException
  {
    // skip recursive function invocations
    // TODO handle complex recursions
    if(c._class.name.equals(methodInvocation.owner) && c._method.name.equals(methodInvocation.name)
       && c._method.desc.equals(methodInvocation.desc))
    {
      return Collections.emptyList();
    }
    
    Type[] argTypes = Type.getArgumentTypes(methodInvocation.desc);
    // the first arg slot depends on the type of the invocation
    int firstArgSlot = -1;
    // methodInvocation.getOpcode() == Opcodes.INVOKESTATIC ? topStackSlotIdx - argTypes.length
    // + 1 : topStackSlotIdx
    // - argTypes.length;
    // Calculate the slots. Below they are converted to the index of the locals.
    List<Integer> argsNeedTracking = new ArrayList<>();
    if(methodInvocation.getOpcode() == Opcodes.INVOKESTATIC) {
      firstArgSlot = topStackSlotIdx - argTypes.length + 1;
      for(int i = 1; i <= argTypes.length; i++) {
        int currentSlot = firstArgSlot + (i - 1);
        if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(argTypes[i - 1].getClassName())
           && trackedStackSlots.contains(currentSlot))
        {
          argsNeedTracking.add(currentSlot);
        }
      }
    } else {
      // non-static methods!
      firstArgSlot = topStackSlotIdx - argTypes.length;
      for(int i = 1; i <= argTypes.length; i++) {
        int currentSlot = firstArgSlot + i;
        if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(argTypes[i - 1].getClassName())
           && trackedStackSlots.contains(currentSlot))
        {
          argsNeedTracking.add(currentSlot);
        }
      }
      
      // owner reference might be tracked!
      if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(Type.getObjectType(methodInvocation.owner))
         && trackedStackSlots.contains(firstArgSlot))
      {
        argsNeedTracking.add(firstArgSlot);
      }
    }
    
    Type retType = Type.getReturnType(methodInvocation.desc);
    Type ownerType = Type.getObjectType(methodInvocation.owner);
    boolean isLanguage =
        ownerType.getClassName().startsWith("java.lang.reflect")
            || ownerType.getClassName().startsWith("java.lang.Class");
    if(retType == Type.VOID_TYPE && argsNeedTracking.isEmpty()
       && methodInvocation.getOpcode() == Opcodes.INVOKESTATIC && !isOrigin || isLanguage)
    {
      // nothing to be done!
      return Collections.emptyList();
    } else {
      List<Reference> found = new ArrayList<>();
      
      ClassNode cn = null;
      MethodNode mn = null;
      // load the new function
      try {
        // System.out.println(c._class.name + "/" + methodInvocation.name);
        Object[] r = Interprocedural.loadMethod(c._class, methodInvocation);
        cn = (ClassNode) r[0];
        mn = (MethodNode) r[1];
        assert mn != null : "Method " + methodInvocation.name + " of class " + methodInvocation.owner
                            + " could not be loaded.";
      }
      catch(IOException e) {
        // should never happen because this is essentially compiled code. it would mean
        // that the library is not there anyways, so this program would not be able to
        // run.
        assert false : e;
      }
      
      if(mn.instructions.size() < 1) {
        // abstract or interface method detected
        // TODO we could give it a try and understand the origin and type of the object that
        // this method belongs to.
        // TODO remember and notify
        // System.out.println("Skipping abstract/interface method " + cn.name + "/" + mn.name);
        return found;
      } else {
        // construct a new context and register the true origins -> change the above
        // algorithm part to check if a found local has another reference origin
        Frame[] fs = Interprocedural.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()), cn.name, mn);
        Context ctxt = new Context(cn, mn, fs);
        
        CFGVisualizer.print(c._instructions, c._frames, cn.name, mn.name);
        
        // find the origin of the arguments
        Map<Var, Reference> origins = new HashMap<>();
        // don't collect the origin of the owner of the function
        for(int i = firstArgSlot; i <= topStackSlotIdx; i++) {
          // FIXME this seems to be not the right thing to do here. we really have to understand
          // where the arguments are coming from.
          List<Reference> argOrigin = ReferenceCollector.detectOriginReference(i, j, c);
          // TODO for now we always take the first one if there are multiple. however, normally
          // we have to run this thing on both origins, I suppose.
          if(!argOrigin.isEmpty()) {
            Reference argOriginRef = argOrigin.iterator().next();
            if(argOriginRef._signature != null) {
              Var v = ctxt._ssa.getSSAFromParams(i - firstArgSlot, 0, fs);
              origins.put(v, argOrigin.iterator().next());
            } else {
              // FIXME might be a function. how do we handle this?!?
            }
          } else {
            // FIXME what does it mean when no origin could be detected???
          }
        }
        // // System.out.println("Checking abstract/interface method " + cn.name + "/" +
        // mn.name);
        // System.out.println("Computed Aliases: " + origins);
        ctxt._aliases = origins;
        
        // do the analysis
        List<Reference> funcRefs = new ArrayList<>();
        switch(c._instructions[j].getOpcode()) {
          case Opcodes.INVOKEDYNAMIC:
            assert false : "Unsupported yet!";
            break;
          case Opcodes.INVOKEINTERFACE:
            assert false : "Unsupported yet!";
            break;
          case Opcodes.INVOKESTATIC:
          case Opcodes.INVOKESPECIAL:
          case Opcodes.INVOKEVIRTUAL:
            // TODO we can also just check if this function has side-effects and register it as
            // such along with the origin of its arguments. this provides the programmer with a
            // bit more control over this analysis.
            
            if(isOrigin) {
              // we are interested in the returned reference that means.
              
              boolean isUntainted = false;
              try {
                isUntainted =
                    KnowledgeBase.getInstance().isFunctionResultUntainted(Type.getObjectType(cn.name).getClassName(),
                                                                          mn.name,
                                                                          mn.desc);
              }
              catch(ClassNotFoundException e) {
                assert false;
              }
              
              if(!isUntainted) {
                /*
                 * analyze the function -> the analysis must return the references encapsulated
                 * by the object reference returned by this function. hence, the instruction
                 * passed here must be the last one of this function.
                 */
                // the last frame of the function might be a LabelNode without a stack map
                // frame.
                // this is the case when debug information is present.
                List<Integer> rInsts = findReturnInstructions(ctxt._instructions);
                try {
                  if(ReturnArrayReferenceAnalysis.PARALLEL) {
                    ReferenceCollector.cAddAll(funcRefs,
                                               rInsts.parallelStream().map(rInst -> runReturnDiscovery(rInst,
                                                                                                       ctxt.shallowCopy())).flatMap(l -> l.stream()).collect(Collectors.toList()));
                  } else {
                    ReferenceCollector.cAddAll(funcRefs,
                                               rInsts.stream().map(rInst -> runReturnDiscovery(rInst,
                                                                                               ctxt.shallowCopy())).flatMap(l -> l.stream()).collect(Collectors.toList()));
                  }
                }
                catch(RuntimeException r) {
                  // FIXME this is a problem if there was really a runtime exception. we need to
                  // understand here if this is essentially a wrapped exception or not!
                  throw (AnalysisException) r.getCause();
                }
              }
            } else {
              // some of the arguments passed to the function are a target reference that we are
              // tracking.
              final int fArgSlot = firstArgSlot;
              try {
                if(ReturnArrayReferenceAnalysis.PARALLEL) {
                  ReferenceCollector.cAddAll(funcRefs,
                                             argsNeedTracking.parallelStream().map(argNeedsTracking -> runArgTracker(argNeedsTracking
                                                                                                                         - fArgSlot,
                                                                                                                     ctxt.shallowCopy())).flatMap(l -> l.stream()).collect(Collectors.toList()));
                } else {
                  if(ReturnArrayReferenceAnalysis.GOOD_OLD_LOOPS) {
                    for(Integer argNeedsTracking : argsNeedTracking) {
                      ReferenceCollector.cAddAll(funcRefs, runArgTracker(argNeedsTracking - fArgSlot, ctxt.shallowCopy()));
                    }
                  } else {
                    ReferenceCollector.cAddAll(funcRefs,
                                               argsNeedTracking.stream().map(argNeedsTracking -> runArgTracker(argNeedsTracking
                                                                                                                   - fArgSlot,
                                                                                                               ctxt.shallowCopy())).flatMap(l -> l.stream()).collect(Collectors.toList()));
                  }
                }
              }
              catch(RuntimeException r) {
                if(r.getCause() != null && r.getCause() instanceof AnalysisException)
                  throw (AnalysisException) r.getCause();
                else throw r;
              }
            }
            break;
          default:
            assert false : "Unknown method invocation op code detected: " + ctxt._instructions[j].getOpcode();
        }
        
        // FIXME the analysis has to signal us whether we should track the returned reference or
        // not! that is: was the top stack slot of the investigated function tainted?!
        
        // System.out.println(cn.name + "." + mn.name + " >> found refs: " + funcRefs);
        // aftermath
        for(Reference funcRef : funcRefs) {
          Reference f = collectFunctionReferences1(topStackSlotIdx, firstArgSlot, funcRef, ctxt, c);
          if(f != null) ReferenceCollector.cAdd(found, f);
        }
        return found;
      }
    }
  }
  
  private static List<Reference> runArgTracker(int local, Context ctxt) {
    try {
      return ReferenceCollector.collectLocalVar(0, local, ctxt);
    }
    catch(AnalysisException e) {
      throw new RuntimeException(e);
    }
  }
  
  private static List<Reference> runReturnDiscovery(int rInst, Context ctxt) {
    int aReturnInst = rInst;
    if(ctxt._frames[rInst] == null) {
      aReturnInst = rInst - 1;
    }
    // here we include the reference because we are interested in the returned
    // reference!
    
    try {
      return ReferenceCollector.collectReferences(aReturnInst, ctxt, true);
    }
    catch(AnalysisException e) {
      throw new RuntimeException(e);
    }
  }
  
  /**
   * Finds all return instructions on branches that return objects.
   * @param insts
   * @return
   */
  private static List<Integer> findReturnInstructions(AbstractInsnNode[] insts) {
    List<Integer> returnInsts = new ArrayList<>();
    for(int i = 0; i < insts.length; i++)
      if(insts[i].getOpcode() == Opcodes.ARETURN) returnInsts.add(i);
    return returnInsts;
  }
  
  private static Reference collectFunctionReferences1(int topStackSlotIdx, int firstArgSlot, Reference funcRef,
                                                      Context c, Context parent)
  {
    // this is still not correct: what do we need to propagate here?!
    switch(funcRef.resolveInsn().getOpcode()) {
      case Opcodes.ALOAD:
      case Opcodes.ASTORE:
        // FIXME I believe this is none-sense. In the path, we should check the context and then
        // we have to understand whether this reference was derived from any of the input
        // arguments of this context.
        // System.out.println("Local: " + ((VarInsnNode) funcRef.resolveInsn()).var);
        if((topStackSlotIdx - firstArgSlot) >= ((VarInsnNode) funcRef.resolveInsn()).var) {
          // System.out.println("Propagting ref1: " + funcRef);
          return funcRef;
        } else {
          
          // find context derivitives
          for(Reference originRef : funcRef._originPath) {
            if(originRef._ctxt == c) {
              AbstractInsnNode originInst = originRef.resolveInsn();
              if(originInst.getOpcode() == Opcodes.ALOAD
                 && (topStackSlotIdx - firstArgSlot) >= ((VarInsnNode) originInst).var)
              {
                // System.out.println("Propagting ref: " + funcRef);
                return funcRef;
              }
            }
          }
          
          // if((topStackSlotIdx - firstArgSlot) >= ((VarInsnNode) funcRef.resolveInsn()).var) {
          // // this should mean that the reference was derived from one of its input args
          // return funcRef;
          // } else {
          // check whether this local is derived from a local that we care about!
          // String desc =
          // ReferenceCollector.getLocalAccessDescription(funcRef.getAssignmentOrigin(),
          // funcRef._instruction,
          // funcRef._ctxt);
          // FIXME should be done in findAliasOrigin
          // Var ssaVar =
          // funcRef._ctxt._ssa.getSSA(((VarInsnNode) funcRef.resolveInsn()).var,
          // funcRef.getAssignmentOrigin());
          // if(ssaVar._phiGenerated) {
          // BasicBlock bb = (BasicBlock) funcRef._ctxt._frames[ssaVar._start];
          // BasicBlock predBB = bb._predecessors.iterator().next();
          // desc = ReferenceCollector.getLocalAccessDescription(predBB._end,
          // funcRef._instruction, funcRef._ctxt);
          // }
          // if(funcRef._ctxt._ssaAliases.containsKey(desc)) {
          // Reference r =
          // collectFunctionReferences(topStackSlotIdx, firstArgSlot,
          // funcRef._ctxt._ssaAliases.get(desc));
          // if(r != null) return funcRef;
          // else return r;
          // } else {
          // // if it comes from any other local then just drop that reference!
          // System.out.println("Dropping ref: " + funcRef);
          return null;
          // }
        }
      case Opcodes.PUTFIELD:
      case Opcodes.PUTSTATIC:
        return funcRef;
      default:
        // if it originates from something that is created outside of this context then we keep
        // it.
        for(Reference r : funcRef._originPath) {
          if(r._ctxt != c) return funcRef;
        }
        
        // drop every reference that does not originate from a field or a local
        return null;
    }
  }
}
