/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.analysis.Frame;

import aua.cfg.CFGAnalyzer;
import aua.cfg.CFGAnalyzer.BasicBlock;

public abstract class OriginDiscovery {
  
  /**
   * Like findOrigin but takes functions into account.
   * 
   * @param start
   * @param hashSet
   * @param _instructions
   * @param _frames
   * @return
   */
  public static Set<Integer> findOriginWithFunction(int start, Set<Integer> trackedStackSlots,
                                                    AbstractInsnNode[] instructions, Frame[] frames)
  {
    
    return findOrigin(start, trackedStackSlots, instructions, frames, true);
  }
  
  protected static Set<Integer> findOrigin(int start, Set<Integer> trackedStackSlots,
                                           AbstractInsnNode[] instructions, Frame[] frames)
  {
    return findOrigin(start, trackedStackSlots, instructions, frames, false);
  }
  
  /**
   * Until ANEWARRAY, NEW, ALOAD, GETFIELD, GETSTATIC, track the stack slot(s) that reference
   * the top stack slot of the instruction.
   */
  protected static Set<Integer> findOrigin(int start, Set<Integer> trackedStackSlots,
                                           AbstractInsnNode[] instructions, Frame[] frames, boolean methodSupport)
  {
    assert trackedStackSlots.size() == 1;
    for(int j = start - 1; j > -1; j--) {
      AbstractInsnNode instruction = instructions[j];
      // this function carries the wrong name. it actually returns the index of the top slot
      int topStackSlotIdx = frames[j] != null ? frames[j].getStackSize() : 0;
      int nextTopStackSlotIdx = frames[j + 1] != null ? frames[j + 1].getStackSize() : 0;
      // if this thing says 0 then the stack is empty! so the first stack position is actually
      // 1.
      
      // FIXME this seems not the right place because I want to know if the current instruction
      // produced the tracked slot but at this moment I'm actually removing it already from the
      // tracked items!
      // the last entry in the frames array may be null (don't know if this is always the case)
      // for(int t = topStackSlotIdx + 1; j + 1 < frames.length && frames[j + 1] != null && t <=
      // nextTopStackSlotIdx; t++)
      // trackedStackSlots.remove(t);
      
      // we are basic looking for the end of the current basic block!
      BasicBlock bb = (BasicBlock) frames[j];
      if(bb._start == j) {
        // FIXME it is possible that a slot has two origins if it is for example the result of
        // the stack evaluation on two conditional branches. so we have to use recursion here!
        Set<Integer> origins = new HashSet<>();
        for(BasicBlock pred : CFGAnalyzer.getNonFeedbackPredecessors(bb))
          // I think here it is ok to look only for the "true" predecessor as only there the
          // variable could be set accordingly.
          if(pred._end < bb._start) {
            origins.addAll(findOrigin(pred._end,
                                      new HashSet<Integer>(trackedStackSlots),
                                      instructions,
                                      frames,
                                      methodSupport));
          }
        return origins;
      } else {
        // continue if we are not at the end of the basic block
        switch(instruction.getType()) {
          case AbstractInsnNode.FIELD_INSN:
            switch(instruction.getOpcode()) {
              case Opcodes.GETFIELD:
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // at this moment the only reference should be the top stack slot
                  assert trackedStackSlots.size() == 1;
                  return Collections.singleton(j);
                }
                break;
              case Opcodes.GETSTATIC:
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // at this moment the only reference should be the top stack slot
                  assert trackedStackSlots.size() == 1;
                  return Collections.singleton(j);
                }
                break;
            }
            break;
          case AbstractInsnNode.TYPE_INSN:
            switch(instruction.getOpcode()) {
              case Opcodes.ANEWARRAY: // creates an array of some class type
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // at this moment the only reference should be the top stack slot
                  assert trackedStackSlots.size() == 1;
                  return Collections.singleton(j);
                }
                break;
              case Opcodes.NEW:
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // at this moment the only reference should be the top stack slot
                  assert trackedStackSlots.size() == 1;
                  return Collections.singleton(j);
                }
                break;
              default:
                // nothing
            }
            break;
          case AbstractInsnNode.INT_INSN:
            switch(instruction.getOpcode()) {
              case Opcodes.BIPUSH: // pushes an integer onto the stack
              case Opcodes.SIPUSH: // pushes a short onto the stack
              case Opcodes.NEWARRAY: // creates an array of basic type
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // at this moment the only reference should be the top stack slot
                  assert trackedStackSlots.size() == 1;
                  return Collections.singleton(j);
                }
                break;
              default:
                // nothing
            }
            break;
          case AbstractInsnNode.LDC_INSN:
            if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
              // at this moment the only reference should be the top stack slot
              assert trackedStackSlots.size() == 1;
              return Collections.singleton(j);
            }
            break;
          case AbstractInsnNode.VAR_INSN:
            switch(instruction.getOpcode()) {
              case Opcodes.FLOAD:
              case Opcodes.ILOAD:
              case Opcodes.DLOAD:
              case Opcodes.ALOAD:
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  // we just found an instruction that identifies the origin
                  return Collections.singleton(j);
                }
                break;
              default:
                // nothing
            }
            break;
          case AbstractInsnNode.INSN:
            switch(instruction.getOpcode()) {
              case Opcodes.DUP:
                boolean addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                boolean addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                if(addSlot1) trackedStackSlots.add(nextTopStackSlotIdx - 1);
                if(addSlot2) trackedStackSlots.add(nextTopStackSlotIdx - 1);
                break;
              case Opcodes.DUP2:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                boolean addSlot3 = trackedStackSlots.contains(nextTopStackSlotIdx - 2);
                boolean addSlot4 = trackedStackSlots.contains(nextTopStackSlotIdx - 3);
                if(addSlot1) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot2) trackedStackSlots.add(topStackSlotIdx - 1);
                if(addSlot3) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot4) trackedStackSlots.add(topStackSlotIdx - 1);
                break;
              case Opcodes.DUP2_X1:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                addSlot3 = trackedStackSlots.contains(nextTopStackSlotIdx - 2);
                addSlot4 = trackedStackSlots.contains(nextTopStackSlotIdx - 3);
                boolean addSlot5 = trackedStackSlots.contains(nextTopStackSlotIdx - 4);
                // FIXME do explicit removals here
                if(addSlot1) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot2) trackedStackSlots.add(topStackSlotIdx - 1);
                if(addSlot3) trackedStackSlots.add(topStackSlotIdx - 2);
                if(addSlot4) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot5) trackedStackSlots.add(topStackSlotIdx - 1);
                break;
              case Opcodes.DUP2_X2:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                addSlot3 = trackedStackSlots.contains(nextTopStackSlotIdx - 2);
                addSlot4 = trackedStackSlots.contains(nextTopStackSlotIdx - 3);
                addSlot5 = trackedStackSlots.contains(nextTopStackSlotIdx - 4);
                boolean addSlot6 = trackedStackSlots.contains(nextTopStackSlotIdx - 5);
                // FIXME do explicit removals here
                if(addSlot1) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot2) trackedStackSlots.add(topStackSlotIdx - 1);
                if(addSlot3) trackedStackSlots.add(topStackSlotIdx - 2);
                if(addSlot4) trackedStackSlots.add(topStackSlotIdx - 3);
                if(addSlot5) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot6) trackedStackSlots.add(topStackSlotIdx - 1);
                break;
              case Opcodes.DUP_X1:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                addSlot3 = trackedStackSlots.contains(nextTopStackSlotIdx - 2);
                
                if(addSlot3) {
                  trackedStackSlots.add(topStackSlotIdx);
                  // explicit remove because this slot persists backwards but not with the same
                  // value!
                  trackedStackSlots.remove(nextTopStackSlotIdx - 2);
                }
                if(addSlot2) trackedStackSlots.add(topStackSlotIdx - 1);
                if(addSlot1) trackedStackSlots.add(topStackSlotIdx);
                break;
              case Opcodes.DUP_X2:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                addSlot3 = trackedStackSlots.contains(nextTopStackSlotIdx - 2);
                addSlot4 = trackedStackSlots.contains(nextTopStackSlotIdx - 3);
                // FIXME do explicit removals here
                if(addSlot1) trackedStackSlots.add(topStackSlotIdx);
                if(addSlot2) trackedStackSlots.add(topStackSlotIdx - 1);
                if(addSlot3) trackedStackSlots.add(topStackSlotIdx - 2);
                if(addSlot4) trackedStackSlots.add(topStackSlotIdx);
                break;
              case Opcodes.SWAP:
                addSlot1 = trackedStackSlots.contains(nextTopStackSlotIdx);
                addSlot2 = trackedStackSlots.contains(nextTopStackSlotIdx - 1);
                if(addSlot1) {
                  trackedStackSlots.add(topStackSlotIdx - 1);
                  trackedStackSlots.remove(nextTopStackSlotIdx);
                }
                if(addSlot2) {
                  trackedStackSlots.add(topStackSlotIdx);
                  trackedStackSlots.remove(nextTopStackSlotIdx - 1);
                }
                break;
              case Opcodes.NOP:
              case Opcodes.ACONST_NULL:
              case Opcodes.ICONST_M1:
              case Opcodes.ICONST_0:
              case Opcodes.ICONST_1:
              case Opcodes.ICONST_2:
              case Opcodes.ICONST_3:
              case Opcodes.ICONST_4:
              case Opcodes.ICONST_5:
              case Opcodes.LCONST_0:
              case Opcodes.LCONST_1:
              case Opcodes.FCONST_0:
              case Opcodes.FCONST_1:
              case Opcodes.FCONST_2:
              case Opcodes.DCONST_0:
              case Opcodes.DCONST_1:
                
                // these are also instructions that produce a stack slot, so they can be an
                // origin!
              case Opcodes.IADD:
              case Opcodes.LADD:
              case Opcodes.FADD:
              case Opcodes.DADD:
              case Opcodes.ISUB:
              case Opcodes.LSUB:
              case Opcodes.FSUB:
              case Opcodes.DSUB:
              case Opcodes.IMUL:
              case Opcodes.LMUL:
              case Opcodes.FMUL:
              case Opcodes.DMUL:
              case Opcodes.IDIV:
              case Opcodes.LDIV:
              case Opcodes.FDIV:
              case Opcodes.DDIV:
              case Opcodes.IREM:
              case Opcodes.LREM:
              case Opcodes.FREM:
              case Opcodes.DREM:
              case Opcodes.INEG:
              case Opcodes.LNEG:
              case Opcodes.FNEG:
              case Opcodes.DNEG:
              case Opcodes.ISHL:
              case Opcodes.LSHL:
              case Opcodes.ISHR:
              case Opcodes.LSHR:
              case Opcodes.IUSHR:
              case Opcodes.LUSHR:
              case Opcodes.IAND:
              case Opcodes.LAND:
              case Opcodes.IOR:
              case Opcodes.LOR:
              case Opcodes.IXOR:
              case Opcodes.LXOR:
                
              case Opcodes.AALOAD:
                if(trackedStackSlots.contains(nextTopStackSlotIdx)) {
                  return Collections.singleton(j);
                }
                break;
              default:
                // all other instructions (ADD, SUB, MULT, NEG etc.) are operations that produce
                // a result. so kick the 'result' off the stack. -> done below.
            }
            break;
          case AbstractInsnNode.METHOD_INSN:
            if(methodSupport && trackedStackSlots.contains(nextTopStackSlotIdx)) {
              // only if this function really produced this stack slot! therefore it needs to
              // produce something.
              if(Type.getReturnType(((MethodInsnNode) instruction).desc) != Type.VOID_TYPE) {
                return Collections.singleton(j);
              }
            }
            break;
          default:
        }
      }
      // the last entry in the frames array may be null (don't know if this is always the case)
      for(int t = topStackSlotIdx + 1; j + 1 < frames.length && frames[j + 1] != null && t <= nextTopStackSlotIdx; t++)
        trackedStackSlots.remove(t);
      
    }
    assert false;
    return null;
  }
  
  /**
   * Looks whether there is any instruction that copies the tracked local to another local or
   * whether there is another local that gets copied to the tracked local.
   * 
   * @param start
   * @param localIdx
   * @param instructions
   * @param frames
   * @return
   */
  protected static List<Integer> findAliases(int start, int localIdx, AbstractInsnNode[] instructions,
                                             Frame[] frames)
  {
    // FIXME This is essentially a special form of SSA where there is not only a numbering
    // scheme but we actually know where the data came from such that we can provide an alias
    // instead.
    return new ArrayList<>(Collections.singletonList(localIdx));
    // TODO
    // Set<Integer> trackedStackSlots = new HashSet<>();
    // Set<Integer> trackedLocalSlots = new HashSet<>();
    // trackedLocalSlots.add(localIdx);
    //
    // List<Integer> aliases = new ArrayList<>();
    // for(int j = start + 1; j < instructions.length; j++) {
    // AbstractInsnNode instruction = instructions[j];
    // // this function carries the wrong name. it actually returns the index of the top slot
    // int topStackSlotIdx = frames[j] != null ? frames[j].getStackSize() : 0;
    // // if this thing says 0 then the stack is empty! so the first stack position is actually
    // // 1.
    //
    // DFACommons.cleanTrackedStackSlots(trackedStackSlots, frames[j - 1].getStackSize(),
    // topStackSlotIdx);
    // switch(instruction.getOpcode()){
    // case Opcodes.ALOAD:
    // // FIXME keep track of which slots are on the stack -> need a map instead of a simple
    // set!
    // break;
    // case Opcodes.ASTORE:
    // break;
    // default:
    // // nothing
    // }
    // // FIXME keep track of which slots are on the stack -> need a map instead of a simple
    // set!
    // DFACommons.trackForward(instruction, topStackSlotIdx, trackedStackSlots,
    // trackedLocalSlots);
    // }
    //
    // return aliases;
  }
  
}
