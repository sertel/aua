/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import jdk.nashorn.internal.codegen.CompilationException;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Value;

import aua.analysis.AbstractOperatorAnalysis;
import aua.analysis.AnalysisException;
import aua.analysis.DFACommons;
import aua.analysis.reference.ReturnArrayReferenceAnalysis.Context;
import aua.cfg.CFGAnalyzer.BasicBlock;
import aua.cfg.SSA.Var;
import aua.utils.ASMUtils;

/**
 * This class walks over the instructions and collects all side-effects to this object.
 * 
 * @author sertel
 *
 */
public abstract class ReferenceCollector {
  
  public static class Reference {
    
    int _instruction = -1; // right side of assignment (origin)
    Context _ctxt = null;
    
    // int _tragetInstruction = -1; // left side of assignment
    /**
     * An identifier string that resolves access to where the state came from that contributed
     * to this object.
     */
    List<String> _signature = null;
    
    /**
     * If it is an AALOAD that puts this var on the stack then the instruction will point to the
     * instruction that loaded the parent object reference but this list will contain the AALOAD
     * instruction.
     */
    List<Reference> _originPath = new ArrayList<>();
    boolean _collectable = true;
    
    protected Reference(int inst, List<String> sig, Context ctxt) {
      assert ctxt._instructions.length > inst;
      _instruction = inst;
      _signature = sig;
      _ctxt = ctxt;
    }
    
    protected Reference(int inst, List<String> sig, List<Reference> originPath, Context ctxt) {
      this(inst, sig, ctxt);
      _originPath = originPath;
    }
    
    public String toString() {
      return String.join(".", _signature);
    }
    
    public boolean equals(Object other) {
      return _signature != null && other != null && ((Reference) other)._signature != null
             && Arrays.deepEquals(_signature.toArray(), ((Reference) other)._signature.toArray());
      // it is more tricky going for the instruction here because we might have collected a
      // reference twice at different places in the code.
      // && _instruction == ((Reference) other)._instruction;
    }
    
    public AbstractInsnNode resolveInsn() {
      return _ctxt._instructions[_instruction];
    }
    
    public Frame resolveFrame() {
      return _ctxt._frames[_instruction];
    }
    
    public int getAssignmentOrigin() {
      // the context must be preserved!
      if(_originPath.isEmpty()) return _instruction;
      else {
        Reference r = _originPath.get(_originPath.size() - 1);
        if(r._ctxt == _ctxt) return r._instruction;
        else return _instruction;
      }
    }
    
    public Reference shallowCopy() {
      Reference copy = new Reference(_instruction, new ArrayList<String>(_signature), _ctxt);
      copy._originPath.addAll(_originPath);
      copy._collectable = _collectable;
      return copy;
    }
  }
  
  /**
   * "contains" uses "equals" and does not require "hashCode" to be implemented.
   * @param l
   * @param r
   */
  protected static void cAdd(List<Reference> l, Reference r) {
    if(!l.contains(r) && r._collectable) l.add(r);
    // else
    // System.out.println("Dropping already collected reference: " + r);
  }
  
  protected static void cAddAll(List<Reference> l, List<Reference> rs) {
    for(Reference r : rs)
      cAdd(l, r);
  }
  
  /**
   * We first find the origin of this object in the current basic block. Afterwards, we can
   * perform a forward analysis on the whole method.
   * 
   * @param aaStoreInstruction
   * @param instructions
   * @param frames
   * @throws AnalysisException
   */
  protected static List<Reference>
      collectReferences(int aaStoreInstruction, Context c, boolean collectTop) throws AnalysisException
  {
    // System.out.println("Collecting in context: " + c._class.name + "." + c._method.name);
    List<Reference> origins =
        handleAssignment(c._frames[aaStoreInstruction].getStackSize(), aaStoreInstruction, c, true);
    List<Reference> found = new ArrayList<>();
    for(Reference origin : origins) {
      found.addAll(collect(origin.getAssignmentOrigin(), origin._ctxt, true));
      // don't forget the top level reference ...
      
      // in case of a variable (ALOAD), we do an origin detection in the above collect function
      // which in the end already collects this top-level reference!
      if(collectTop && !isImmutable(origin._instruction, origin._ctxt) && origin._signature != null
         && origin.resolveInsn().getOpcode() != Opcodes.ALOAD)
      {
        // ... if it is immutable, collectable and has not been collected yet!
        cAdd(found, origin);
      }
    }
    return found;
  }
  
  protected static List<Reference> collectLocalVar(int inst, int local, Context c) throws AnalysisException {
    Set<Integer> locals = new HashSet<>();
    locals.add(local);
    // a forward pass for the local
    List<Reference> found = bbCollect(inst, locals, c);
    List<Reference> finalFound = new ArrayList<>();
    // for newly detected locals we have to go again another round
    // FIXME needs to be a recursive loop similar to collect until fixpoint!
    for(Reference r : found) {
      AbstractInsnNode instNode = r.resolveInsn();
      if(instNode.getType() == AbstractInsnNode.VAR_INSN
      // && ((VarInsnNode) instNode).var != local
         && r._ctxt == c)
      {
        cAddAll(finalFound, collectUntilFixpoint(r._instruction, c));
      }
      cAdd(finalFound, r);
    }
    return finalFound;
  }
  
  private static Var getSSA(Reference ref) throws AnalysisException {
    assert ref._ctxt._instructions[ref._instruction].getType() == AbstractInsnNode.VAR_INSN;
    // at this point, we want to do a deep dive to really understand where the heck that
    // reference comes from.
    int local = ((VarInsnNode) ref._ctxt._instructions[ref._instruction]).var;
    return ref._ctxt._ssa.getSSAFromCode(local, ref._instruction, ref._ctxt._frames);
  }
  
  /**
   * This is the collection pass for locals.
   * 
   * @param c
   * @return
   * @throws CompilationException
   */
  private static List<Reference> findSSAOrigins(Reference ref) throws AnalysisException {
    assert ref._ctxt._instructions[ref._instruction].getType() == AbstractInsnNode.VAR_INSN;
    // at this point, we want to do a deep dive to really understand where the heck that
    // reference comes from.
    int local = ((VarInsnNode) ref._ctxt._instructions[ref._instruction]).var;
    Var ssaVar = ref._ctxt._ssa.getSSAFromCode(local, ref._instruction, ref._ctxt._frames);
    if(ssaVar._start == 0) {
      return Collections.emptyList();
    } else {
      if(ssaVar._phiGenerated) {
        List<Reference> found = new ArrayList<>();
        for(Var pred : getAllNonPhiPredecessors(ssaVar, ref._ctxt)) {
          cAddAll(found, deepDive(pred._start, ref._ctxt));
        }
        return found;
      } else {
        return deepDive(ssaVar._start, ref._ctxt);
      }
    }
  }
  
  private static List<Reference> deepDive(int assignmentInst, Context c) throws AnalysisException {
    return deepDive(assignmentInst, c._frames[assignmentInst].getStackSize(), c);
  }
  
  private static List<Reference>
      deepDive(int assignmentInst, int trackedStackSlot, Context c) throws AnalysisException
  {
    // Assertion.invariant(c._instructions[assignmentInst].getOpcode() == Opcodes.ASTORE
    // || c._instructions[assignmentInst].getOpcode() == Opcodes.AASTORE);
    List<Reference> found = new ArrayList<>();
    List<Reference> refs = handleAssignment(trackedStackSlot, assignmentInst, c, true);
    for(Reference ref : refs) {
      switch(ref.resolveInsn().getOpcode()) {
        case Opcodes.INVOKEDYNAMIC:
        case Opcodes.INVOKEINTERFACE:
        case Opcodes.INVOKESPECIAL:
        case Opcodes.INVOKESTATIC:
        case Opcodes.INVOKEVIRTUAL:
          List<Reference> foundRefs =
              InterproceduralReferenceAnalysis.handleFunctionInvocation(ref._instruction,
                                                                        ref._ctxt._frames[ref._instruction].getStackSize(),
                                                                        (MethodInsnNode) c._instructions[ref._instruction],
                                                                        new HashSet<Integer>(),
                                                                        c,
                                                                        true);
          if(foundRefs.isEmpty()) {
            // it may happen that we deep dive but do not find an origin because the origin is
            // the return value that was untainted then the origin must be the current
            // assignment instruction.
            
            if(c._instructions[assignmentInst].getType() == AbstractInsnNode.VAR_INSN) {
              found.add(constructReferenceForVar(assignmentInst,
                                                 assignmentInst,
                                                 new ArrayList<>(),
                                                 new ArrayList<>(),
                                                 c));
            } else {
              found.add(ref);
            }
          } else {
            cAddAll(found, foundRefs);
          }
          break;
        case Opcodes.ALOAD:
          // we found a local variable, is it the best we can do in terms of resolution?!
          Reference currentVar = ref;
          int local = ((VarInsnNode) currentVar.resolveInsn()).var;
          Var ssaVar = c._ssa.getSSAFromCode(local, currentVar._instruction, currentVar._ctxt._frames);
          if(ssaVar._count == 0) {
            if(c._aliases.containsKey(ssaVar)) cAdd(found, c._aliases.get(ssaVar).shallowCopy());
            else cAdd(found, currentVar);
            break;
          } else {
            // iterate again
            List<Reference> r = findSSAOrigins(ref);
            if(r.isEmpty()) {
              found.add(ref);
            } else {
              for(Reference a : r)
                if(!found.contains(a)) found.add(a);
            }
          }
          break;
        default:
          if(!found.contains(ref)) found.add(ref);
      }
    }
    
    // if(found.isEmpty()) {
    // // covers the cAddAll case for the functional branch where nothing is returned.
    //
    // // it may happen that we deep dive but do not find an origin because the origin is the
    // // return value that was untainted then the origin must be the current assignment
    // // instruction.
    // Assertion.invariant(c._instructions[assignmentInst].getType() ==
    // AbstractInsnNode.VAR_INSN);
    // found.add(constructReferenceForVar(assignmentInst, assignmentInst, new ArrayList<>(), new
    // ArrayList<>(), c));
    // }
    return found;
  }
  
  private static List<Var> getAllNonPhiPredecessors(Var ssaVar, Context c) {
    List<Var> preds = new ArrayList<>();
    BasicBlock bb = (BasicBlock) c._frames[ssaVar._start];
    for(BasicBlock pred : bb._predecessors) {
      if(pred._end > bb._start) {
        // skip loops
      } else {
        Var predSsaVar = c._ssa.getSSAFromCode(ssaVar._local, pred._end, c._frames);
        if(predSsaVar._phiGenerated) preds.addAll(getAllNonPhiPredecessors(predSsaVar, c));
        // don't count the initial var because there must not be a usage before!
        else if(predSsaVar._count > 0) preds.add(predSsaVar);
      }
    }
    return preds;
  }
  
  private static List<Reference> correctSSAOrigins(Reference ref, List<Reference> origins) {
    if(origins.isEmpty()) {
      // this was a parameter of the function. don't throw it away!
      return Collections.singletonList(ref);
    } else {
      // System.out.println("Reference: " + ref);
      // System.out.println("Computed SSA origin: " + origins);
      List<Reference> corrected = new ArrayList<>();
      for(Reference origin : origins) {
        // TODO we have to give a direct origin here!
        List<String> signature = new ArrayList<>();
        signature.addAll(origin._signature);
        if(ref._signature.size() > 1) signature.addAll(ref._signature.subList(1, ref._signature.size()));
        
        List<Reference> originPath = new ArrayList<>();
        originPath.addAll(origin._originPath);
        originPath.addAll(ref._originPath);
        originPath.add(ref);
        Reference r = new Reference(ref._instruction, signature, originPath, ref._ctxt);
        r._collectable = origin._collectable;
        cAdd(corrected, r);
      }
      return corrected;
    }
  }
  
  protected static boolean isImmutable(int inst, Context c) {
    switch(c._instructions[inst].getType()) {
      case AbstractInsnNode.VAR_INSN:
        return isImmutableLocal((VarInsnNode) c._instructions[inst], c);
      case AbstractInsnNode.FIELD_INSN:
        Type t = Type.getType(((FieldInsnNode) c._instructions[inst]).desc);
        return AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName());
      case AbstractInsnNode.TYPE_INSN:
        t = Type.getObjectType(((TypeInsnNode) c._instructions[inst]).desc);
        return AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName());
      default:
        if(c._frames.length > inst + 1) {
          Value v = c._frames[inst + 1].getStack(c._frames[inst + 1].getStackSize() - 1);
          if(v instanceof BasicValue) {
            t = ((BasicValue) v).getType();
            return AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName());
          } else {
            return false;
          }
        } else {
          return false;
        }
    }
  }
  
  private static boolean isImmutableLocal(VarInsnNode varInsn, Context c) {
    LocalVariableNode l = (LocalVariableNode) c._method.localVariables.get(varInsn.var);
    Type t = Type.getType(l.desc);
    return AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getClassName());
  }
  
  private static List<Reference>
      collect(int origin, Context c, boolean isAssignmentOrigin) throws AnalysisException
  {
    switch(c._instructions[origin].getOpcode()) {
      case Opcodes.NEW:
        // track the stack (forward from origin)
        return collect(origin, c._instructions.length - 1, true, new HashSet<>(), new HashSet<Integer>(), c);
      case Opcodes.ALOAD:
        // track the local accesses in the basic blocks of the function (note, once we discover
        // the instruction on a conditional branch, we do not have to go through the whole
        // program anymore. our search should probably check, if the local is present anyhow in
        // this basic block.)
        return collectUntilFixpoint(origin, c);
      case Opcodes.INVOKEDYNAMIC:
      case Opcodes.INVOKEINTERFACE:
      case Opcodes.INVOKESPECIAL:
      case Opcodes.INVOKESTATIC:
      case Opcodes.INVOKEVIRTUAL:
        return new ArrayList<Reference>(InterproceduralReferenceAnalysis.handleFunctionInvocation(origin,
                                                                                                  c._frames[origin].getStackSize(),
                                                                                                  (MethodInsnNode) c._instructions[origin],
                                                                                                  new HashSet<Integer>(),
                                                                                                  c,
                                                                                                  true));
      case Opcodes.GETSTATIC:
      case Opcodes.GETFIELD:
        // May happen when this is an origin retrieval. Nothing to be done.
        if(!isAssignmentOrigin) {
          System.err.println("We collect on an origin which is never a field but a local or such. ");
          System.err.println("Can you please submit a bug on this along with the operator/code that you tried to compile/analyze.");
        }
        return new ArrayList<Reference>();
      case Opcodes.AALOAD:
        // May happen when this is an origin retrieval. Nothing to be done.
        if(!isAssignmentOrigin) {
          System.err.println("We collect on an origin which is never a load from an array but a local or such.");
          System.err.println("Can you please submit a bug on this along with the operator/code that you tried to compile/analyze.");
        }
        return new ArrayList<Reference>();
      default:
        // May happen when this is an origin retrieval. Nothing to be done.
        if(!isAssignmentOrigin) {
          System.err.println("Unsupported case detected!");
          System.err.println("Can you please submit a bug on this along with the operator/code that you tried to compile/analyze.");
        }
        return new ArrayList<Reference>();
    }
  }
  
  /**
   * Input to this method is a var instruction that marks a target object. The method collects
   * all source references that get assigned to it.
   * 
   * @param inst
   * @param c
   * @return
   * @throws CompilationException
   */
  private static List<Reference> collectUntilFixpoint(int inst, Context c) throws AnalysisException {
    List<Reference> found = new ArrayList<>();
    List<Reference> currentFound = new ArrayList<>();
    Reference r = new Reference(inst, Collections.emptyList(), c);
    r._collectable = false;
    
    List<Reference> ssaOrigins = findSSAOrigins(r);
    cAddAll(found, correctSSAOrigins(r, ssaOrigins));
    // FIXME it is unclear at this point where we should start as we might have gone back
    // several local variables to find the true origin. it is not clear whether we need any sort
    // of collection pass here at all. all we want is the correct reference in order to scope
    // the things that we find.
    // collection passes for the origins (stack-based)
    for(Reference ssaOrigin : ssaOrigins) {
      if(ssaOrigin._ctxt == c) {
        Set<Integer> trackedStackSlots = new HashSet<>();
        trackedStackSlots.add(ssaOrigin._ctxt._frames[ssaOrigin._instruction + 1].getStackSize());
        c._aliases.put(getSSA(r), ssaOrigin);
        cAddAll(currentFound,
                collect(ssaOrigin._instruction + 1, inst, true, trackedStackSlots, new HashSet<>(), c));
      }
    }
    
    // collection pass for the SSA
    cAddAll(currentFound, bbCollect(inst, c));
    
    // if we traverse shared references then it might be that we retrieved the GETFIELD
    // operation which loaded a field that was then shared. we collected that already.
    List<Reference> ownUsedRefs =
        collectOwnUsed(inst, c).stream().filter(co -> !found.contains(co)).collect(Collectors.toList());
    for(Reference ownUsedRef : ownUsedRefs) {
      int assignmentOrigin = ownUsedRef.getAssignmentOrigin();
      if(ownUsedRef._ctxt._instructions[assignmentOrigin].getType() == AbstractInsnNode.FIELD_INSN
         && !isImmutable(assignmentOrigin, ownUsedRef._ctxt))
      {
        // TODO if those are still variables then we have to go another round!
        // TODO find out whether this field was assigned some else before -> SSA for fields
        cAdd(currentFound, ownUsedRef);
      }
    }
    List<Reference> alreadyCollected = new ArrayList<>();
    while(!currentFound.isEmpty()) {
      Reference ref = currentFound.remove(0);
      if(alreadyCollected.contains(ref)) {
        // skip it because we already performed the analysis
      } else {
        alreadyCollected.add(ref);
        // TODO again this is dead simple to be parallelized when needed!
        if(ref._ctxt == c) {
          if(ref.resolveInsn().getType() == AbstractInsnNode.VAR_INSN && !found.contains(ref)) {
            // only locals ...
            if(!isImmutableLocal((VarInsnNode) ref.resolveInsn(), ref._ctxt)) {
              // ... of mutable type require another round
              
              ssaOrigins = findSSAOrigins(ref);
              
              // if we take another round for this variable then we do not collect it but the
              // origin that we find below.
              ref._collectable = ssaOrigins.isEmpty();
              cAddAll(found, correctSSAOrigins(ref, ssaOrigins));
              // collection passes for the origins (stack-based)
              for(Reference ssaOrigin : ssaOrigins) {
                if(ssaOrigin._ctxt == c) {
                  Set<Integer> trackedStackSlots = new HashSet<>();
                  trackedStackSlots.add(ssaOrigin._ctxt._frames[ssaOrigin._instruction + 1].getStackSize());
                  ref._ctxt._aliases.put(getSSA(ref), ssaOrigin);
                  cAddAll(currentFound,
                          collect(ssaOrigin._instruction + 1,
                                  ref._instruction,
                                  true,
                                  trackedStackSlots,
                                  new HashSet<>(),
                                  ref._ctxt));
                }
              }
              
              cAddAll(currentFound, bbCollect(ref._instruction, ref._ctxt));
              
              // if we traverse shared references then it might be that we retrieved the
              // GETFIELD operation which loaded a field that was then shared. we collected that
              // already.
              cAddAll(found,
                      collectOwnUsed(ref._instruction, c).stream().filter(co -> !found.contains(co)).collect(Collectors.toList()));
            } else {
              // drop the ref because it points to an immutable object
              continue;
            }
          } else {
            // it is not a local so no need to do anything else or we already took care of this
            // local
          }
        } else {
          // the iteration over variables from other functions already took place, so just
          // collect
        }
        cAdd(found, ref);
      }
    }
    return found;
  }
  
  private static List<Reference> bbCollect(int inst, Context c) throws AnalysisException {
    switch(c._instructions[inst].getOpcode()) {
      case Opcodes.ASTORE:
      case Opcodes.ALOAD:
        int localIdx = ((VarInsnNode) c._instructions[inst]).var;
        Set<Integer> locals = new HashSet<Integer>();
        locals.add(localIdx);
        return bbCollect(inst, locals, c);
      case Opcodes.GETSTATIC:
        // tracking down static access is inherent to the collection algorithm.
        return bbCollect(new HashSet<Integer>(), c);
      default:
        assert false : "Unsupported instruction type for reference collection detetcted: "
                       + c._instructions[inst].getOpcode();
        return null;
    }
  }
  
  private static List<Reference> bbCollect(Set<Integer> locals, Context c) throws AnalysisException {
    return bbCollect(-1, locals, c);
  }
  
  private static List<Reference>
      bbCollect(int start, final Set<Integer> locals, Context c) throws AnalysisException
  {
    assert locals.size() == 1;
    // iterate the basic blocks one by one and collect
    Var ssaVar = c._ssa.getSSAFromCode(locals.iterator().next(), start, c._frames);
    final int s = ssaVar._start;
    Map<BasicBlock, Integer> scopes = null;
    
    try {
      scopes = c._ssa.getDefinitionRange(locals.iterator().next(), start, c._frames);
    }
    catch(NullPointerException npe) {
      System.out.println(c._class.name);
      System.out.println(ssaVar._name);
    }
    
    try {
      if(ReturnArrayReferenceAnalysis.PARALLEL) {
        return scopes.entrySet().parallelStream().map(e -> collectOnBlock(s, locals, c, e)).flatMap(l -> l.stream()).collect(Collectors.toList());
      } else {
        if(ReturnArrayReferenceAnalysis.GOOD_OLD_LOOPS) {
          List<Reference> references = new ArrayList<>();
          for(Map.Entry<BasicBlock, Integer> entry : scopes.entrySet()) {
            references.addAll(collectOnBlock(s, locals, c, entry));
          }
          return references;
        } else {
          return scopes.entrySet().stream().map(e -> collectOnBlock(s, locals, c, e)).flatMap(l -> l.stream()).collect(Collectors.toList());
        }
      }
    }
    catch(RuntimeException r) {
      if(r.getCause() != null && r.getCause() instanceof AnalysisException) throw (AnalysisException) r.getCause();
      else throw r;
    }
  }
  
  private static List<Reference> collectOnBlock(int start, Set<Integer> locals, Context c,
                                                Map.Entry<BasicBlock, Integer> scope)
  {
    Set<Integer> trackedStackSlots = new HashSet<Integer>();
    int s = -1;
    boolean isOrigin = false;
    BasicBlock bb = scope.getKey();
    if(bb._start <= start && start < bb._end) {
      s = start;
      isOrigin = true;
    } else {
      s = bb._start;
      isOrigin = false;
    }
    try {
      return collect(s, scope.getValue(), isOrigin, trackedStackSlots, new HashSet<Integer>(locals), c);
    }
    catch(AnalysisException e) {
      throw new RuntimeException(e);
    }
  }
  
  private static List<Reference> collect(int start, int end, boolean isOrigin, Set<Integer> trackedStackSlots,
                                         Set<Integer> trackedLocals, Context c) throws AnalysisException
  {
    List<Reference> found = new ArrayList<>();
    for(int j = start; j <= end; j++) {
      AbstractInsnNode instruction = c._instructions[j];
      // this function carries the wrong name. it actually returns the index of the top slot
      int topStackSlotIdx = c._frames[j] != null ? c._frames[j].getStackSize() : 0;
      // if this thing says 0 then the stack is empty! so the first stack position is actually
      // 1.
      
      // TODO optimization: abort the analysis when there are neither tainted stack slots nor
      // tainted locals left.
      if(j > 0) {
        DFACommons.cleanTrackedStackSlots(trackedStackSlots, c._frames[j - 1].getStackSize(), topStackSlotIdx);
      }
      
      switch(instruction.getType()) {
        case AbstractInsnNode.FIELD_INSN:
          switch(instruction.getOpcode()) {
            case Opcodes.PUTFIELD:
              if(trackedStackSlots.contains(topStackSlotIdx - 1)
                 && !AbstractOperatorAnalysis.NUMBER_TYPES.contains(Type.getType(((FieldInsnNode) instruction).desc).getClassName()))
              {
                // FIXME INVOKESTATIC
                // com/ohua/tests/lang/ReferenceAnalysisOperators$Container.access$0 this
                // happens when accessing a non public field in an inner class. we have to be
                // able to deal with this!
                
                // TODO it is unclear whether we should continue here because all we care about
                // is the very last assignment! -> but I assume that this also needs to be
                // performed on the block level!
                found.addAll(handleAssignment(topStackSlotIdx, j, c));
              }
              break;
            case Opcodes.GETFIELD:
              if(trackedStackSlots.contains(topStackSlotIdx)) {
                // this means that a part (field) of the object reference being monitored is
                // loaded onto the stack and can therefore be subject to further assignments.
                
                // we just add this to the tracked stack slots, that's all!
                // -> essentially this is being done already by the analysis because the top
                // stack slot is already a tracked object and this instruction just exchanges
                // the top stack slot. -> the stack remains tainted!
              }
              break;
            case Opcodes.PUTSTATIC:
              if(trackedStackSlots.contains(topStackSlotIdx)) {
                assert false : "Should not catch an assignment to a static Variable. "
                               + "Can you please submit a bug on this along with the operator/code that you tried to compile/analyze.";
              }
              break;
            case Opcodes.GETSTATIC:
              // can this ever happen at this point? I think the only case where this can happen
              // is when trying to detect the origin.
              if(trackedStackSlots.contains(topStackSlotIdx + 1)) {
                assert false : "Loading a static field to the stack is not an assignment and is neither performed on any object reference that could be monitored. "
                               + "Can you please submit a bug on this along with the operator/code that you tried to compile/analyze.";
              }
              break;
          }
          break;
        case AbstractInsnNode.METHOD_INSN:
          found.addAll(InterproceduralReferenceAnalysis.handleFunctionInvocation(j,
                                                                                 topStackSlotIdx,
                                                                                 (MethodInsnNode) instruction,
                                                                                 trackedStackSlots,
                                                                                 c,
                                                                                 isOrigin));
          break;
        case AbstractInsnNode.INSN:
          switch(instruction.getOpcode()) {
            case Opcodes.AASTORE:
              if(trackedStackSlots.contains(topStackSlotIdx - 2)) {
                // we really have to deep dive here because AASTORE instructions are not handle
                // again in the fixpoint loop. this accounts only for locals. since we have to
                // go for function return values here as well, we have to make a deep dive.
                found.addAll(deepDive(j, c));
              }
              break;
          }
          break;
        case AbstractInsnNode.VAR_INSN:
          int local = ((VarInsnNode) instruction).var;
          switch(instruction.getOpcode()) {
            case Opcodes.ASTORE:
              if(trackedStackSlots.contains(topStackSlotIdx)) {
                // a reference of the tracked state gets assigned to a different variable. we do
                // have to track it.
                Var ssaVar = c._ssa.getSSAFromCode(local, j + 1, c._frames);
                if(c._aliases.containsKey(ssaVar)) {
                  found.add(c._aliases.get(ssaVar).shallowCopy());
                } else {
                  Reference r =
                      new Reference(ssaVar._start, constructList(getLocalAccessDescription(j + 1, j, c)), c);
                  // we are setting this here because we say that we do not collect internally
                  // assigned (SSA) locals (only ones that are passed to the function that we
                  // are investigating and those should not have an ASTORE)
                  r._collectable = false;
                  found.add(r);
                }
              }
              break;
          }
          break;
        default:
          // all other instructions are not interesting to us at this moment
      }
      DFACommons.trackForward(instruction, topStackSlotIdx, trackedStackSlots, trackedLocals);
      if(isOrigin) {
        if(c._frames.length - 1 > j + 1) trackedStackSlots.add(c._frames[j + 1].getStackSize());
        isOrigin = false;
      }
    }
    return found;
  }
  
  private static List<Reference> handleAssignment(int stackSlot, int inst, Context c) throws AnalysisException {
    return handleAssignment(stackSlot, inst, c, false);
  }
  
  /**
   * This is the collection pass for the stack.
   * 
   * @param stackSlot
   * @param inst
   * @param c
   * @param methodSensitive - does not mean that it recurses into functions but gives back
   *          references to invocations!
   * @return
   * @throws CompilationException
   */
  private static List<Reference>
      handleAssignment(int stackSlot, int inst, Context c, boolean methodSensitive) throws AnalysisException
  {
    // find the origin of the object reference and record it -> basic block local
    List<Reference> refs = detectOriginReference(stackSlot, inst, c, methodSensitive);
    for(Reference ref : refs) {
      switch(ref.resolveInsn().getType()) {
        case AbstractInsnNode.VAR_INSN:
          switch(ref.resolveInsn().getOpcode()) {
            case Opcodes.ILOAD:
            case Opcodes.LLOAD:
            case Opcodes.FLOAD:
            case Opcodes.DLOAD:
            case Opcodes.ALOAD:
              // trace the object reference (recursion) and register the computed
              // reference -> done, because the reference is collected above and the
              // fixpoint recursion will do the recursion.
            case Opcodes.ASTORE:
              break;
            default:
              assert false : "Unsupported yet!";
          }
          break;
        case AbstractInsnNode.TYPE_INSN:
          switch(ref.resolveInsn().getOpcode()) {
            case Opcodes.NEW:
              // trace the parameters (recursion) <- there are no parameters to a
              // new statement! (they are invoked on the initialize() routine!)
              // then we have to find the initialize routine here! and recurs
              // into it. (needs to be performed by the fixpoint iteration.)
              break;
            case Opcodes.ANEWARRAY:
              // TODO we have to trace the array construction. other than that there
              // is nothing left to do because no other blocks have to be
              // investigated. -> done by fixpoint iteration tracing the ref
              break;
            default:
              assert false : "Unsupported yet!";
          }
          break;
        case AbstractInsnNode.METHOD_INSN:
          assert methodSensitive : "not supported yet";
          break;
      }
    }
    return refs;
  }
  
  protected static List<Reference>
      detectOriginReference(int topStackSlotIdx, int currentInst, Context c) throws AnalysisException
  {
    return detectOriginReference(topStackSlotIdx, currentInst, c, false);
  }
  
  protected static List<Reference> detectOriginReference(int topStackSlotIdx, int currentInst, Context c,
                                                         boolean methodSensitive) throws AnalysisException
  {
    Set<Integer> objectRef = new HashSet<>();
    objectRef.add(topStackSlotIdx);
    Set<Integer> originInsts =
        OriginDiscovery.findOrigin(currentInst, objectRef, c._instructions, c._frames, methodSensitive);
    List<Reference> refs = new ArrayList<>();
    for(Integer originInst : originInsts) {
      List<Reference> rs =
          constructReferences(topStackSlotIdx, currentInst, originInst, new ArrayList<>(), c, methodSensitive);
      for(Reference r : rs)
        if(!refs.contains(r)) refs.add(r);
    }
    return refs;
  }
  
  private static List<Reference>
      constructReferences(int topStackSlotIdx, int currentInst, int originInst, List<String> path, Context c,
                          boolean methodSensitive) throws AnalysisException
  {
    List<Reference> originPath = new ArrayList<>();
    
    // handle constants
    if(Opcodes.ACONST_NULL <= c._instructions[originInst].getOpcode()
       && c._instructions[originInst].getOpcode() <= Opcodes.LDC)
    {
      String id = handleConstants(originInst, c);
      Reference r = new Reference(originInst, constructList(id), new ArrayList<>(), c);
      r._collectable = false;
      return constructList(r);
    }
    
    // handle operational origins such as ADD SUB etc. this is especially important when
    // checking for array indexes.
    if(Opcodes.IADD <= c._instructions[originInst].getOpcode()
       && c._instructions[originInst].getOpcode() <= Opcodes.IINC)
    {
      String opId = handleOperations(originInst, c);
      Reference r = new Reference(originInst, constructList(opId), new ArrayList<>(), c);
      r._collectable = false;
      return constructList(r);
    }
    
    // handle functions
    if(c._instructions[originInst].getType() == AbstractInsnNode.METHOD_INSN) {
      // TODO this is really tricky as the true origin can potentially come from various
      // places inside the function due to different branches in the CFG. we will use a call
      // site description as an identifier. furthermore, we will consult the knowledge base to
      // understand whether this function releases an item. if so then we will create a string
      // that marks this origin as unique!
      if(methodSensitive) {
        // this happens when the result is not even stored somewhere in a local variable or
        // such. the returned reference is not for evaluation purposes but for further
        // processing.
        return constructList(new Reference(originInst, null, c));
      } else {
        assert false : "Origin would actually have been a function!";
      }
    }
    
    switch(c._instructions[originInst].getOpcode()) {
      case Opcodes.ILOAD:
      case Opcodes.LLOAD:
      case Opcodes.FLOAD:
      case Opcodes.DLOAD:
      case Opcodes.ALOAD:
        return constructList(constructReferenceForVar(currentInst, originInst, path, originPath, c));
      case Opcodes.GETFIELD:
        path.add(0, ((FieldInsnNode) c._instructions[originInst]).name);
        originPath.add(0, new Reference(originInst, constructList(path.get(0)), c));
        // System.out.println("Recursion for field: " + path.get(0));
        // recursion -> does not traverse into functions.
        if(ReferenceCollector.isImmutable(originInst, c)) {
          Reference r = new Reference(originInst, constructList(path.get(0)), c);
          r._collectable = false;
          return constructList(r);
        } else {
          // so if I find a field then all I want to do is to construct the correct
          // reference where reference means all local variables on the path need to be properly
          // resolved.
          List<Reference> refs = deepDive(originInst, c);
          List<Reference> foundRefs = new ArrayList<>();
          for(Reference ref : refs) {
            Reference r = new Reference(originInst, new ArrayList<String>(ref._signature), c);
            r._signature.addAll(path);
            r._originPath.addAll(ref._originPath);
            r._originPath.addAll(originPath);
            foundRefs.add(r);
          }
          // System.out.println("Found field access: " + ((FieldInsnNode)
          // c._instructions[originInst]).name);
          // System.out.println("Constructed references for field: " +
          // Arrays.deepToString(refs.toArray()));
          return foundRefs;
        }
      case Opcodes.GETSTATIC:
        Reference r = new Reference(originInst, path, originPath, c);
        r._collectable = !ReferenceCollector.isImmutable(originInst, c);
        path.add(0, ((FieldInsnNode) c._instructions[originInst]).owner + "."
                    + ((FieldInsnNode) c._instructions[originInst]).name);
        return constructList(r);
      case Opcodes.NEWARRAY:
        path.add(0, "*" + Type.getDescriptor(int.class) + ":" + originInst + "*");
        return constructList(new Reference(originInst, path, originPath, c));
      case Opcodes.ANEWARRAY:
      case Opcodes.NEW:
        if(isImmutable(originInst, c)) return Collections.emptyList();
        else {
          path.add(0, "*" + ((TypeInsnNode) c._instructions[originInst]).desc + ":" + originInst + "*");
          return constructList(new Reference(originInst, path, originPath, c));
        }
      case Opcodes.MULTIANEWARRAY:
        // TODO -> MultiANewArrayInsnNode
        assert false : "Unsupported yet.";
        break;
      case Opcodes.AALOAD:
        // translated this is nothing else but accessing a field of an object so we essentially
        // have to perform the very same steps here. the only difference is that we need to find
        // the "name" of the field which is a computed index/key.
        
        // track down the origin of an index -> not sure we can go that far because it
        // might encompass multiple stack instructions. I guess we should at least try for
        // shared locals loaded via ALOAD
        List<Reference> idxOrigins = deepDive(originInst, c);
        
        // find the origin of the array reference
        List<Reference> arrayOrigins = deepDive(originInst, c._frames[originInst].getStackSize() - 1, c);
        
        List<Reference> refs = new ArrayList<>();
        for(Reference arrayOrigin : arrayOrigins) {
          AbstractInsnNode array = arrayOrigin.resolveInsn();
          if(array instanceof VarInsnNode) {
            @SuppressWarnings("unchecked")
            LocalVariableNode lvn =
                (LocalVariableNode) arrayOrigin._ctxt._method.localVariables.get(ASMUtils.translateCodeIndexToParameterIndex(((VarInsnNode) array).var,
                                                                                                                             arrayOrigin._ctxt._method.localVariables));
            Type t = Type.getType(lvn.desc);
            if(t.getSort() == Type.ARRAY && AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getElementType().getClassName()))
            {
              // skip references to immutable objects
            } else {
              for(Reference idxOrigin : idxOrigins) {
                List<String> np = new ArrayList<>();
                np.addAll(arrayOrigin._signature);
                String idx = handleArrayIndexOrigin(currentInst, idxOrigin._instruction, c);
                np.add("<" + idx + ">");
                Reference nr = new Reference(currentInst, np, c);
                nr._originPath.addAll(arrayOrigin._originPath);
                refs.add(nr);
              }
            }
          }
        }
        
        return refs;
      default:
        assert false;
    }
    assert false;
    return null;
  }
  
  private static Reference constructReferenceForVar(int currentInst, int originInst, List<String> path,
                                                    List<Reference> originPath, Context c)
  {
    int local = ((VarInsnNode) c._instructions[originInst]).var;
    // System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    // System.out.println("Local: " + local);
    // System.out.println("Aliases: " + c._aliases);
    Var v = c._ssa.getSSAFromCode(local, currentInst, c._frames);
    // System.out.println("SSA Var (current instruction): " + v._name + ":" + v._count +
    // " > " + v._phiGenerated);
    if(c._aliases.containsKey(v)) {
      Reference alias = c._aliases.get(v);
      // System.out.println("Local: " + local + " Found alias:" + alias);
      path.addAll(0, alias._signature);
      originPath.addAll(0, alias._originPath);
      originPath.add(new Reference(originInst, path, originPath, c));
    } else {
      // only register if it is a mutable object reference
      String ssaId = getLocalAccessDescription(currentInst, originInst, c);
      path.add(0, ssaId);
    }
    // System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    return new Reference(originInst, path, originPath, c);
    
  }
  
  @SafeVarargs
  private static <T> List<T> constructList(T... r) {
    List<T> a = new ArrayList<>();
    for(T ref : r)
      a.add(ref);
    return a;
  }
  
  /**
   * Currently we do not distinguish between the type or the arguments of an operation. This can
   * later on be made more context-sensitive.
   * 
   * @param currentInst
   * @param c
   * @return
   */
  private static String handleOperations(int currentInst, Context c) {
    switch(c._instructions[currentInst].getOpcode()) {
      case Opcodes.IADD:
      case Opcodes.LADD:
      case Opcodes.FADD:
      case Opcodes.DADD:
      case Opcodes.ISUB:
      case Opcodes.LSUB:
      case Opcodes.FSUB:
      case Opcodes.DSUB:
      case Opcodes.IMUL:
      case Opcodes.LMUL:
      case Opcodes.FMUL:
      case Opcodes.DMUL:
      case Opcodes.IDIV:
      case Opcodes.LDIV:
      case Opcodes.FDIV:
      case Opcodes.DDIV:
      case Opcodes.IREM:
      case Opcodes.LREM:
      case Opcodes.FREM:
      case Opcodes.DREM:
      case Opcodes.INEG:
      case Opcodes.LNEG:
      case Opcodes.FNEG:
      case Opcodes.DNEG:
      case Opcodes.ISHL:
      case Opcodes.LSHL:
      case Opcodes.ISHR:
      case Opcodes.LSHR:
      case Opcodes.IUSHR:
      case Opcodes.LUSHR:
      case Opcodes.IAND:
      case Opcodes.LAND:
      case Opcodes.IOR:
      case Opcodes.LOR:
      case Opcodes.IXOR:
      case Opcodes.LXOR:
      case Opcodes.IINC: // visitIincInsn
        return "*OPERATION*:" + currentInst;
      default:
        assert false : "Unknown operation detected: " + c._instructions[currentInst].getOpcode();
        return null;
    }
    
  }
  
  private static String handleConstants(int currentInst, Context c) throws AnalysisException {
    switch(c._instructions[currentInst].getOpcode()) {
      case Opcodes.ACONST_NULL:
        return "null";
      case Opcodes.ICONST_M1:
      case Opcodes.ICONST_0:
      case Opcodes.LCONST_0:
      case Opcodes.FCONST_0:
      case Opcodes.DCONST_0:
        return "0";
      case Opcodes.ICONST_1:
      case Opcodes.LCONST_1:
      case Opcodes.FCONST_1:
      case Opcodes.DCONST_1:
        return "1";
      case Opcodes.ICONST_2:
      case Opcodes.FCONST_2:
        return "2";
      case Opcodes.ICONST_3:
        return "3";
      case Opcodes.ICONST_4:
        return "4";
      case Opcodes.ICONST_5:
        return "5";
      case Opcodes.BIPUSH:
      case Opcodes.SIPUSH:
        return Integer.toString(((IntInsnNode) c._instructions[currentInst]).operand);
      case Opcodes.LDC:
        return ((LdcInsnNode) c._instructions[currentInst]).cst.toString();
      default:
        assert false;
        return null;
    }
  }
  
  private static String handleArrayIndexOrigin(int currentInst, int origin, Context c) throws AnalysisException {
    if(Opcodes.ICONST_0 <= c._instructions[origin].getOpcode()
       && c._instructions[origin].getOpcode() <= Opcodes.LDC)
    {
      return handleConstants(origin, c);
    } else if(Opcodes.IADD <= c._instructions[origin].getOpcode()
              && c._instructions[origin].getOpcode() <= Opcodes.IINC)
    {
      return handleOperations(origin, c);
    } else {
      switch(c._instructions[origin].getOpcode()) {
        case Opcodes.ILOAD:
        case Opcodes.LLOAD:
          return constructReferenceForVar(currentInst, origin, new ArrayList<>(), new ArrayList<>(), c).toString();
        default:
          Exception e =
              new Exception("Unsupported array index computation detected. Origin opcode: "
                            + c._instructions[origin].getOpcode());
          throw new AnalysisException(e);
      }
    }
  }
  
  protected static String getLocalAccessDescription(int currentInst, int originInst, Context c) {
    int local = ((VarInsnNode) c._instructions[originInst]).var;
    Var ssaVar = c._ssa.getSSAFromCode(local, currentInst, c._frames);
    return "[" + ssaVar._name + ":" + ssaVar._count + "]";
  }
  
  /**
   * Collects all fields of the target object that were loaded to the stack via GETFIELD and
   * therefore mark references that are contained in the object reference that we collected.
   * 
   * @return
   * @throws CompilationException
   */
  private static List<Reference> collectOwnUsed(int inst, Context c) throws AnalysisException {
    List<Reference> found = new ArrayList<>();
    Set<Integer> trackedStackSlots = new HashSet<>();
    Set<Integer> trackedLocals = new HashSet<>();
    int start = -1;
    switch(c._instructions[inst].getOpcode()) {
      case Opcodes.ALOAD:
        trackedLocals.add(((VarInsnNode) c._instructions[inst]).var);
        start = ((BasicBlock) c._frames[inst])._start;
        break;
      case Opcodes.NEW:
        trackedStackSlots.add(c._frames[inst].getStackSize() + 1);
        //$FALL-THROUGH$
      default:
        start = inst;
    }
    
    for(int j = start + 1; j <= ((BasicBlock) c._frames[inst])._end; j++) {
      AbstractInsnNode instruction = c._instructions[j];
      // this function carries the wrong name. it actually returns the index of the top slot
      int topStackSlotIdx = c._frames[j] != null ? c._frames[j].getStackSize() : 0;
      // if this thing says 0 then the stack is empty! so the first stack position is actually
      // 1.
      
      // TODO optimization: abort the analysis when there are neither tainted stack slots nor
      // tainted locals left.
      
      DFACommons.cleanTrackedStackSlots(trackedStackSlots, c._frames[j - 1].getStackSize(), topStackSlotIdx);
      
      switch(instruction.getOpcode()) {
        case Opcodes.GETFIELD:
          if(trackedStackSlots.contains(topStackSlotIdx)) {
            found.addAll(detectOriginReference(topStackSlotIdx, j + 1, c));
          }
      }
      
      DFACommons.trackForward(instruction, topStackSlotIdx, trackedStackSlots, trackedLocals);
    }
    return found;
  }
}
