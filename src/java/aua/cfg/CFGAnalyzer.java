/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;

public class CFGAnalyzer extends Analyzer {
  
  /**
   * Comparison only works inside the same CFG.
   * 
   * @author sertel
   *
   */
  public class BasicBlock extends Frame {
    public Set<BasicBlock> _predecessors = new HashSet<BasicBlock>();
    public Set<BasicBlock> _successors = new HashSet<BasicBlock>();
    public int _start = -1;
    public int _end = -1;
    // those are predecessors why end a loop. (feedback arcs)
    public Set<BasicBlock> _loopBacks = new HashSet<BasicBlock>();
    
    public BasicBlock(int nLocals, int nStack) {
      super(nLocals, nStack);
    }
    
    public BasicBlock(Frame src) {
      super(src);
    }
    
    /**
     * Comparison only works inside the same CFG.
     */
    public int hashCode() {
      return Objects.hash(_start, _end);
    }
    
    public boolean equals(Object other) {
      return other instanceof BasicBlock && _start == ((BasicBlock) other)._start
             && _end == ((BasicBlock) other)._end;
    }
  }
  
  private Map<Integer, Set<Integer>> _srcToDst = new HashMap<>();
  private Map<Integer, Set<Integer>> _dstToSrc = new HashMap<>();
  
  private Map<Integer, Set<Integer>> _exceptionBlockToSrc = new HashMap<>();
  
  public CFGAnalyzer(Interpreter interpreter) {
    super(interpreter);
  }
  
  protected Frame newFrame(int nLocals, int nStack) {
    return new BasicBlock(nLocals, nStack);
  }
  
  protected Frame newFrame(Frame src) {
    return new BasicBlock(src);
  }
  
  /**
   * We really need to save all pairs here and find common targets and common sources which
   * represent the control flow arcs. (This would place no assumptions on the numbering of the
   * instructions.) <br>
   * Additionally, this method is not only called on jumps but sadly also on LABEL, LINENUMBERS
   * etc.
   */
  protected void newControlFlowEdge(int src, int dst) {
    if(!_srcToDst.containsKey(src)) _srcToDst.put(src, new HashSet<Integer>());
    if(!_dstToSrc.containsKey(dst)) _dstToSrc.put(dst, new HashSet<Integer>());
    _srcToDst.get(src).add(dst);
    _dstToSrc.get(dst).add(src);
    // System.out.println("Received CF edge from " + src + " to " + dst);
  }
  
  /**
   * We can't handle these CF arcs the same as above because we receive essentially a call for
   * every single instruction that is located inside a try-block. Note: this list of arcs also
   * contains the arc that points from the last block in the subgraph to the catch block.
   */
  protected boolean newControlFlowExceptionEdge(final int src, final int dst) {
    if(!_exceptionBlockToSrc.containsKey(dst)) _exceptionBlockToSrc.put(dst, new HashSet<Integer>());
    _exceptionBlockToSrc.get(dst).add(src);
    // System.out.println("Received CF edge (exception) from " + src + " to " + dst);
    return true;
  }
  
  public Frame[] analyze(final String owner, final MethodNode m) throws AnalyzerException {
    Frame[] result = super.analyze(owner, m);
    compute(result, m.instructions.toArray());
    return result;
  }
  
  /**
   * This is brute-force but it makes no assumptions. We would need really a fix on the API
   * above to go around this.
   */
  private void compute(Frame[] frames, AbstractInsnNode[] insn) {
    
    // merge instructions
    for(Map.Entry<Integer, Set<Integer>> entry : _dstToSrc.entrySet()) {
      if(entry.getValue().size() > 1) {
        BasicBlock target = (BasicBlock) frames[entry.getKey()];
        target._start = entry.getKey();
        for(Integer src : entry.getValue()) {
          BasicBlock source = (BasicBlock) frames[src];
          source._end = src;
          source._successors.add(target);
          target._predecessors.add(source);
        }
      }
    }
    
    // branching instructions
    for(Map.Entry<Integer, Set<Integer>> entry : _srcToDst.entrySet()) {
      if(entry.getValue().size() > 1) {
        BasicBlock source = (BasicBlock) frames[entry.getKey()];
        source._end = entry.getKey();
        for(Integer dst : entry.getValue()) {
          BasicBlock target = (BasicBlock) frames[dst];
          target._start = dst;
          target._predecessors.add(source);
          source._successors.add(target);
        }
      }
    }
    
    /*
     * handle the exception blocks: exception blocks may encompass a subgraph of the CFG.
     * therefore, we find the block for each instruction and add a successor arc to the
     * exception block that points to the exception block.
     */
    for(Map.Entry<Integer, Set<Integer>> entry : _exceptionBlockToSrc.entrySet()) {
      BasicBlock exceptionBlock = (BasicBlock) frames[entry.getKey()];
      // create the catch block
      exceptionBlock._start = entry.getKey();
      // System.out.println(entry.getValue());
      // now it gets tricky: we need to find clusters to understand the blocks
      List<List<Integer>> clusters = getClusters(entry.getValue());
      // System.out.println(clusters);
      for(List<Integer> cluster : clusters) {
        int clusterStart = cluster.get(0);
        int clusterEnd = cluster.get(cluster.size() - 1);
        ((BasicBlock) frames[clusterStart])._start = clusterStart;
        ((BasicBlock) frames[clusterEnd])._end = clusterEnd;
        // System.out.println(clusterStart + " -> " + clusterEnd);
        
        // now we have to understand what the predecessors and successors are
        
        if(clusterStart > 0) {
          // the monitor exit code might point to itself!
          if(clusterStart != entry.getKey()) {
            ((BasicBlock) frames[clusterStart - 1])._end = clusterStart - 1;
            ((BasicBlock) frames[clusterStart - 1])._successors.add((BasicBlock) frames[clusterStart]);
            ((BasicBlock) frames[clusterStart])._predecessors.add((BasicBlock) frames[clusterStart - 1]);
          }
        }
        
        ((BasicBlock) frames[clusterEnd + 1])._start = clusterEnd + 1;
        ((BasicBlock) frames[clusterEnd])._successors.add((BasicBlock) frames[clusterEnd + 1]);
        ((BasicBlock) frames[clusterEnd + 1])._predecessors.add((BasicBlock) frames[clusterEnd]);
        
        // handle jumps here
        BasicBlock lastBlockOfByteCodeBeforeCatchBlock = (BasicBlock) frames[entry.getKey() - 1];
        lastBlockOfByteCodeBeforeCatchBlock._end = entry.getKey() - 1;
        
        // find the jump in the source-to-destination list (not covered because it has only a
        // single destination)
        // if there is no such entry then the previous instruction is just not a jump but most
        // likely a return statement.
        if(_srcToDst.containsKey(entry.getKey() - 1)) {
          Set<Integer> prev = _srcToDst.get(entry.getKey() - 1);
          assert prev.size() == 1;
          int startAfterCatch = prev.iterator().next();
          BasicBlock blockAfterCatch = (BasicBlock) frames[startAfterCatch];
          lastBlockOfByteCodeBeforeCatchBlock._successors.add(blockAfterCatch);
          blockAfterCatch._start = startAfterCatch;
          blockAfterCatch._predecessors.add(lastBlockOfByteCodeBeforeCatchBlock);
        }
        
        // fix the other blocks -> point them to the catch block
        for(Integer inst : entry.getValue()) {
          BasicBlock instHost = (BasicBlock) frames[inst];
          instHost._successors.add(exceptionBlock);
          exceptionBlock._predecessors.add(instHost);
        }
      }
    }
    
    // out of some reason the last entry in the frame list seems to be always null (maybe
    // because of the return statement???)
    int lastFrameIdx = lastFrameIndex(frames);
    
    ((BasicBlock) frames[0])._start = 0;
    ((BasicBlock) frames[lastFrameIdx])._end = lastFrameIdx;
    
    int i = 1;
    while(i < lastFrameIdx) {
      int j = i;
      // propagate information forward
      while(j <= lastFrameIdx && ((BasicBlock) frames[j])._start == -1) {
        ((BasicBlock) frames[j])._start = ((BasicBlock) frames[j - 1])._start;
        ((BasicBlock) frames[j])._predecessors = ((BasicBlock) frames[j - 1])._predecessors;
        j++;
      }
      
      // first instruction of the next block is j at this point
      int next = j;
      j--;
      
      // propagate information backwards
      while(i <= j) {
        ((BasicBlock) frames[j - 1])._end = ((BasicBlock) frames[j])._end;
        ((BasicBlock) frames[j - 1])._successors = ((BasicBlock) frames[j])._successors;
        j--;
      }
      
      // the loop starts with the index of the second instruction of the next block
      i = next + 1;
    }
    
    // final blocks
    int lastExit = lastFrameIdx;
    for(int j = lastFrameIdx; j > -1; j--) {
      if(Opcodes.IRETURN <= insn[j].getOpcode() && insn[j].getOpcode() <= Opcodes.RETURN
         || insn[j].getOpcode() == Opcodes.ATHROW) lastExit = j;
      if(((BasicBlock) frames[j])._end == -1) {
        ((BasicBlock) frames[j])._end = lastExit;
      }
    }
    
    // rehash to remove duplicates (only now the basic blocks hash will not change
    // anymore)
    for(int j = 0; j < frames.length - 1; j++) {
      // maintain the pointers though!
      HashSet<BasicBlock> s = new HashSet<BasicBlock>(((BasicBlock) frames[j])._successors);
      ((BasicBlock) frames[j])._successors.clear();
      ((BasicBlock) frames[j])._successors.addAll(s);
      HashSet<BasicBlock> p = new HashSet<BasicBlock>(((BasicBlock) frames[j])._predecessors);
      ((BasicBlock) frames[j])._predecessors.clear();
      ((BasicBlock) frames[j])._predecessors.addAll(p);
    }
    
    computeLoops(frames, insn);
    
    _exceptionBlockToSrc = null;
    _srcToDst = null;
    _dstToSrc = null;
  }
  
  private void computeLoops(Frame[] frames, AbstractInsnNode[] insn) {
    BasicBlock[] blocks = getBasicBlocks(frames);
    // loop header candidates have at least two outgoing arcs
    List<BasicBlock> loopHeaderCandidates =
        Arrays.stream(blocks).filter(a -> a._successors.size() > 1).collect(Collectors.toList());
    
    for(BasicBlock loopHeaderCandidate : loopHeaderCandidates) {
      // can I reach this
      detectLoopBacks(loopHeaderCandidate);
    }
  }
  
  private void detectLoopBacks(BasicBlock candidate) {
    for(BasicBlock succ : candidate._successors) {
      candidate._loopBacks = isReachable(candidate, succ, new ArrayList<>());
    }
  }
  
  private Set<BasicBlock> isReachable(BasicBlock candidate, BasicBlock current, List<BasicBlock> path) {
    path.add(current);
    HashSet<BasicBlock> loopBacks = new HashSet<>();
    for(BasicBlock succ : current._successors) {
      if(succ == candidate) {
        loopBacks.add(succ);
      } else {
        // skip loops
        if(!path.contains(succ)) loopBacks.addAll(isReachable(candidate, succ, path));
      }
    }
    return loopBacks;
  }
  
  private List<List<Integer>> getClusters(Set<Integer> values) {
    List<Integer> sorted = values.stream().sorted(Integer::compare).collect(Collectors.toList());
    List<List<Integer>> clusters = new ArrayList<>();
    List<Integer> current = new ArrayList<>();
    for(Integer i : sorted) {
      if(current.isEmpty() || current.get(current.size() - 1) + 1 == i) {
        current.add(i);
      } else {
        clusters.add(current);
        current = new ArrayList<>();
        current.add(i);
      }
    }
    clusters.add(current);
    return clusters;
  }
  
  private int lastFrameIndex(Frame[] frames) {
    int i = frames.length - 1;
    for(; frames[i] == null; i--) {}
    return i;
  }
  
  protected static BasicBlock[] getBasicBlocks(Frame[] frames) {
    List<BasicBlock> bb = new ArrayList<>();
    bb.add((BasicBlock) frames[0]);
    Set<Integer> seen = new HashSet<>();
    seen.add(((BasicBlock) frames[0])._start);
    List<Frame> next = new ArrayList<>(((BasicBlock) frames[0])._successors);
    while(!next.isEmpty()) {
      BasicBlock b = (BasicBlock) next.remove(0);
      // skip blocks that we already visited
      if(!seen.contains(b._start)) {
        seen.add(b._start);
        bb.add(b);
        next.addAll(b._successors);
      }
    }
    return bb.toArray(new BasicBlock[bb.size()]);
  }
  
  /**
   * Provides an iterator that walks along the topology of the CFG through the set of basic
   * blocks. This iterator will not traverse loop arcs!
   * 
   * @param basicBlocks
   * @return
   */
  protected static Iterator<BasicBlock> getIterator(final BasicBlock[] basicBlocks) {
    final Set<Integer> unvisited =
        Arrays.stream(basicBlocks).filter(b -> b != null).map((a) -> a._start).collect(Collectors.toSet());
    final Set<Integer> visited = new HashSet<>();
    final List<Frame> next = new ArrayList<>();
    next.add(basicBlocks[0]);
    return new Iterator<BasicBlock>() {
      BasicBlock _n = null;
      
      @Override
      public boolean hasNext() {
        while(!next.isEmpty()) {
          BasicBlock b = (BasicBlock) next.remove(0);
          // skip blocks that we already visited
          if(!visited.contains(b._start)) {
            // reduce did not work here for whatever weird reason. and this is supposed to be
            // nicer?! not convinced yet!
            if(b._predecessors.stream().map((x) -> unvisited.contains(x)).filter(x -> x).collect(Collectors.toSet()).isEmpty())
            {
              next.addAll(b._successors);
              _n = b;
              break;
            } else {
              // put it back into the list and try another one
              next.add(b);
            }
          }
        }
        
        return _n != null;
      }
      
      @Override
      public BasicBlock next() {
        unvisited.remove(_n._start);
        visited.add(_n._start);
        BasicBlock b = _n;
        _n = null;
        return b;
      }
    };
  }
  
  // TODO this needs more (stronger) testing to make sure that we handle loops properly!
  public static Set<BasicBlock> getNonFeedbackPredecessors(BasicBlock bb) {
    Set<BasicBlock> preds = new HashSet<>();
    if(bb._start == 0) {
      // it is the first basic block
    } else {
      preds.addAll(bb._predecessors);
      preds.removeAll(bb._loopBacks);
    }
    
    return preds;
  }
  
  // FIXME this needs to drain this information from the loopback list. it needs to exclude the feedback successor.
  public static Set<BasicBlock> getNonFeedbackSuccessors(BasicBlock bb) {
    if(bb._predecessors.size() == 1 && bb._predecessors.iterator().next()._start > bb._end) {
      // loop case: loop body
      return Collections.emptySet();
    } else {
      return bb._successors;
    }
  }
  
  protected static boolean isCatchBlock(BasicBlock bb, AbstractInsnNode[] insns) {
    return insns[bb._end].getOpcode() == Opcodes.ATHROW;
  }
}
