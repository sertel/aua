/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.tree.analysis.Frame;

import aua.cfg.CFGAnalyzer.BasicBlock;

/**
 * Transformation to static single assignment form.
 * 
 * @author sertel
 *
 */
public class SSA {
  
  public class Var {
    // pointer into the array of local variable nodes
    public int _ref = -1;
    public String _name = null;
    
    public int _local = -1;
    public int _count = 0;
    public int _start = -1; // points to the ASTORE instruction
    public boolean _phiGenerated = false;
    
    private Var(String name, int ref, int local, int count, int start, boolean phi) {
      _name = name;
      _ref = ref;
      _local = local;
      _count = count;
      _start = start;
      _phiGenerated = phi;
    }
    
    public boolean equals(Object other) {
      if(other instanceof Var) {
        Var otherVar = (Var) other;
        return otherVar._count == _count && otherVar._local == _local;
      } else {
        return false;
      }
    }
    
    public int hashCode() {
      return java.util.Objects.hash(_local, _count);
    }
    
    public String toString() {
      return _name;
    }
  }
  
  // TODO probably wants to become a more efficient inner data structure for looking up a
  // local's SSA form.
  private Map<Integer, List<Var>> _ssaForms = null;
  
  public SSA() {}
  
  public void transform(List<LocalVariableNode> locals, BasicBlock[] bbs, AbstractInsnNode[] function) {
    _ssaForms = new HashMap<>();
    
    // initialize with the very first version!
    for(int i = 0; i < locals.size(); i++) {
      LocalVariableNode local = locals.get(i);
      if(local != null) {
        _ssaForms.put(i, new ArrayList<>());
        _ssaForms.get(i).add(new Var(local.name, i, local.index, 0, 0, false));
      }
    }
    
    Iterator<BasicBlock> it = CFGAnalyzer.getIterator(bbs);
    while(it.hasNext()) {
      transform(_ssaForms, it.next(), function);
    }
  }
  
  private Map<Integer, List<Var>> transform(Map<Integer, List<Var>> ssaForms, BasicBlock bb,
                                            AbstractInsnNode[] function)
  {
    // create phi-generated variables if necessary
    if(bb._predecessors.size() > 1) {
      for(Map.Entry<Integer, List<Var>> localSSA : ssaForms.entrySet()) {
        Set<Var> reDefs = new HashSet<>();
        for(BasicBlock pred : bb._predecessors) {
          // this does not mean that the alteration was performed on the predecessor! (hence we
          // use a set.)
          reDefs.add(getSSA(localSSA.getKey(), localSSA.getValue(), pred._end));
        }
        
        // we performed initialization for all local variables
        assert localSSA.getValue().size() > 0;
        
        // phi function (note: this works because the first statement of a basic block can never
        // be an ASTORE instruction!)
        if(reDefs.size() > 1) {
          localSSA.getValue().add(new Var(localSSA.getValue().get(0)._name,
                                          localSSA.getValue().get(0)._ref,
                                          localSSA.getKey(),
                                          reDefs.stream().map(x -> x._count).reduce(Math::max).get() + 1,
                                          bb._start,
                                          true));
        }
      }
    }
    
    // create the rest of the ssa forms
    for(int i = bb._start; i <= bb._end; i++) {
      AbstractInsnNode inst = function[i];
      if(inst.getOpcode() == Opcodes.ASTORE) {
        VarInsnNode local = (VarInsnNode) inst;
        
        // we performed initialization for all local variables
        assert ssaForms.containsKey(local.var) && ssaForms.get(local.var).size() > 0;
        
        List<Var> localSSAForms = ssaForms.get(local.var);
        Var ssa = new Var(localSSAForms.get(0)._name, localSSAForms.get(0)._ref, local.var,
        // we start at 0 so this is essentially an increment
                          localSSAForms.size(),
                          i,
                          false);
        localSSAForms.add(ssa);
      }
    }
    return ssaForms;
  }
  
  private Var getSSA(int local, List<Var> vars, int inst) {
    Var f = null;
    for(Var v : vars) {
      if(v._start == inst) {
        f = v;
        break;
      } else {
        if(v._start < inst) {
          // this tries to find the closest SSA definition to the given instruction.
          if(f == null) f = v;
          else if(f._start < v._start) f = v;
        }
      }
    }
    return f;
  }
  
  public Var getSSAFromParams(int local, int inst, Frame[] bbs) {
    return getSSA(local, inst, (BasicBlock) bbs[inst]);
  }
  
  public Var getSSAFromCode(int local, int inst, Frame[] bbs) {
    List<Integer> paramIndex = _ssaForms.entrySet().stream().filter(a -> a.getValue().get(0)._local == local).map(a -> a.getKey()).collect(Collectors.toList());
    if(paramIndex.isEmpty()) throw new RuntimeException("No such local: " + local);
    assert paramIndex.size() < 2;
    return getSSA(paramIndex.get(0), inst, (BasicBlock) bbs[inst]);
  }
  
  private Var getSSA(int local, int inst, BasicBlock bb) {
    Var s = get(local, inst, bb);
    assert s != null;
    return s;
  }
  
  private Var get(int local, int inst, BasicBlock block) {
    List<BasicBlock> blocks = new ArrayList<>();
    blocks.add(block);
    while(!blocks.isEmpty()) {
      BasicBlock bb = blocks.remove(0);
      
      // there might be an assignment on this basic block.
      List<Var> candidates = new ArrayList<>();
      for(Var v : _ssaForms.get(local))
        if(bb._start <= v._start && v._start <= inst) candidates.add(v);
      
      if(candidates.isEmpty()) {
        Set<BasicBlock> preds = CFGAnalyzer.getNonFeedbackPredecessors(bb);
        if(preds.isEmpty()){
          // this happens for loop bodys that loop back
        }else{
          
        }
        // ... otherwise we would have found the initial assignment
        blocks.addAll(preds);
      } else {
        return getSSA(local, candidates, inst);
      }
    }
    assert false;
    return null;
  }
  
  /**
   * Finds all basic blocks that define the scope of the this SSA var.
   * 
   * @param local
   * @param inst
   * @param bbs
   * @return
   */
  public Map<BasicBlock, Integer> getDefinitionRange(int local, int inst, Frame[] bbs) {
    Var ssaVar = getSSAFromCode(local, inst, bbs);
    BasicBlock bb = (BasicBlock) bbs[ssaVar._start];
    return getDefinitionRange(local, bb, bbs, ssaVar._count);
  }

  private Map<BasicBlock, Integer> getDefinitionRange(int local, BasicBlock block, Frame[] bbs, int count) {
    Map<BasicBlock, Integer> found = new HashMap<>();
    List<BasicBlock> blocks = new ArrayList<>();
    blocks.add(block);
    while(!blocks.isEmpty()) {
      BasicBlock bb = blocks.remove(0);
      Var ssaVar = getSSAFromCode(local, bb._end, bbs);
      
      if(ssaVar._count == count) {
        found.put(bb, bb._end);
        // check the successors
        Set<BasicBlock> succs = CFGAnalyzer.getNonFeedbackSuccessors(bb);
        for(BasicBlock succ : succs) {
          Var ssaSucc = getSSAFromCode(local, succ._start, bbs);
          if(ssaSucc._count == count
          // the point that we need this means that there is somewhere a loop that the
          // getNonFeedbackSuccessor function does not detect! FIXME
             && !found.containsKey(succ))
          {
            blocks.add(succ);
          }
        }
      } else {
        // we found a boundary, so where is it?!
        int foundCount = ssaVar._count;
        if(foundCount < count) {
          // this is a loop body. for now we do not collect it.
        } else {
          Var prev = ssaVar;
          while(foundCount != count) {
            prev = ssaVar;
            ssaVar = getSSAFromCode(local, ssaVar._start - 1, bbs);
            foundCount = ssaVar._count;
          }
          assert bb._start <= prev._start;
          found.put(bb, prev._start - 1);
        }
      }
    }
    
    return found;
  }
}
