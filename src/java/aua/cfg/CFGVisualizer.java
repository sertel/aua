/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import aua.cfg.CFGAnalyzer.BasicBlock;

public class CFGVisualizer {
  
  public static String OUTPUT = null;
  public static String CONDITION = null;
  
  public static void print(AbstractInsnNode[] instructions, Frame[] frames, String className, String methodName) {
    String fileName = className + "-" + methodName;
    if(CONDITION != null && fileName.equals(CONDITION)) {
      String outFile = OUTPUT + className + "-" + methodName;
      print(instructions, frames, outFile);
    }
  }
  
  @SuppressWarnings("unchecked")
  public static void print(AbstractInsnNode[] insts, Frame[] frames, String functionDescription) {
    try (PrintStream output = new PrintStream(new FileOutputStream(functionDescription + ".dot"))) {
      output.println("digraph FLOW {");
      output.println("labelloc=top;");
      output.println("labeljust=l;");
      output.println("rankdir=TB;");
      
      // resolve it here at once to get the labels right!
      TraceMethodVisitor tcv = new TraceMethodVisitor(new Textifier());
      for(AbstractInsnNode inst : insts)
        inst.accept(tcv);
      assert tcv.p.text.size() == insts.length;
      
      BasicBlock[] blocks = CFGAnalyzer.getBasicBlocks(frames);
      for(int i = 0; i < blocks.length; i++)
        printBlock(blocks[i], i, tcv.p.text, output);
      
      for(int i = 0; i < blocks.length; i++)
        printArcs(i, findDestinations(blocks[i], blocks), output);
      
      output.println("}");
    }
    catch(FileNotFoundException e) {
      e.printStackTrace();
    }
  }
  
  private static List<Integer> findDestinations(BasicBlock bb, BasicBlock[] bbs) {
    List<Integer> dsts = new ArrayList<>();
    for(BasicBlock pred : bb._successors) {
      for(int i = 0; i < bbs.length; i++)
        if(bbs[i] == pred) dsts.add(i);
    }
    assert dsts.size() == bb._successors.size();
    return dsts;
  }
  
  private static void printBlock(BasicBlock bb, int blockID, List<Object> insts, PrintStream output) {
    Object label =
        IntStream.range(bb._start, bb._end + 1).mapToObj(i -> new Object[] { i,
                                                                            insts.get(i) }).map(a -> a[0]
                                                                                                     + " : "
                                                                                                     + a[1].toString().trim().replace("\"",
                                                                                                                                      "\\\"")).reduce((a,
                                                                                                                                                       b) -> a
                                                                                                                                                             + "\\l"
                                                                                                                                                             + b).get();
    output.println(blockID + " [shape=rectangle, labeljust=\"l\", fontsize=6, label=\"" + label + "\\l\"];");
  }
  
  private static void printArcs(int source, List<Integer> dsts, PrintStream output) {
    for(Integer dst : dsts) {
      output.println(source + " -> " + dst
      // + " [label=\"(" + arc.getSourcePort().getPortName() + ", " +
      // arc.getTargetPort().getPortName() + ",\n" + arc.getType().name() + ")\"]"
                     + ";");
    }
  }
}
