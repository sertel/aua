/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import aua.analysis.reference.ReturnArrayReferenceAnalysis.ReferenceLeakException;
import aua.analysis.reference.ReferenceCollector.Reference;

/**
 * The class holds test cases of the reference analysis that go again library classes.
 * <p>
 * More work is necessary to really handle recursive data structures like hash maps and hash
 * trees. (That's why these tests are currently not part of the regression.)
 * 
 * @author sertel
 *
 */
@Ignore
public class testReferenceAnalysisLibs {
  private boolean _debug = false;
  
  @Test
  public void testArrayListGet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.ArrayListGet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.ArrayListGet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(1, references.get(0).size());
    // the item reference and the piece retrieved from the state
    Assert.assertEquals("[[this:0]._l.elementData.<0>]", Arrays.deepToString(refs.toArray()));
  }
  
  @Test
  public void testArrayListAdd() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.ArrayListAdd.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.ArrayListAdd.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(3, references.get(0).size());
    Assert.assertEquals("[*aua/analysis/reference/ReferenceAnalysisOperators$Item:9*, *java/util/ArrayList:2*, *java/util/ArrayList:2*.elementData]",
                        Arrays.deepToString(refs.toArray()));
  }
  
  @Test
  public void testArrayListRemove() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.ArrayListRemove.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.ArrayListRemove.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    System.out.println(Arrays.deepToString(references.toArray()));
    Assert.assertEquals(1, references.size());
    Assert.assertEquals(1, references.get(0).size());
    // only returns the "item" reference because the function "ArrayList.remove" is untainted!
    Assert.assertEquals("[[[item:1]]]", Arrays.deepToString(references.toArray()));
  }
  
  @Test
  public void testArrayListSet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.ArrayListSet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.ArrayListSet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(4, references.get(0).size());
    Assert.assertEquals("[*aua/analysis/reference/ReferenceAnalysisOperators$Item:10*, *java/util/ArrayList:2*, [l:1], [l:1].elementData.<[synthetic$var1:0]>[]]",
                        Arrays.deepToString(refs.toArray()));
    
  }
  
  @Test
  public void testLinkedListGet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.LinkedListGet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.LinkedListGet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(4, references.get(0).size());
    // the item reference and the piece retrieved from the state
    Assert.assertEquals("[[item:1], [this:0]._l.first, [this:0]._l.first.item, [this:0]._l.last]",
                        Arrays.deepToString(refs.toArray()));
    // FIXME aren't those the same?! -> seems like item:1 is just an alias! -> alias support
    // missing
  }
  
  @Test
  public void testLinkedListAdd() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.LinkedListAdd.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.LinkedListAdd.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(5, references.get(0).size());
    Assert.assertEquals("[*aua/analysis/reference/ReferenceAnalysisOperators$Item:9*, *java/util/LinkedList:2*, [l:1], [l:1].last, [synthetic$var2:1]]",
                        Arrays.deepToString(refs.toArray()));
  }
  
  @Test
  public void testLinkedListRemove() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.LinkedListRemove.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.LinkedListRemove.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    System.out.println(Arrays.deepToString(references.toArray()));
    Assert.assertEquals(1, references.size());
    Assert.assertEquals(1, references.get(0).size());
    // only returns the "item" reference because the function "ArrayList.remove" is untainted!
    Assert.assertEquals("[[[item:1]]]", Arrays.deepToString(references.toArray()));
  }
  
  @Test
  public void testLinkedListSet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.LinkedListSet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.LinkedListSet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals(6, references.get(0).size());
    Assert.assertEquals("[*aua/analysis/reference/ReferenceAnalysisOperators$Item:10*, *java/util/LinkedList:2*, [l:1], [l:1].first, [l:1].first.item, [l:1].last]",
                        Arrays.deepToString(refs.toArray()));
    
  }
  
  @Test
  public void testHashMapPut() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.HashMapPut.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.HashMapPut.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    // Assert.assertEquals(4, references.get(0).size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals("*aua/analysis/reference/ReferenceAnalysisOperators$Item:10*", refs.get(0));
    Assert.assertEquals("*java/util/HashMap:2*", refs.get(1));
    Assert.assertEquals("[m:1].table", refs.get(3));
    // TODO the rest of the found references need to be dropped!
  }
  
  @Test
  public void testMultiple() throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = true;
    for(int i = 0; i < 10; i++) {
      // testHashMapPut();
      testArrayListGet();
    }
  }
  
  @Test
  public void testHashMapGet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.HashMapGet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.HashMapGet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertTrue(refs.contains("[this:0]._m.table.<*OPERATION*:17>.value"));
    // FIXME all the rest of the references should not be there.
  }
  
  @Test
  public void testHashMapRemove() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.HashMapRemove.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.HashMapRemove.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    Assert.assertEquals(1, references.get(0).size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    // 'remove' is listed in the knowledge base!
    Assert.assertEquals("[[i:1]]", Arrays.deepToString(refs.toArray()));
    
  }
  
  @Test
  public void testTreeMapPut() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.TreeMapPut.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.TreeMapPut.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    // Assert.assertEquals(4, references.get(0).size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertEquals("*aua/analysis/reference/ReferenceAnalysisOperators$Item:10*", refs.get(0));
    Assert.assertEquals("*java/util/TreeMap:2*", refs.get(1));
    Assert.assertEquals("[m:1].root", refs.get(3));
    // TODO the rest of the found references need to be dropped!
  }
  
  @Test
  public void testTreeMapGet() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.TreeMapGet.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.TreeMapGet.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    Assert.assertTrue(refs.contains("[this:0]._m.root"));
    // FIXME all the rest of the references should not be there.
  }
  
  @Test
  public void testTreeMapRemove() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.TreeMapRemove.class.getName());
    long start = System.currentTimeMillis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisLibTestClasses.TreeMapRemove.class.getName(), "go", Object[].class);
    System.out.println("done: " + (System.currentTimeMillis() - start));
    Assert.assertEquals(1, references.size());
    Assert.assertEquals(1, references.get(0).size());
    List<String> refs = references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    System.out.println(Arrays.deepToString(refs.toArray()));
    // 'remove' is listed in the knowledge base!
    Assert.assertEquals("[[i:1]]", Arrays.deepToString(refs.toArray()));
  }
  
  @Test
  public void testCombined() throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = true;
    // System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "1");
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisLibTestClasses.Combined.class.getName());
    long start = System.currentTimeMillis();
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisLibTestClasses.Combined.class.getName(), "go", Object[].class);
    }
    catch(ReferenceLeakException lde) {
      System.out.println("Leak: " + lde.getLeak());
    }
    System.out.println("done: " + (System.currentTimeMillis() - start));
    // List<String> refs =
    // references.get(0).stream().map(Reference::toString).sorted().collect(Collectors.toList());
    // System.out.println(Arrays.deepToString(refs.toArray()));
  }
  
}
