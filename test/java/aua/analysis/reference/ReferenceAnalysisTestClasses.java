/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import aua.analysis.qual.Untainted;

public class ReferenceAnalysisTestClasses {
  
  public static interface SetSomeData {
    void setSomeData(Object d);
  }
  
  public static class Item implements SetSomeData {
    public Object data;
    
    Item() {
      // nothing
    }
    
    Item(Object d) {
      data = d;
    }
    
    void setData(Object d) {
      data = d;
    }
    
    @Override
    public void setSomeData(Object d) {
      setData(d);
    }
  }
  
  public static class InheritedItem extends Item {
    // nothing
  }
  
  public static class Container {
    public Item item = new Item();
    
    void setItem(Item item) {
      this.item = item;
    }
    
    void setAnotherItem(Item item) {
      this.item = item;
    }
    
    Item getItem() {
      return this.item;
    }
  }
  
  public static class OtherContainer {
    private Item item = new Item();
    
    void setItem(Item item) {
      this.item = item;
    }
    
    void setAnotherItem(Item item) {
      this.item = item;
    }
    
    Item getItem() {
      return this.item;
    }
  }
  
  public static class BasicDisjoint {
    
    public Object go() {
      Item i1 = new Item();
      Item i2 = new Item();
      return new Object[] { i1,
                           i2 };
    }
  }
  
  public static class BasicDisjointFields {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      c2.item = i2;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class BasicLeakFields {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.item = i1;
      Container c2 = new Container();
      c2.item = i1; // leak
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class BasicReassignedFields {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.item = i1;
      Container c2 = new Container();
      i1 = new Item(); // re-assignment
      c2.item = i1;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * This is the basic example for checking the recursion step in the analysis.
   * 
   * @author sertel
   *
   */
  public static class BasicNestedReferenceLocalLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Item i1 = new Item();
      i1.data = s;
      Container c1 = new Container();
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item(); // re-assignment
      i2.data = s;
      c2.item = i2;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class BasicNestedReferenceFieldLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Item i1 = new Item();
      i1.data = s.data;
      Container c1 = new Container();
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item(); // re-assignment
      i2.data = s.data;
      c2.item = i2;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class BasicNestedReferenceFieldNoLeak {
    
    public Object go() {
      Item s = new Item();
      Item t = new Item();
      Item i1 = new Item();
      i1.data = s.data;
      Container c1 = new Container();
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item(); // re-assignment
      i2.data = t.data;
      c2.item = i2;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class MoreThanOneAssignmentsLeak {
    
    public Object go() {
      Item s = new Item();
      Item t = new Item();
      Item i1 = new Item();
      i1.data = s.data;
      Container c1 = new Container();
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = t.data;
      i1.data = t.data; // 2nd assignment to this field is a leak
      c2.item = i2;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * Check that we trace not only the origin of an assigned object reference but also its
   * construction.
   */
  public static class SourceReferenceConstructionLeak {
    
    public Object go() {
      // reference tree (with shared leave) of a source
      Item s = new Item();
      Item t1 = new Item();
      Item t2 = new Item();
      t1.data = s;
      t2.data = s;
      Item u1 = new Item();
      Item u2 = new Item();
      u1.data = t1;
      u2.data = t2;
      Item v1 = new Item();
      Item v2 = new Item();
      v1.data = u1;
      v2.data = u2;
      
      // here we must understand how i1 and i2 are being composed to detect the leak.
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = v1;
      i2.data = v2;
      
      Container c1 = new Container();
      c1.item = i1;
      
      Container c2 = new Container();
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * Here we share an object reference by placing it into two different places of the target
   * trees.
   */
  public static class DeepTargetTreeLeak {
    
    public Object go() {
      Item s = new Item(); // shared reference
      
      Container c1 = new Container();
      Container c2 = new Container();
      Item t1 = c1.item;
      Item t2 = c2.item;
      Item u1 = (Item) t1.data;
      Item u2 = (Item) t2.data;
      Item v1 = (Item) u1.data;
      Item v2 = (Item) u2.data;
      Item w1 = (Item) v1.data;
      Item w2 = (Item) v2.data;
      w1.data = s;
      w2.data = s;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class UnbalancedDeepTargetTreeLeak {
    
    public Object go() {
      Item s = new Item(); // shared reference
      
      Container c1 = new Container();
      Container c2 = new Container();
      Item t1 = c1.item;
      Item t2 = c2.item;
      Item u1 = (Item) t1.data;
      Item u2 = (Item) t2.data;
      Item v1 = (Item) u1.data;
      u2.data = s;
      Item w1 = (Item) v1.data;
      w1.data = s;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class MutualAssignmentLeak {
    
    public Object go() {
      Item s = new Item(); // shared reference
      
      Container c1 = new Container();
      Container c2 = new Container();
      Item t1 = c1.item;
      Item t2 = c2.item;
      Item u1 = (Item) t1.data;
      Item u2 = (Item) t2.data;
      Item v1 = (Item) u1.data;
      Item v2 = (Item) u2.data;
      Item w1 = (Item) v1.data;
      Item w2 = (Item) v2.data;
      w1.data = s;
      w2.data = w1.data;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class DeepTargetTreeStackLeak {
    
    public Object go() {
      Item s = new Item(); // shared reference
      
      Container c1 = new Container();
      Container c2 = new Container();
      ((Item) ((Item) ((Item) c1.item.data).data).data).data = s;
      ((Item) ((Item) ((Item) c2.item.data).data).data).data = s;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class DeepObjectArrayLeak {
    
    public Object go() {
      // reference tree (with shared leave) of a source
      Item[] s = new Item[2];
      Item t1 = new Item();
      Item t2 = new Item();
      t1.data = s;
      t2.data = s;
      Item u1 = new Item();
      Item u2 = new Item();
      u1.data = t1;
      u2.data = t2;
      Item v1 = new Item();
      Item v2 = new Item();
      v1.data = u1;
      v2.data = u2;
      
      // here we must understand how i1 and i2 are being composed to detect the leak.
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = v1;
      i2.data = v2;
      
      Container c1 = new Container();
      c1.item = i1;
      
      Container c2 = new Container();
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class DeepPrimitiveArrayLeak {
    
    public Object go() {
      // reference tree (with shared leave) of a source
      int[] s = new int[2];
      Item t1 = new Item();
      Item t2 = new Item();
      t1.data = s;
      t2.data = s;
      Item u1 = new Item();
      Item u2 = new Item();
      u1.data = t1;
      u2.data = t2;
      Item v1 = new Item();
      Item v2 = new Item();
      v1.data = u1;
      v2.data = u2;
      
      // here we must understand how i1 and i2 are being composed to detect the leak.
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = v1;
      i2.data = v2;
      
      Container c1 = new Container();
      c1.item = i1;
      
      Container c2 = new Container();
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * There is more to this example. As a matter of fact one can have an object-sensitive
   * reference analysis that takes into account the changes performed to the field. Similar to
   * an SSA form one would have to bring the object into an SSA form but across the whole
   * program instead of just inside a function!
   */
  public static class ShallowObjectLeak {
    
    public Object go() {
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = new Item();
      i2.data = i1.data;
      return new Object[] { i1,
                           i2 };
    }
  }
  
  public static class ShallowArrayLeak {
    
    public Object go() {
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = new Item[2];
      i2.data = i1.data;
      return new Object[] { i1,
                           i2 };
    }
  }
  
  public static class ShallowObjectDirectAssignmentLeak {
    
    public Object go() {
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = i2.data; // direct assignment but to a field that was never assigned anywhere in
                         // the
                         // routine.
      return new Object[] { i1,
                           i2 };
    }
  }
  
  public static class ArrayConstructionLeak {
    
    public Object go() {
      // array with shared content
      Item[] s1 = new Item[1];
      Item[] s2 = new Item[1];
      Item shared = new Item();
      s1[0] = shared;
      s2[0] = shared;
      
      Item t1 = new Item();
      Item t2 = new Item();
      t1.data = s1;
      t2.data = s2;
      
      Container c1 = new Container();
      c1.item = t1;
      Container c2 = new Container();
      c2.item = t2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ArrayTopLevelLeak {
    
    public Object go() {
      Item shared = new Item();
      
      Item[] c1 = new Item[1];
      c1[0] = shared;
      Item[] c2 = new Item[1];
      c2[0] = shared;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ArrayTopLevelNoLeak {
    
    public Object go() {
      int shared = 5;
      
      int[] c1 = new int[1];
      c1[0] = shared;
      int[] c2 = new int[1];
      c2[0] = shared;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * A string is an immutable object so we should not complain about the sharing.
   */
  public static class StringArrayTopLevelNoLeak {
    
    public Object go() {
      String shared = "shared";
      
      String[] c1 = new String[1];
      c1[0] = shared;
      String[] c2 = new String[1];
      c2[0] = shared;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ArrayRetrievalLeak {
    
    public Object go() {
      Item[] s = new Item[1];
      
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = s[0];
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = s[0];
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * Different indexes accessed.
   */
  public static class ArrayRetrievalNoLeak {
    
    public Object go() {
      Item[] s = new Item[2];
      
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = s[0];
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = s[1];
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ArrayRetrievalCalculateLeak {
    
    public Object go() {
      int index = 100 - 12;
      Item[] s = new Item[100];
      
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = s[index];
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = s[index];
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * Normally, we do not support if the index is being calculated on the stack but in this case
   * the Java compiler already performs this calculation and uses BIPUSH to load this static
   * result to the stack. So we can support it.
   */
  public static class ArrayRetrievalCalculateCompilerCalculatedLeak {
    
    public Object go() {
      Item[] s = new Item[100];
      
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = s[100 - 12];
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = s[100 - 22 + 10];
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ArrayRetrievalCalculateUnsupported {
    
    public Object go(int input) {
      Item[] s = new Item[100];
      
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = s[input + 4];
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = s[input + 4];
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * In order to detect this type of none-leak, we must also capture the target and perform a
   * reduction as a last step of the analysis.
   * <p>
   * I think we should not support this. Instead we should just force the programmer to write
   * clean code because obviously there should always just be a single assignment! The rest
   * should be dealt with via local variables.
   */
  public static class UpdateSensitiveNoLeak {
    
    public Object go() {
      Item s = new Item();
      Item i1 = new Item();
      Item i2 = new Item();
      i1.data = s;
      i2.data = s;
      i1.data = new Item(); // update to a new reference
      return new Object[] { i1,
                           i2 };
    }
  }
  
  public static class StaticLeak {
    private static Item _s = null;
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      i1.data = _s;
      c1.item = i1;
      Container c2 = new Container();
      Item i2 = new Item();
      i2.data = _s;
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class TopLevelStaticLeak {
    private static Item _s = null;
    
    public Object go() {
      return new Object[] { _s,
                           _s };
    }
  }
  
  public static class TopLevelLeak {
    
    public Object go() {
      Container s = new Container();
      return new Object[] { s,
                           s };
    }
  }
  
  public static class TopLevelArrayLeak {
    
    public Object go() {
      Container[] s = new Container[10];
      return new Object[] { s,
                           s };
    }
  }
  
  public static class TopLevelPrimitiveArrayLeak {
    
    public Object go() {
      int[] s = new int[10];
      return new Object[] { s,
                           s };
    }
  }
  
  public static class TopLevelFieldLeak {
    
    public Object go() {
      Container s = new Container();
      return new Object[] { s.item,
                           s.item };
    }
  }
  
  public static class TopLevelArrayItemLeak {
    
    public Object go() {
      Container[] s = new Container[2];
      return new Object[] { s[1],
                           s[1] };
    }
  }
  
  public static class TopLevelImmutableNoLeak {
    
    public Object go() {
      String s = "something";
      return new Object[] { s,
                           s };
    }
  }
  
  public static class ConditionalSharingLeak {
    
    public Object go() {
      // the root items are constructed on a different block than the array
      Container c1 = new Container();
      Container c2 = new Container();
      
      Item s = new Item(); // Nested shared reference
      Item i1 = new Item();
      i1.data = s.data;
      
      Item i2 = new Item();
      if(i1 instanceof Item) i2.data = s.data; // conditional sharing
      else i2.data = new Item();
      
      // connection is established on the final block
      c1.item = i1;
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class LoopSharingLeak {
    
    public Object go(int input) {
      // the root items are constructed on a different block than the array
      Container c1 = new Container();
      Container c2 = new Container();
      
      Item s = new Item(); // Nested shared reference
      Item i1 = new Item();
      i1.data = s.data;
      
      Item i2 = new Item();
      while(input < 5) {
        i2.data = s.data; // loop sharing
        input++;
      }
      
      // connection is established on the final block
      c1.item = i1;
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class BasicFunctionLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Container c1 = construct(s);
      Container c2 = construct(s);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      Item i = new Item();
      i.data = shared;
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class BasicFunctionalParameterSharingLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      
      Container c1 = new Container();
      construct(c1, s);
      Container c2 = new Container();
      construct(c2, s);
      
      return new Object[] { c1,
                           c2 };
    }
    
    private void construct(Container c, Item shared) {
      Item i = new Item();
      i.data = shared;
      c.item = i;
    }
  }
  
  public static class FieldFunctionLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Container c1 = construct(s);
      Container c2 = construct(s);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      Item i = new Item();
      i.data = shared.data;
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class FieldConstructorLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      
      Item i1 = new Item(s.data);
      Container c1 = new Container();
      c1.item = i1;
      
      Item i2 = new Item(s.data);
      Container c2 = new Container();
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class NestedFieldConstructorLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Container c1 = construct(s);
      Container c2 = construct(s);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      Item i = new Item(shared.data);
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class NestedFieldDirectCallLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      return new Object[] { construct(s),
                           construct(s) };
    }
    
    private Container construct(Item shared) {
      Item i = new Item(shared.data);
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class FieldDirectCallLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      return new Object[] { get(s),
                           get(s) };
    }
    
    private Container get(Item t) {
      return construct(t);
    }
    
    private Container construct(Item shared) {
      Item i = new Item(shared.data);
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class InheritedFunctionFieldLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Container c1 = construct(s);
      Container c2 = construct(s);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      InheritedItem i = new InheritedItem();
      i.setData(shared.data); // this function is defined only in Item
      Container c = new Container();
      c.item = i;
      return c;
    }
  }
  
  public static class InterfaceFunctionFieldLeak {
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      Container c1 = construct(s);
      Container c2 = construct(s);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      SetSomeData i = new InheritedItem();
      i.setSomeData(shared.data); // this function is an interface function
      Container c = new Container();
      c.item = (Item) i;
      return c;
    }
  }
  
  public static class KnowledgeBaseSupportLeak {
    private ArrayList<Item> _state = null;
    
    public Object go() {
      Container c1 = new Container();
      Container c2 = new Container();
      
      Item i1 = _state.get(0);
      Item i2 = _state.get(0);
      
      c1.item = i1;
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class KnowledgeBaseSupportNoLeak {
    private ArrayList<Item> _state = null;
    
    public Object go() {
      Container c1 = new Container();
      Container c2 = new Container();
      
      Item i1 = _state.remove(0);
      Item i2 = _state.remove(0);
      
      c1.item = i1;
      c2.item = i2;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class KnowledgeBaseSupportRemovalLeak {
    private ArrayList<Item> _state = null;
    
    @SuppressWarnings("unused")
    public Object go() {
      Container c1 = new Container();
      Container c2 = new Container();
      
      Item i1 = _state.remove(0);
      Item i2 = _state.remove(0);
      
      c1.item = i1;
      c2.item = i1;
      
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ImmutableTypeArray {
    
    public Object[] go(String input) {
      
      String[] keyValue = input.split(":");
      
      // specific type here
      return new String[] { keyValue[0],
                           keyValue[1] };
    }
  }

  public static class ImmutableTypeItems {
    
    public Object[] go(String input) {
      
      String[] keyValue = input.split(":");
      
      // general type here
      return new Object[] { keyValue[0],
                           keyValue[1] };
    }
  }

  public static class DataStructureLeak {
    private Item _state = null;
    
    public Object go() {
      Item s = new Item(); // Nested shared reference
      _state = s; // we store this to a field and now this is a reference copy
      Container c1 = construct(s);
      Container c2 = construct(_state);
      return new Object[] { c1,
                           c2 };
    }
    
    private Container construct(Item shared) {
      Item i = new Item();
      i.setSomeData(shared.data);
      Container c = new Container();
      c.item = (Item) i;
      return c;
    }
  }
  
  /*************** Base cases for context sensitive points-to analysis *****************/
  
  /**
   * This is tricky because it reuses the local slot here. We can only handle this correctly if
   * we understand the reassignment happening in between the assignments. We have to encode this
   * in the path information that we produce. It really has to encapsulate not only the local
   * but also the instruction index of the new statement.
   * 
   * @author sertel
   *
   */
  public static class BasicDisjointFieldsContextSensitive {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.item = i1;
      Container c2 = new Container();
      i1 = new Item();
      c2.item = i1;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class Duplicate {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      Container c2 = c1;
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class HiddenShared {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      Container c2 = new Container();
      c2.setItem(i1);
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ContextInsensitive1 {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      Container c2 = new Container();
      c2.setAnotherItem(i1);
      return new Object[] { c1,
                           c2 };
    }
  }
  
  /**
   * Now this is a real deal breaker for SPARK. It means that no two elements can be of the same
   * type. I assume this to happen very often.
   * 
   * @author sertel
   *
   */
  public static class ContextInsensitive2 {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      Container c2 = new Container();
      Item i2 = new Item();
      c2.setAnotherItem(i2);
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class ContextInsensitive3 {
    
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      OtherContainer c2 = new OtherContainer();
      Item i2 = new Item();
      c2.setItem(i2);
      return new Object[] { c1,
                           c2 };
    }
  }
  
  public static class FileLoad {
    Object[] load(String resource) {
      return null;
    }
  }
  
  /**
   * This operator is a good example for an operator that does not define the returned array
   * straight inside the function but stores it in another field and just retrieves it.
   *
   */
  public static class Cache extends FileLoad {
    private Map<String, Object[]> _cache = new HashMap<>();
    
    Object[] go(String resource) {
      if(_cache.containsKey(resource)) {
        return (@Untainted Object[]) _cache.get(resource);
      } else {
        Object[] contentAndLength = super.load(resource);
        _cache.put(resource, contentAndLength);
        return (@Untainted Object[]) contentAndLength;
      }
    }
  }
  
}
