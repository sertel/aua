/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import aua.analysis.AnalysisException;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.reference.ReferenceCollector.Reference;

public class testReferenceAnalysis {
  
  private boolean _debug = false;
  
  @Test
  public void testBasicDisjoint() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicDisjoint.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.BasicDisjoint.class.getName(), "go", Object.class);
    System.out.println(references);
    // we have two items stored inside the destructuring array
    Assert.assertEquals(2, references.size());
    // none of the items carries any state apart from the top references
    Assert.assertEquals(1, references.get(0).size());
    Assert.assertEquals(1, references.get(1).size());
  }
  
  @Test
  public void testBasicDisjointFields() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicDisjointFields.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.BasicDisjointFields.class.getName(), "go", Object.class);
    System.out.println(references);
    // we have two items stored inside the destructuring array
    Assert.assertEquals(2, references.size());
    // each of these items carries the reference from the new instruction and the containers
    Assert.assertEquals(2, references.get(0).size());
    Assert.assertEquals(2, references.get(1).size());
    System.out.println(Arrays.deepToString(references.toArray()));
  }
  
  @Test
  public void testBasicLeakFields() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicLeakFields.class.getName());
    try {
      analysis.analyze(ReferenceAnalysisTestClasses.BasicLeakFields.class.getName(), "go", Object.class);
      Assert.fail("We should have detected the shared item reference!");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test
  public void testBasicReassignedFields() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicReassignedFields.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.BasicReassignedFields.class.getName(), "go", Object.class);
    System.out.println(references);
    // we have two items stored inside the destructuring array
    Assert.assertEquals(2, references.size());
    // each of these items carries the reference from the new instruction and the containers
    Assert.assertEquals(2, references.get(0).size());
    Assert.assertEquals(2, references.get(1).size());
  }
  
  @Test
  public void testBasicNestedReferenceLocalLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicNestedReferenceLocalLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.BasicNestedReferenceLocalLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      System.out.println(lde.getLeak());
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testBasicNestedReferenceFieldLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicNestedReferenceFieldLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.BasicNestedReferenceFieldLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testBasicNestedReferenceFieldNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.BasicNestedReferenceFieldNoLeak.class.getName());
    
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.BasicNestedReferenceFieldNoLeak.class.getName(),
                         "go",
                         Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    Assert.assertEquals(2, references.size());
    // each of these items carries the reference from the new instruction, the field and the
    // containers
    Assert.assertEquals(3, references.get(0).size());
    Assert.assertEquals(3, references.get(1).size());
  }
  
  @Test
  public void testMoreThanOneAssignmentsLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.MoreThanOneAssignmentsLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.MoreThanOneAssignmentsLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level. (2nd assignment)");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:8*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testSourceReferenceConstructionLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.SourceReferenceConstructionLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.SourceReferenceConstructionLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level. (2nd assignment)");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testDeepTargetTreeLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.DeepTargetTreeLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.DeepTargetTreeLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level. (2nd assignment)");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testUnbalancedDeepTargetTreeLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.UnbalancedDeepTargetTreeLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.UnbalancedDeepTargetTreeLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level. (2nd assignment)");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testMutualAssignmentLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.MutualAssignmentLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.MutualAssignmentLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level. (2nd assignment)");
    }
    catch(LeakDetectionException lde) {
      System.out.println(lde.getLeak());
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Container:8*.item.data.data.data.data)",
                          lde.getLeak());
    }
  }
  
  @Test
  public void testDeepTargetTreeStackLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.DeepTargetTreeStackLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.DeepTargetTreeStackLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      System.out.println(lde.getLeak());
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testDeepObjectArrayLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.DeepObjectArrayLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.DeepObjectArrayLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:3*)", lde.getLeak());
    }
  }
  
  @Test
  public void testDeepPrimitiveArrayLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.DeepPrimitiveArrayLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.DeepPrimitiveArrayLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      Assert.assertEquals("(*I:3*)", lde.getLeak());
    }
  }
  
  /**
   * Currently the following sets are computed:<br>
   * [*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*,
   * *aua/analysis/reference/ReferenceAnalysisTestClasses$Item:15*]<br>
   * [*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:8*,
   * *aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*.data]<br>
   * The second references in both sets are essentially aliases. At this point, this problem can
   * be solved in many ways. Check out the comment on the ShallowObjectLeak class.
   * 
   * @throws Throwable
   */
  @Test(expected = AssertionError.class)
  // TODO
      public
      void testShallowObjectLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ShallowObjectLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ShallowObjectLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      // we find the origin but also the direct assignment
      String leaksMsg = lde.getLeak();
      String[] leaks = leaksMsg.split(",");
      Assert.assertEquals(2, leaks.length);
      Assert.assertTrue(leaks[0].startsWith("(*"));
      Assert.assertTrue(leaks[0].endsWith("*"));
      Assert.assertEquals("aua/analysis/reference/ReferenceAnalysisTestClasses$Item",
                          leaks[0].substring(2).split("\\:")[0]);
      Assert.assertEquals("*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*.data",
                          leaks[1].trim().substring(0, leaks[1].length() - 2));
    }
  }
  
  @Test
  public void testShallowArrayLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ShallowArrayLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ShallowArrayLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      // we find the origin but also the direct assignment
      String leaksMsg = lde.getLeak();
      String[] leaks = leaksMsg.split(",");
      Assert.assertEquals(1, leaks.length);
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*.data)", leaks[0].trim());
    }
  }
  
  @Test
  public void testShallowObjectDirectAssignmentLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ShallowObjectDirectAssignmentLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ShallowObjectDirectAssignmentLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:8*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testArrayConstructionLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayConstructionLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayConstructionLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:12*)", lde.getLeak());
    }
  }
  
  @Test
  public void testArrayTopLevelLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayTopLevelLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayTopLevelLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testArrayTopLevelNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayTopLevelNoLeak.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.ArrayTopLevelNoLeak.class.getName(), "go", Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    // those are primitive arrays, so we should not at all track them. all we track is the
    // references to the arrays themselves
    Assert.assertEquals(2, references.size());
    Assert.assertEquals(1, references.get(0).size());
    Assert.assertEquals(1, references.get(1).size());
  }
  
  /**
   * Out of some reason we currently do not collect the references to the array(s).
   * @throws Throwable
   */
  @Test(expected = AssertionError.class)
  // TODO
      public
      void testStringArrayTopLevelNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.StringArrayTopLevelNoLeak.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.StringArrayTopLevelNoLeak.class.getName(),
                         "go",
                         Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    // those are primitive arrays, so we should not at all track them. all we track is the
    // references to the arrays themselves
    Assert.assertEquals(2, references.size());
    Assert.assertEquals(1, references.get(0).size());
    Assert.assertEquals(1, references.get(1).size());
  }
  
  @Test
  public void testArrayRetrievalLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayRetrievalLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayRetrievalLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:3*.<0>)", lde.getLeak());
    }
  }
  
  @Test
  public void testArrayRetrievalNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayRetrievalNoLeak.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.ArrayRetrievalNoLeak.class.getName(), "go", Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    // those are primitive arrays, so we should not at all track them. all we track is the
    // references to the arrays themselves
    Assert.assertEquals(2, references.size());
    Assert.assertEquals(3, references.get(0).size());
    Assert.assertEquals(3, references.get(1).size());
  }
  
  @Test
  public void testArrayRetrievalCalculateLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the reference shared at the Item level.");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:7*.<[index:0]>)",
                          lde.getLeak());
    }
  }
  
  @Test
  public void testArrayRetrievalCalculateCompilerCalculatedLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateCompilerCalculatedLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateCompilerCalculatedLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("The compiler already computes static calculations such that we can support them!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:3*.<88>)", lde.getLeak());
    }
  }
  
  @Test
  public void testArrayRetrievalCalculateUnsupported() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateUnsupported.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ArrayRetrievalCalculateUnsupported.class.getName(),
                           "go",
                           Object.class,
                           int.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      // Does not detect the leak because we are not sensitive enough on array access
      // operations.
    }
    catch(AnalysisException ce) {
      // success
    }
  }
  
  @Test
  public void testStaticLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.StaticLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.StaticLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("The compiler already computes static calculations such that we can support them!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(aua/analysis/reference/ReferenceAnalysisTestClasses$StaticLeak._s)", lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelStaticLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelStaticLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelStaticLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared static reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(aua/analysis/reference/ReferenceAnalysisTestClasses$TopLevelStaticLeak._s)",
                          lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Container:2*)", lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelArrayLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelArrayLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelArrayLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Container:3*)", lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelPrimitiveArrayLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelPrimitiveArrayLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelPrimitiveArrayLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*I:3*)", lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelFieldLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelFieldLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelFieldLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Container:2*.item)",
                          lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelArrayItemLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelArrayItemLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.TopLevelArrayItemLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Container:3*.<1>)", lde.getLeak());
    }
  }
  
  @Test
  public void testTopLevelImmutableNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.TopLevelImmutableNoLeak.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.TopLevelImmutableNoLeak.class.getName(), "go", Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    Assert.assertEquals(2, references.size());
    Assert.assertEquals(0, references.get(0).size());
    Assert.assertEquals(0, references.get(1).size());
  }
  
  @Test
  public void testConditionalSharingLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.ConditionalSharingLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.ConditionalSharingLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:14*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testLoopSharingLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.LoopSharingLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.LoopSharingLeak.class.getName(),
                           "go",
                           Object.class,
                           int.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:14*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testBasicFunctionLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    // if(_debug)
    analysis.printInstructions(ReferenceAnalysisTestClasses.BasicFunctionLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.BasicFunctionLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1])", lde.getLeak());
    }
  }
  
  @Test
  public void testFieldFunctionLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.FieldFunctionLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.FieldFunctionLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
  @Test
  public void testFieldConstructorLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.FieldConstructorLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.FieldConstructorLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("(*aua/analysis/reference/ReferenceAnalysisTestClasses$Item:2*.data)", lde.getLeak());
    }
  }
  
  @Test
  public void testBasicFunctionalParameterSharingLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    // if(_debug)
    analysis.printInstructions(ReferenceAnalysisTestClasses.BasicFunctionalParameterSharingLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.BasicFunctionalParameterSharingLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1])", lde.getLeak());
    }
  }
  
  @Test
  public void testNestedFieldConstructorLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.NestedFieldConstructorLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.NestedFieldConstructorLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
  @Test
  public void testNestedFieldDirectCallLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.NestedFieldDirectCallLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.NestedFieldDirectCallLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
  @Test
  public void testFieldDirectCallLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.FieldDirectCallLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.FieldDirectCallLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
  @Test
  public void testInheritedFunctionFieldLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.InheritedFunctionFieldLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.InheritedFunctionFieldLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
  /**
   * Currently we skip interface functions.
   * @throws Throwable
   */
  @Test
  public void testInterfaceFunctionFieldLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.InterfaceFunctionFieldLeak.class.getName());
    // try {
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.InterfaceFunctionFieldLeak.class.getName(),
                         "go",
                         Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    // Assert.fail("We should have detected the shared reference!");
    // }
    // catch(LeakDetectionException lde) {
    // // success
    // System.out.println(lde.getLeak());
    // Assert.assertEquals("([s:1].data)", lde.getLeak());
    // }
  }
  
  @Test
  public void testKnowledgeBaseSupportLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.KnowledgeBaseSupportLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.KnowledgeBaseSupportLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([this:0]._state.elementData.<0>)", lde.getLeak());
    }
  }
  
  @Test
  public void testKnowledgeBaseSupportNoLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.KnowledgeBaseSupportNoLeak.class.getName());
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.KnowledgeBaseSupportNoLeak.class.getName(),
                         "go",
                         Object.class);
    System.out.println(Arrays.deepToString(references.toArray()));
    Assert.assertEquals(2, references.size());
    Assert.assertEquals(2, references.get(0).size()); // i1 and c1
    Assert.assertEquals(2, references.get(1).size()); // i2 and c2
  }
  
  @Test
  public void testKnowledgeBaseSupportRemovalLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.KnowledgeBaseSupportRemovalLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.KnowledgeBaseSupportRemovalLeak.class.getName(),
                           "go",
                           Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([i1:1])", lde.getLeak());
    }
  }
  
  @Test
  public void testImmutableTypeReturnArray() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.ImmutableTypeArray.class.getName(),
                         "go",
                         Object[].class,
                         String.class);
    Assert.assertTrue(references.isEmpty());
  }
  
  @Test
  public void testImmutableTypeArrayItems() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    List<List<Reference>> references =
        analysis.analyze(ReferenceAnalysisTestClasses.ImmutableTypeItems.class.getName(),
                         "go",
                         Object[].class,
                         String.class);
    for(List<Reference> refSet : references)
      Assert.assertTrue(refSet.isEmpty());
    
  }
  
  /**
   * We do not detect this yet because our algorithm does not investigate the origin of the
   * references of the 'this' object.
   * @throws Throwable
   */
  @Test(expected = AssertionError.class)
  // TODO
      public
      void testDataStructureLeak() throws Throwable {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    if(_debug) analysis.printInstructions(ReferenceAnalysisTestClasses.DataStructureLeak.class.getName());
    try {
      List<List<Reference>> references =
          analysis.analyze(ReferenceAnalysisTestClasses.DataStructureLeak.class.getName(), "go", Object.class);
      System.out.println(Arrays.deepToString(references.toArray()));
      Assert.fail("We should have detected the shared reference!");
    }
    catch(LeakDetectionException lde) {
      // success
      System.out.println(lde.getLeak());
      Assert.assertEquals("([s:1].data)", lde.getLeak());
    }
  }
  
}
