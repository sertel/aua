/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.reference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeMap;

import aua.analysis.reference.ReferenceAnalysisTestClasses.Item;

public class ReferenceAnalysisLibTestClasses {
  public static class ArrayListGet {
    ArrayList<?> _l = null;
    
    public Object[] go() {
      Object item = _l.get(0);
      return new Object[] { item };
    }
  }
  
  public static class ArrayListAdd {
    public Object[] go() {
      ArrayList<Item> l = new ArrayList<>();
      l.add(new Item());
      return new Object[] { l };
    }
  }
  
  public static class ArrayListRemove {
    ArrayList<?> _l = null;
    
    public Object[] go() {
      Object item = _l.remove(0);
      return new Object[] { item };
    }
  }
  
  public static class ArrayListSet {
    public Object[] go() {
      ArrayList<Item> l = new ArrayList<>();
      l.set(10, new Item());
      return new Object[] { l };
    }
  }
  
  public static class LinkedListGet {
    LinkedList<?> _l = null;
    
    public Object[] go() {
      Object item = _l.get(0);
      return new Object[] { item };
    }
  }
  
  public static class LinkedListAdd {
    public Object[] go() {
      LinkedList<Item> l = new LinkedList<>();
      l.add(new Item());
      return new Object[] { l };
    }
  }
  
  public static class LinkedListRemove {
    LinkedList<?> _l = null;
    
    public Object[] go() {
      Object item = _l.remove(0);
      return new Object[] { item };
    }
  }
  
  public static class LinkedListSet {
    public Object[] go() {
      LinkedList<Item> l = new LinkedList<>();
      l.set(10, new Item());
      return new Object[] { l };
    }
  }
  
  public static class HashMapPut {
    public Object[] go() {
      HashMap<String, Item> m = new HashMap<>();
      m.put("key", new Item());
      return new Object[] { m };
    }
  }
  
  public static class HashMapGet {
    HashMap<String, Item> _m = null;
    
    public Object[] go() {
      Item i = _m.get("key");
      return new Object[] { i };
    }
  }
  
  public static class HashMapRemove {
    HashMap<String, Item> _m = null;
    
    public Object[] go() {
      Item i = _m.remove("key");
      return new Object[] { i };
    }
  }
  
  public static class TreeMapPut {
    public Object[] go() {
      TreeMap<String, Item> m = new TreeMap<>();
      m.put("key", new Item());
      return new Object[] { m };
    }
  }
  
  public static class TreeMapGet {
    TreeMap<String, Item> _m = null;
    
    public Object[] go() {
      Item i = _m.get("key");
      return new Object[] { i };
    }
  }
  
  public static class TreeMapRemove {
    TreeMap<String, Item> _m = null;
    
    public Object[] go() {
      Item i = _m.remove("key");
      return new Object[] { i };
    }
  }
  
  public static class Combined {
    HashMap<String, Item> n = new HashMap<>();
    ArrayList<Item> k = new ArrayList<>();
    
    public Object[] go() {
      // HashMap
      HashMap<String, Item> m = new HashMap<>();
      m.put("key", new Item());
      HashMap<String, Item> m1 = new HashMap<>();
      m1.put("key", new Item());
      HashMap<String, Item> m2 = new HashMap<>();
      m2.put("key", new Item());
      HashMap<String, Item> m3 = new HashMap<>();
      m3.put("key", new Item());
      HashMap<String, Item> m4 = new HashMap<>();
      m4.put("key", new Item());
      HashMap<String, Item> m5 = new HashMap<>();
      m5.put("key", new Item());
      HashMap<String, Item> m6 = new HashMap<>();
      m6.put("key", new Item());
      HashMap<String, Item> m7 = new HashMap<>();
      m7.put("key", new Item());
      HashMap<String, Item> m8 = new HashMap<>();
      m8.put("key", new Item());
      HashMap<String, Item> m9 = new HashMap<>();
      m9.put("key", new Item());
      HashMap<String, Item> m10 = new HashMap<>();
      m10.put("key", new Item());
      
      Item i = n.get("some other key");
      Item i1 = n.get("some other key");
      Item i2 = n.get("some other key");
      Item i3 = n.get("some other key");
      Item i4 = n.get("some other key");
      Item i5 = n.get("some other key");
      Item i6 = n.get("some other key");
      Item i7 = n.get("some other key");
      Item i8 = n.get("some other key");
      Item i9 = n.get("some other key");
      Item i10 = n.get("some other key");
      
      // ArrayList
      ArrayList<Item> l = new ArrayList<>();
      l.add(new Item());
      ArrayList<Item> l1 = new ArrayList<>();
      l1.add(new Item());
      ArrayList<Item> l2 = new ArrayList<>();
      l2.add(new Item());
      ArrayList<Item> l3 = new ArrayList<>();
      l3.add(new Item());
      ArrayList<Item> l4 = new ArrayList<>();
      l4.add(new Item());
      ArrayList<Item> l5 = new ArrayList<>();
      l5.add(new Item());
      ArrayList<Item> l6 = new ArrayList<>();
      l6.add(new Item());
      ArrayList<Item> l7 = new ArrayList<>();
      l7.add(new Item());
      ArrayList<Item> l8 = new ArrayList<>();
      l8.add(new Item());
      ArrayList<Item> l9 = new ArrayList<>();
      l9.add(new Item());
      ArrayList<Item> l10 = new ArrayList<>();
      l10.add(new Item());

      Item j = k.get(0);
      
      return new Object[] { m,
                           m1,
                           m2,
                           m3,
                           m4,
                           m5,
                           m6,
                           m7,
                           m8,
                           m9,
                           m10,
                           i,
                           i1,
                           i2,
                           i3,
                           i4,
                           i5,
                           i6,
                           i7,
                           i8,
                           i9,
                           i10,
                           l,
                           l1,
                           l2,
                           l3,
                           l4,
                           l5,
                           l6,
                           l7,
                           l8,
                           l9,
                           l10,
                           j };
    }
  }
  
}
