/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import org.junit.Ignore;
import org.junit.Test;

import aua.analysis.AbstractConcurrencyAnalysisTestCase;

public class testReleaseAnalysis extends AbstractConcurrencyAnalysisTestCase {
  /*
   * None-leak test cases
   */
  
  @Test(timeout = 10000)
  public void testDirectStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.DirectStateRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testDirectArrayStateReleaseWithConstant() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.DirectArrayStateReleaseWithConstant.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testDirectArrayStateReleaseWithLocal() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.DirectArrayStateReleaseWithLocal.class.getSimpleName());
  }
  
  @Ignore
  // TODO needs implicit linearity inference
      @Test(timeout = 10000)
      public
      void testLinearArrayStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.LinearArrayStateRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testExplicitLinearArrayStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitLinearArrayStateRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testOutsideLinearArrayStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.OutsideLinearArrayStateRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testExplicitLinearListStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitLinearListStateRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testExplicitUntaintLocalRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitUntaintLocalRelease.class.getSimpleName());
  }

  @Ignore
  // type annotations in casts are not working properly (in the openjdk compiler - works for
  // eclipse compiler). you must create a local variable and
  // annotate its type!
  @Test(timeout = 10000)
  public void testExplicitUntaintReturnRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitUntaintReturnRelease.class.getSimpleName());
  }

  @Ignore
  // type annotations in casts are not working properly (in the openjdk compiler - works for
  // eclipse compiler). you must create a local variable and
  // annotate its type!
  @Test(timeout = 10000)
  public void testExplicitUntaintImplicitReturnRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitUntaintImplicitReturnRelease.class.getSimpleName());
  }

  @Ignore
  @Test(timeout = 10000)
  public void testExplicitUntaintFunctionRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ExplicitUntaintFunctionRelease.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testPrimitiveArrayState() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.PrimitiveArrayState.class.getSimpleName(), new int[] { 2 });
  }
  
  @Test(timeout = 10000)
  public void testParameterStateRelease() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ParameterStateRelease.class.getSimpleName());
  }
  
  /*
   * Leak test cases
   */
  
  @Test(timeout = 10000)
  public void testConditionalStateRelease() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ConditionalStateRelease.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testComplicatedConditionalStateRelease() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ComplicatedConditionalStateRelease.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testComplicatedConditionalArrayStateRelease() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ComplicatedConditionalArrayStateRelease.class.getSimpleName(), "_state");
  }
  
}
