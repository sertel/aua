/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

import aua.analysis.escape.EscapeAnalysis.AnalysisBoundary;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.qual.KnowledgeBase;

/**
 * These tests are performed directly on certain data structures to make sure our escape
 * analysis works in a broad sense.
 * 
 * @author sertel
 * 
 */
public class testAdvancedEscapeAnalysis {
  
  @Test(timeout = 10000)
  public void testHashMapRemove() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = HashMap.class.getResource(HashMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    url = HashMap.class.getResource(HashMap.class.getSimpleName() + "$TreeNode.class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(HashMap.class.getName(), "remove", new Class<?>[] { Object.class,
                                                                          Object.class });
      Assert.fail("Leak not detected!");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test(timeout = 10000)
  public void testHashMapPut() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = HashMap.class.getResource(HashMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(HashMap.class.getName(), "put", new Class<?>[] { Object.class,
                                                                       Object.class,
                                                                       Object.class });
      // this needs knowledge base support because of the access to HashMap.EMPTY_TABLE
      // analysis.analyze(HashMap.class.getName(), "put", new Class<?>[] { Object.class,
      // Object.class,
      // Object.class });
      Assert.fail("Can't detect release in Java 1.8 because the overwrite is conditionalized.");
    }
    catch(LeakDetectionException lde) {
      // Assert.fail("Leak detected, although in the HashMap.put() function our ReleaseAnalysis is strong enough to understand that this item was released!");
      // in Java 1.8 the release in the HashMap.put() function is more tricky and can not be
      // detected that easily anymore. the release is performed in a separate function.
    }
  }
  
  @Test(timeout = 10000)
  public void testLinkedListRemove() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = LinkedList.class.getResource(LinkedList.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(LinkedList.class.getName(), "remove", new Class<?>[] { Object.class });
    }
    catch(LeakDetectionException lde) {
      Assert.fail("Leak detected, although in the LinkedList.unlinkFirst() function our ReleaseAnalysis is strong enough to understand that this item was released!");
    }
  }
  
  @Test(timeout = 10000)
  public void testLinkedListRemoveRelease() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = LinkedList.class.getResource(LinkedList.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(LinkedList.class.getName(), "remove", new Class<?>[] { Object.class,
                                                                             int.class });
    }
    catch(LeakDetectionException lde) {
      Assert.fail("Leak detected, although in the LinkedList.unlink() function our ReleaseAnalysis is strong enough to understand that this item was released!");
    }
  }
  
  /**
   * This test case checks the propagation of the "field load" information across method
   * invocations.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testConcurrentDequePoll() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = ConcurrentLinkedDeque.class.getResource(ConcurrentLinkedDeque.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(ConcurrentLinkedDeque.class.getName(), "poll", new Class<?>[] { Object.class });
      Assert.fail("Our release analysis is currently not strong enough to detect this type of release.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  /**
   * This tests the code to load function from different classes in the object hierarchy. (here:
   * ReentrantLock.unlock())
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testConcurrentHashMapRemove() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = ConcurrentHashMap.class.getResource(ConcurrentHashMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    url = ConcurrentHashMap.class.getResource(ConcurrentHashMap.class.getSimpleName() + "$TreeBin.class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(ConcurrentHashMap.class.getName(), "remove", new Class<?>[] { Object.class,
                                                                                    Object.class });
      Assert.fail("Our release analysis is currently not strong enough to detect this type of release.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  /**
   * The ConcurrentSkipListMap.doRemove() function is heavily nested with two for loops and tons
   * of conditionals. This tests whether we handle frames properly.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testConcurrentSkipListMapRemove() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = ConcurrentSkipListMap.class.getResource(ConcurrentSkipListMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(ConcurrentSkipListMap.class.getName(), "remove", new Class<?>[] { Object.class,
                                                                                        Object.class });
      Assert.fail("Our release analysis is currently not strong enough to detect this type of release.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  /**
   * The code uses stack moving operations (DUP_X1 etc.) a lot. It makes sure that we change our
   * analysis information properly.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testTreeMapRemove() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url = TreeMap.class.getResource(TreeMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(TreeMap.class.getName(), "remove", new Class<?>[] { Object.class,
                                                                          Object.class });
      Assert.fail("Our release analysis is currently not strong enough to detect this type of release.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  public static class TreeMapUser {
    TreeMap<String, Object[]> _state = new TreeMap<>();
    
    public Object[] doSomething() {
      return _state.remove("something");
    }
  }
  
  @Test(timeout = 20000)
  public void testTreeMapRemoveInvocation() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis();
    
    URL url =
        TreeMapUser.class.getResource(testAdvancedEscapeAnalysis.class.getSimpleName() + "$"
                                      + TreeMapUser.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(TreeMapUser.class.getName(), "doSomething", new Class<?>[] { Object[].class });
    }
    catch(LeakDetectionException lde) {
      Assert.fail("Our release analysis is currently not strong enough to detect this type of release but our knowledge base in the escape analysis!");
    }
  }
  
  @Test(timeout = 10000)
  public void testMethodReflection() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis(new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        return true; // analyze all
      }
    });
    
    URL url = Method.class.getResource(Method.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      Set<String> results = analysis.analyze(Method.class.getName(), "invoke", new Class<?>[] { Object.class,
                                                                                               Object.class,
                                                                                               Object[].class });
      Assert.assertEquals(1, results.size());
      Assert.assertEquals("sun/reflect/MethodAccessor.invoke", results.toArray()[0]);
      System.out.println(Arrays.deepToString(results.toArray()));
    }
    catch(LeakDetectionException lde) {
      Assert.fail("This is a reflective method and therefore we can not know what it invokes!");
    }
  }
  
  /**
   * We received an InvariantBroken here.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testRegexFramework() throws Throwable {
    EscapeAnalysis analysis = new EscapeAnalysis(new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        return true; // analyze all
      }
    });
    
    URL url = Pattern.class.getResource(Pattern.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(Pattern.class.getName(), "compile", new Class<?>[] { Pattern.class,
                                                                           String.class });
    }
    catch(LeakDetectionException lde) {
      Assert.fail("The compile method only creates a Pattern object ...");
    }
    
    try {
      analysis.analyze(Pattern.class.getName(), "matcher", new Class<?>[] { Matcher.class,
                                                                           CharSequence.class });
      Assert.fail("... however, the created matcher actually has a reference to the pattern itself and therefore leaks a reference to it!");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test(timeout = 10000)
  public void testTreeMapPut() throws Throwable {
    KnowledgeBase.getInstance().clear();
    EscapeAnalysis analysis = new EscapeAnalysis(new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        return true; // analyze all
      }
    });
    
    URL url = TreeMap.class.getResource(TreeMap.class.getSimpleName() + ".class");
    analysis.printInstructions(url.openStream());
    
    url = TreeMap.class.getResource(TreeMap.class.getSimpleName() + "$Entry.class");
    analysis.printInstructions(url.openStream());
    
    try {
      analysis.analyze(TreeMap.class.getName(), "put", new Class<?>[] { Object.class,
                                                                       Object.class,
                                                                       Object.class });
    }
    catch(LeakDetectionException lde) {
      Assert.fail("This is a clean release operation in TreeMap$Entry.setValue, so we should be able to detect it.");
    }
  }
  
}
