/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Classes for leak analysis testing.
 * 
 * @author sertel
 * 
 */
public abstract class LeakTestClasses {
  protected static class MyState {
    public MyState _stateField = null;
    public int _field = 1;
  }
  
  public class DirectLeak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      return new Object[] { _state };
    }
  }
  
  public class IndirectLocalLeak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      MyState s = new MyState();
      s = _state;
      return new Object[] { s };
    }
  }
  
  public class IndirectFunctionLeak1 {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      return new Object[] { hide() };
    }
    
    private MyState hide() {
      return _state;
    }
  }
  
  public class IndirectFunctionLeak2 {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      return new Object[] { hide(_state) };
    }
    
    private MyState hide(MyState me) {
      return me;
    }
  }
  
  public class IndirectFunctionLeak3 {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      MyState s = new MyState();
      s = _state;
      return new Object[] { hide(s) };
    }
    
    private MyState hide(MyState me) {
      return me;
    }
  }
  
  public class InputSideEffectLeak {
    @SuppressWarnings("unused") private MyState _state = new MyState();
    
    
    public Object[] doSomething(MyState in) {
      _state = in;
      return new Object[] { in };
    }
  }
  
  public class InputSideEffect2Leak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething(MyState in) {
      in._stateField = _state;
      return new Object[] { in };
    }
  }
  
  public class ConditionLeak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      Object[] result = new Object[1];
      if(_state._field < 10) {
        result[0] = _state;
      } else {
        result[0] = new MyState();
      }
      return result;
    }
  }
  
  public static class TestLeakException extends RuntimeException {
    MyState leak = null;
  }
  
  public class ExceptionLeak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      Object[] result = new Object[1];
      if(_state._field < 10) {
        TestLeakException e = new TestLeakException();
        e.leak = _state;
        throw e;
      } else {
        result[0] = new MyState();
      }
      return result;
    }
  }
  
  /**
   * Uses concrete classes for the collection.
   */
  public class ArrayListLeak {
    private ArrayList<MyState> _state = new ArrayList<>();
    
    
    public Object[] doSomething() {
      return new Object[] { _state.get(0) };
    }
  }
  
  public class HashMapLeak {
    private HashMap<String, MyState> _state = new HashMap<>();
    
    
    public Object[] doSomething() {
      return new Object[] { _state.get("some-key") };
    }
  }
  
  /**
   * Trying to create a case that is impossible to support because we do not know the final
   * implementation used.<br>
   * My assumption: This is impossible.<br>
   * Reasoning:<br>
   * The data must come from outside the realm of this operator. Nevertheless, it needs to be
   * stored somewhere. Therefore, it needs a data structure or field. Two cases are possible
   * now:
   * <ol>
   * <li>The data structure was created inside the op. -> Then we can detect the concrete class.
   * <li>The data structure comes from outside the op. -> Then we nevertheless must store the
   * new DS. Hence, we have to perform a side-effect (PUTFIELD) which would then be tracked.
   * </ol>
   * Here is an interesting case:<br>
   * Assume that I store the data structure as a side-effect and then invoke a method on it.
   * Then I have no clue whether this method leaks state. I can of course inspect the return
   * value. If it is void or a primitive type then nothing needs to be performed. If not then we
   * have to find out more about this interface.<br>
   * It seems that interface analysis is very important concern along this topic!
   * <p>
   * One approach would be to just find classes that implement the interface and investigate
   * those. An interface is always a contract and defines certain semantics. Hence, by
   * investigating examples one can potentially derive these semantics very precisely. This
   * makes especially sense for collections.
   * <p>
   * The above approach is performed at compile-time and might be able to use some history data
   * on well known interfaces. However, if the interface is unknown and implementations do not
   * exist then it makes sense to move this analysis to pre-runtime. At least there we need to
   * have the implementation class since Java is statically typed. -> This is not necessarily
   * true because class loading works on-demand! So this might need to be performed inline with
   * the computation then. It is however worthwhile to understand the costs of this analysis.
   * This is easily parallelizable and could be extremely fast. We already use ASM, that way we
   * can circumvent loading all these classes via the class loader.
   */
  public class DSInterfaceLeak {
    private List<MyState> _state = null;
    
    
    public Object[] doSomething(List<MyState> l) {
      _state = l;
      return new Object[] { _state.get(0) };
    }
  }
  
  public class ConditionalStateRelease {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething(boolean release) {
      MyState s = null;
      if(release) s = _state;
      else _state = null; // explicitly release the state but only on an else branch!
      return new Object[] { s };
    }
  }
  
  public class ComplicatedConditionalStateRelease {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething(boolean release) {
      MyState s = _state;
      if(release) {
//        s = _state;
        _state = null; // explicitly release the state but only in the condition!
      }
      return new Object[] { s };
    }
  }

  public class ComplicatedConditionalArrayStateRelease {
    private MyState[] _state = new MyState[1];
    
    
    public Object[] doSomething(boolean release) {
      MyState s = _state[0];
      if(release) {
        _state[0] = null; // explicitly release the state but only in the condition!
      }
      return new Object[] { s };
    }
  }

  public class NonLinearIteratorStateRelease {
    private HashSet<MyState> _state = new HashSet<>();
    
    
    public Object[] doSomething() {
      Iterator<MyState> it = _state.iterator();
      return new Object[] { it.next() };
    }
  }
  
  // FIXME I'm not 100% sure which analysis is supposed to cover that. Clearly a normal escape
  // analysis should detect that. But in our case this only leaks state when the arguments were
  // also passed to another operator. This could also be part of the purity analysis. So there
  // are two things to debate: 1) Should we incorporate that into our escape analysis algorithm
  // or should we have that as a separate analysis? 2) Certainly we want to throw another type
  // of exception here that allows us to discover where the leak is! We should WARN on
  // compile-time and then identify at runtime whether we require additional safety support via
  // STM etc.
  // This analysis essentially would have to follow the arguments only and detect whenever there
  // was a PUTFIELD operation. It should not be aborting when discovering this. Only the
  // surrounding code must decide whether it wants to throw this exception or not.
  public class PureArgumentsLeak {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething(MyState[] s) {
      s[0] = _state;
      return new Object[0];
    }
  }
  
}
