/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import aua.analysis.escape.LeakTestClasses.MyState;
import aua.analysis.qual.Linear;
import aua.analysis.qual.Untainted;

public abstract class NoneLeakTestClasses {
  public class DirectLeak {
    private Integer _state = 0;
    
    
    public Object[] doSomething() {
      return new Object[] { _state };
    }
  }
  
  public class IndirectLocalLeak {
    private Double _state = 0.0;
    
    
    public Object[] doSomething() {
      Double s = 1.0;
      s = _state;
      return new Object[] { s };
    }
  }
  
  public class IndirectFunctionLeak1 {
    private Float _state = 1.0001F;
    
    
    public Object[] doSomething() {
      return new Object[] { hide() };
    }
    
    private Float hide() {
      return _state;
    }
  }
  
  public class IndirectFunctionLeak2 {
    private Boolean _state = false;
    
    
    public Object[] doSomething() {
      return new Object[] { hide(_state) };
    }
    
    private Boolean hide(Boolean me) {
      return me;
    }
  }
  
  public class IndirectFunctionLeak3 {
    private int _state = 5;
    
    
    public Object[] doSomething() {
      int s = 6;
      s = _state;
      return new Object[] { hide(s) };
    }
    
    private int hide(int me) {
      return me;
    }
  }
  
  public class ArrayListStore {
    private ArrayList<MyState> _state = new ArrayList<>();
    
    
    public Object[] doSomething() {
      return new Object[] { _state.add(new MyState()) };
    }
  }
  
  public class DirectStateRelease {
    private MyState _state = new MyState();
    
    
    public Object[] doSomething() {
      MyState s = _state;
      _state = null; // explicitly release the state
      return new Object[] { s };
    }
  }
  
  public class DirectArrayStateReleaseWithConstant {
    private MyState[] _state = new MyState[10];
    
    
    public Object[] doSomething() {
      MyState s = _state[2];
      _state[2] = null;
      return new Object[] { s };
    }
  }
  
  public class DirectArrayStateReleaseWithLocal {
    private MyState[] _state = new MyState[10];
    
    
    public Object[] doSomething(int index) {
      MyState s = _state[index];
      _state[index] = null;
      return new Object[] { s };
    }
  }
  
  public class LinearArrayStateRelease {
    private MyState[] _state = new MyState[10];
    int _index = 0;
    
    
    public Object[] doSomething() {
      if(_index < _state.length) return new Object[] { _state[_index++] };
      else return null;
    }
  }
  
  /**
   * Flow graph compilation must make sure that the input to this function is produced in a
   * linear fashion.
   * 
   * @author sertel
   * 
   */
  public class OutsideLinearArrayStateRelease {
    private MyState[] _state = new MyState[10];
    
    
    public Object[] doSomething(int first, @Linear int index, String someOtherValue) {
      @SuppressWarnings("unused") String aLocal = "test";
      if(index < _state.length) return new Object[] { _state[index] };
      else return null;
    }
  }
  
  /**
   * This operator produces a linear result value.
   * 
   * @author sertel
   * 
   */
  public class LinearResult {
    private int _counter = 0;
    
    
    public Object[] doSomething() {
      return new Object[] { _counter++ };
    }
  }
  
  public class ExplicitLinearArrayStateRelease {
    private MyState[] _state = new MyState[10];
    @Linear int _index = 0;
    
    
    public Object[] doSomething() {
      if(_index < _state.length) return new Object[] { _state[_index++] };
      else return null;
    }
  }
  
  public class ExplicitLinearListStateRelease {
    private List<MyState> _state = new ArrayList<>();
    @Linear int _index = 0;
    
    
    public Object[] doSomething() {
      if(_index < _state.size()) return new Object[] { _state.get(_index++) };
      else return null;
    }
  }
  
  public class LinearIteratorStateRelease {
    private HashSet<MyState> _state = new HashSet<>();
    private Iterator<MyState> _iterator = _state.iterator();
    
    
    public Object[] doSomething() {
      return new Object[] { _iterator.next() };
    }
  }
  
  /**
   * This case is really tricky because here we have to understand the next()-remove()-access
   * pattern.
   * 
   * @author sertel
   * 
   */
  public class LinearIteratorRemoveStateRelease {
    private HashSet<MyState> _state = new HashSet<>();
    
    
    public Object[] doSomething() {
      Iterator<MyState> iterator = _state.iterator();
      MyState s = iterator.next();
      iterator.remove();
      return new Object[] { s };
    }
  }
  
  /**
   * Normally the analysis forbids static field access. However the "out" field is flagged as
   * untainted in the compiler knowledge and therefore this operator does not leak any state.
   * 
   */
  public class UntaintedField {
    private Integer _state = 0;
    
    
    public Object[] doSomething() {
      System.out.println("Print something");
      return new Object[] { _state };
    }
  }
  
  public class UntaintedLocal {
    private MyState _state = null;
    
    
    public Object[] doSomething() {
      @Untainted MyState l = leaking();
      return new Object[] { l };
    }
    
    private MyState leaking() {
      return _state;
    }
  }
  
  // This is more tricky compiler-wise. It turns out that the Java compiler removes the
  // CHECKCAST instructions where this type information would normally reside. Inside the type
  // information there is an offset that points to the instruction that this Type information
  // belongs to. ASM now blindly applies this offset without knowing that instructions were
  // removed and hence the offset became incorrect!
  public class UntaintedFunction {
    private MyState _state = null;
    
    
    public Object[] doSomething() {
      return new Object[] { (@Untainted MyState) leaking() };
    }
    
    // The below does not work because then the java compiler removes the redundant cast in the
    // above routine and then fails to attach the annotation properly. I will flag a bug on
    // this.
    // private MyState leaking() {
    private Object leaking() {
      return _state;
    }
  }
  
  /**
   * Explicitly untaints part of its state.
   */
  public class ExplicitUntaintLocalRelease {
    private List<Object> _state = null;
    
    
    public Object[] doSomething(boolean input) {
      if(input) {
        @Untainted Object value = _state.get(0);
        return new Object[] { value };
      } else {
        @Untainted Object value = _state.get(0);
        return new Object[] { value };
      }
    }
  }
  
  /**
   * Explicitly untaints part of its state.
   */
  public class ExplicitUntaintReturnRelease {
    private List<Object> _state = null;
    
    
    public Object[] doSomething() {
      Object value = (@Untainted Object) _state.get(0);
      return new Object[] { value };
    }
  }
  
  public class ExplicitUntaintImplicitReturnRelease {
    private List<Object> _state = null;
    
    
    public Object[] doSomething() {
      return new Object[] { (@Untainted Object) _state.get(0) };
    }
  }
  
  /**
   * Explicitly untaints part of its state via a function.
   */
  public class ExplicitUntaintFunctionRelease {
    private List<Object> _state = null;
    
    
    public Object[] doSomething() {
      Object value = release();
      return new Object[] { value };
    }
    
    @Untainted
    private Object release() {
      return _state.get(0);
    }
  }
  
  /**
   * The test case taints the 'state' array. It is an array of primitive type but nevertheless
   * we have to perform the analysis clean up. This test case makes sure we do so.
   */
  public class PrimitiveArrayState {
    
    public Object[] doSomething(int value, int[] state) {
      state[0] = value;
      return new Object[] {};
    }
  }
  
  /**
   * Releases state from an untainted parameter.
   */
  public class ParameterStateRelease {
    
    public Object[] doSomething(List<Object> state) {
      Object value = state.get(0);
      return new Object[] { value };
    }
  }
  
  public enum TestEnum {
    SOME_CONSTANT,
    OTHER_CONSTANT
  }
  
  public class DirectEnumAccess {
    
    public Object[] doSomething() {
      return new Object[] { TestEnum.SOME_CONSTANT };
    }
  }
  
  public class IndirectEnumAccess {
    
    public Object[] doSomething() {
      return new Object[] { TestEnum.values()[0] };
    }
  }
  
  public class SwitchEnumAccess {
    
    public Object[] doSomething(TestEnum input) {
      TestEnum result = null;
      switch(input) {
        case SOME_CONSTANT:
          result = input;
          break;
        default:
          System.out.println("Can't happen!");
      }
      return new Object[] { result };
    }
  }
  
  public static class HeavySysOut {
    private int _index = 0;
    
    
    public Object[] collect(long value, long[] values) {
      values[_index++] = value;
      System.out.print("Called HeavySysOut: ");
      for(long l : values) {
        System.out.print(l + ",");
      }
      System.out.println("");
      return new Object[] {};
    }
  }

}
