/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Set;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import aua.analysis.AbstractConcurrencyAnalysisTestCase;
import aua.analysis.escape.EscapeAnalysis.AnalysisBoundary;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;

public class testEscapeAnalysis extends AbstractConcurrencyAnalysisTestCase {
  // @Ignore
  // @Test
  // public void simpleAMSTest() throws Throwable {
  // File f = new File("bin/com/ohua/tests/lang/TestOperator.class");
  // Assert.assertTrue(f.exists());
  //
  // DependencyVisitor v = new DependencyVisitor();
  // ClassReader cr = new ClassReader(new FileInputStream(f));
  // long l1 = System.currentTimeMillis();
  // cr.accept(v, 0);
  // long l2 = System.currentTimeMillis();
  //
  // // taken from DependencyTracker.main
  // Map<String, Map<String, Integer>> globals = v.getGlobals();
  // Set<String> jarPackages = globals.keySet();
  // Set<String> classPackages = v.getPackages();
  // int size = classPackages.size();
  // System.err.println("time: " + (l2 - l1) / 1000f + "  " + size);
  //
  // String[] jarNames = jarPackages.toArray(new String[jarPackages.size()]);
  // String[] classNames = classPackages.toArray(new String[classPackages.size()]);
  // Arrays.sort(jarNames);
  // Arrays.sort(classNames);
  //
  // DependencyTracker.buildDiagram(jarNames, classNames, globals);
  // }
  
  @Test(timeout = 10000)
  public void testDirectLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.DirectLeak.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testNoneDirectLeak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.DirectLeak.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testIndirectLocalLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.IndirectLocalLeak.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testNoneIndirectLocalLeak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.IndirectLocalLeak.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testIndirectFunction1Leak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.IndirectFunctionLeak1.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testNoneIndirectFunction1Leak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.IndirectFunctionLeak1.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testIndirectFunction2Leak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.IndirectFunctionLeak2.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testNoneIndirectFunction2Leak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.IndirectFunctionLeak2.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testIndirectFunction3Leak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.IndirectFunctionLeak3.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testNoneIndirectFunction3Leak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.IndirectFunctionLeak3.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testInputSideEffectLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.InputSideEffectLeak.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testInputSideEffect2Leak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.InputSideEffect2Leak.class.getSimpleName(), "_state");
  }
  
  /**
   * This makes sure that we do not flag leaks although there was only a side-effect to this
   * operator.<br>
   * (Later on though we want to detect that there was a side-effect.)
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testNoneLeak() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.ArrayListStore.class.getSimpleName());
  }
  
  /**
   * Leaks a slot of the internal array of ArrayList.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testArrayListLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ArrayListLeak.class.getSimpleName(),
                    "java.util.ArrayList.get");
  }
  
  @Test(timeout = 10000)
  public void testConditionLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ConditionLeak.class.getSimpleName(), "_state");
  }
  
  @Test(timeout = 10000)
  public void testExceptionLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.ExceptionLeak.class.getSimpleName(), "_state");
  }
  
  /**
   * This test case uses an iterator to retrieve a value from a hash set.
   * 
   * @throws Throwable
   */
  @Ignore
  // TODO support interface analysis
      @Test(timeout = 10000)
      public
      void testHashSetLeak() throws Throwable {
    File f = new File("bin/aua/analysis/escape/LeakTestClasses$HashSetLeak.class");
    Assert.assertTrue(f.exists());
    
    EscapeAnalysis analysis = new EscapeAnalysis();
    if(_printInstructions) analysis.printInstructions(f);
    try {
      analysis.analyze(f, "doSomething");
      Assert.fail();
    }
    catch(LeakDetectionException lde) {
      System.out.println("DETECTED LEAK!");
      Assert.assertEquals("_state", lde.getLeak());
    }
  }
  
  @Test(timeout = 10000)
  public void testHashMapLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.HashMapLeak.class.getSimpleName(), "java.util.HashMap.get");
  }
  
  /**
   * Since this operator uses a list, this test also checks that we are able to load other
   * classes and analyze them. Furthermore, since we are going through an interface here, this
   * test case also assures that we can handle this scenario.
   * 
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testInterfaceLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.DSInterfaceLeak.class.getSimpleName(),
                    "java.util.List.get");
  }
  
  @Test(timeout = 10000)
  public void testUntaintedField() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.UntaintedField.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testUntaintedLocal() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.UntaintedLocal.class.getSimpleName());
  }
  
  @Ignore
  // type annotations in casts are not working properly (in the openjdk compiler - works for
  // eclipse compiler). you must create a local variable and
  // annotate its type!
      @Test(timeout = 10000)
      public
      void testUntaintedFunction() throws Throwable {
    // _printInstructions = true;
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.UntaintedFunction.class.getSimpleName());
    // _printInstructions = false;
  }
  
  @Test(timeout = 10000)
  public void testDirectEnumAccess() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.DirectEnumAccess.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testIndirectEnumAccess() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.IndirectEnumAccess.class.getSimpleName());
  }
  
  @Test(timeout = 10000)
  public void testSwitchEnumAccess() throws Throwable {
    performNoneLeakTest(aua.analysis.escape.NoneLeakTestClasses.SwitchEnumAccess.class.getSimpleName());
  }
  
  @Ignore
  // TODO
      @Test(timeout = 10000)
      public
      void testPureArgumentsLeak() throws Throwable {
    performLeakTest(aua.analysis.escape.LeakTestClasses.PureArgumentsLeak.class.getSimpleName(),
                    "java.util.List.get");
  }
  
  @Test(timeout = 10000)
  public void testHeavySysoutNoneLeak() throws Throwable {
    AnalysisBoundary b = new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        return true; // analyze all
      }
    };
    
    Method m =
        aua.analysis.escape.NoneLeakTestClasses.HeavySysOut.class.getMethod("collect", long.class, long[].class);
    URL url =
        this.getClass().getResource("NoneLeakTestClasses$"
                                    + aua.analysis.escape.NoneLeakTestClasses.HeavySysOut.class.getSimpleName()
                                    + ".class");
    File f = new File(url.getFile());
    Assert.assertTrue(f.exists());
    
    EscapeAnalysis analysis = new EscapeAnalysis(b);
    if(_printInstructions) analysis.printInstructions(f);
    Set<String> skipped = analysis.analyze(m, new int[] { 2 });
    Assert.assertTrue(skipped.isEmpty());
  }
}
