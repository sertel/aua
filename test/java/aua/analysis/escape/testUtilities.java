/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.escape;

import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

import aua.analysis.Interprocedural;

/**
 * Test simple utility functions here.
 * 
 * @author sertel
 *
 */
public class testUtilities {
  
  public interface SomeInterface<D, F> {
    public void someInterfaceFunction(D d, F f);
  }
  
  public static abstract class SomeAbstractClass<E, G> implements SomeInterface<E,G>{
    // nothing
  }
  
  public static class TestClass<A, B, C> extends SomeAbstractClass<A, B> {
    public void someFunction(A a, B b) {
      // nothing
    }
    
    public C someOtherFunction() {
      return null;
    }

    @Override
    public void someInterfaceFunction(A d, B f) {
      // TODO Auto-generated method stub
    }
  }
  
  @Test
  public void testMethodDiscoveryOnGenerics() throws Throwable {
    MethodNode m =
        Interprocedural.loadMethod(TestClass.class.getName(),
                                   "someFunction",
                                   void.class,
                                   Object.class,
                                   Object.class);
    Assert.assertTrue(m != null);
    MethodInsnNode min =
        new MethodInsnNode(3,
                           Type.getInternalName(TestClass.class),
                           "someFunction",
                           Type.getMethodDescriptor(TestClass.class.getDeclaredMethod("someFunction",
                                                                                      Object.class,
                                                                                      Object.class)), false);
    Method ref = Interprocedural.classLoadMethod(min);
    Assert.assertTrue(ref != null);
    
    m = Interprocedural.loadMethod(TestClass.class.getName(), "someOtherFunction", Object.class);
    Assert.assertTrue(m != null);
    
    min =
        new MethodInsnNode(3,
                           Type.getInternalName(TestClass.class),
                           "someOtherFunction",
                           Type.getMethodDescriptor(TestClass.class.getDeclaredMethod("someOtherFunction")),
                           false);
    ref = Interprocedural.classLoadMethod(min);
    Assert.assertTrue(ref != null);
  }
  
  @Test
  public void testMethodDiscoveryOnGenericInterface() throws Throwable {
    MethodInsnNode min =
        new MethodInsnNode(3,
                           Type.getInternalName(SomeAbstractClass.class),
                           "someInterfaceFunction",
                           Type.getMethodDescriptor(SomeInterface.class.getDeclaredMethod("someInterfaceFunction",
                                                                                          Object.class,
                                                                                          Object.class)), false);
    Method ref = Interprocedural.classLoadMethod(min);
    Assert.assertTrue(ref != null);
  }
}
