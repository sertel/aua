/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis;

import java.io.File;
import java.net.URL;

import org.junit.Assert;

import aua.analysis.escape.EscapeAnalysis;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;

public abstract class AbstractConcurrencyAnalysisTestCase {
  protected boolean _printInstructions = false; // for debugging purposes
  
  protected final void performNoneLeakTest(String clsName) throws Throwable {
    performNoneLeakTest("NoneLeakTestClasses$" + clsName, "doSomething", null);
  }

  protected final void performNoneLeakTest(String clsName, int[] stateArgs) throws Throwable {
    performNoneLeakTest("NoneLeakTestClasses$" + clsName, "doSomething", stateArgs);
  }

  protected final void performNoneLeakTest(String clsName, String methodName, int[] stateArgs) throws Throwable {
    URL url = this.getClass().getResource(clsName + ".class");
    File f = new File(url.getFile());
    Assert.assertTrue(f.exists());
    
    EscapeAnalysis analysis = new EscapeAnalysis();
    if(_printInstructions) analysis.printInstructions(f);
    if(stateArgs == null) analysis.analyze(f, methodName);
    else analysis.analyze(f, methodName, stateArgs);
  }
  
  protected final void performLeakTest(String clsName, String leaked) throws Throwable {
    URL url = this.getClass().getResource("LeakTestClasses$" + clsName + ".class");
    File f = new File(url.getFile());
    Assert.assertTrue(f.exists());
    
    EscapeAnalysis analysis = new EscapeAnalysis();
    if(_printInstructions) analysis.printInstructions(f);
    try {
      analysis.analyze(f, "doSomething");
      Assert.fail();
    }
    catch(LeakDetectionException lde) {
      Assert.assertEquals(leaked, lde.getLeak());
    }
  }
  
}
