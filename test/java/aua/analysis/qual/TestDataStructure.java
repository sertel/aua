/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;


public class TestDataStructure
{
    
  public void add(Object o) {
    // nothing
  }
  
  public @Tainted Object get() {
    // nothing
    return null;
  }
  
  public @Untainted Object remove(Object o){
    // nothing
    return null;
  }
}
