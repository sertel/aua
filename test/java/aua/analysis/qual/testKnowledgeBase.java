/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;


public class testKnowledgeBase {
  
  /**
   * The class loader just always adds the URLs into the set of discovered resources.
   * @author sertel
   *
   */
  public static class TestURLClassLoader extends URLClassLoader {
    private URL[] _urls = null;
    
    public TestURLClassLoader(URL[] urls, ClassLoader parent) {
      super(urls, parent);
      _urls = urls;
    }
    
    public Enumeration<URL> findResources(final String name) throws IOException {
      final Enumeration<URL> urls = super.findResources(name);
      
      final List<URL> matchedURLs = new ArrayList<>();
      for(URL url : _urls)
        if(url.toString().contains(name)) matchedURLs.add(url);
      
      return new Enumeration<URL>() {
        @Override
        public boolean hasMoreElements() {
          return urls.hasMoreElements() || !matchedURLs.isEmpty();
        }
        
        @Override
        public URL nextElement() {
          return urls.hasMoreElements() ? urls.nextElement() : matchedURLs.remove(0);
        }
      };
    }
    
    public static void addToClasspath(URL... references) throws MalformedURLException {
      ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
      
      // Add the conf dir to the classpath
      // Chain the current thread classloader
      URLClassLoader urlClassLoader =
          new TestURLClassLoader(references,
                                 currentThreadClassLoader instanceof TestURLClassLoader ? currentThreadClassLoader.getParent() : currentThreadClassLoader);
      
      // Replace the thread classloader - assumes
      // you have permissions to do so
      Thread.currentThread().setContextClassLoader(urlClassLoader);
    }
    
  }
  
  @Test
  public void testClasspathExtension() throws Throwable {
    // put the test META-INF folder onto the class path
    TestURLClassLoader.addToClasspath(Paths.get("test/resources/testKnowledgeBase/testClasspathExtension/META-INF").toAbsolutePath().normalize().toUri().toURL());
    
    // load the knowledge base
    KnowledgeBase kb = KnowledgeBase.getInstance();
    
    // make sure the new entry is in there
    String[] newEntry = kb.getAnnotations("aua.analysis.qual.testKnowledgeBase", "testClasspathExtension");
    Assert.assertEquals(1, newEntry.length);
  }
  
  @Test
  public void testLoadingMultipleFiles() throws Throwable {
    // put the test META-INF folder onto the class path
    TestURLClassLoader.addToClasspath(Paths.get("test/resources/testKnowledgeBase/testLoadingMultipleFiles/META-INF").toAbsolutePath().normalize().toUri().toURL(),
                                      Paths.get("test/resources/testKnowledgeBase/testClasspathExtension/META-INF").toAbsolutePath().normalize().toUri().toURL());
    
    // load the knowledge base
    KnowledgeBase kb = KnowledgeBase.getInstance();
    
    // make sure the new entry is in there
    String[] newEntry = kb.getAnnotations("aua.analysis.qual.testKnowledgeBase", "testClasspathExtension");
    Assert.assertEquals(1, newEntry.length);
    newEntry = kb.getAnnotations("aua.analysis.qual.testKnowledgeBase", "testLoadingMultipleFiles");
    Assert.assertEquals(1, newEntry.length);
  }
}
