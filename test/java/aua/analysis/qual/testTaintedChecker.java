/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.analysis.qual;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.tree.MethodNode;

import aua.analysis.AbstractOperatorAnalysis;

public class testTaintedChecker {
  private class Analysis extends AbstractOperatorAnalysis {
    public MethodNode load(InputStream f, String methodName) throws IOException {
      super.loadClassFile(f);
      return super.loadMethod(methodName, null);
    }
  }
  
  @Test
  public void testCustomDataStructure() throws Throwable {
    Analysis analysis = new Analysis();
    
    URL url = this.getClass().getResource(TestDataStructure.class.getSimpleName() + ".class");
    File f = new File(url.getFile());
    Assert.assertTrue(f.exists());
    
    MethodNode method = analysis.load(url.openStream(), "get");
    TaintedChecker checker = new TaintedChecker();
    Annotation result = checker.getTaintedAnnotation(method);
    Assert.assertEquals(Tainted.class, result.annotationType());
    
    method = analysis.load(url.openStream(), "remove");
    result = checker.getTaintedAnnotation(method);
    Assert.assertEquals(Untainted.class, result.annotationType());
  }
  
  @Test
  public void testCollectionsDataStructure() throws Throwable {
    Method method = ArrayList.class.getDeclaredMethod("get", Integer.TYPE);
    Assert.assertNotNull(method);
    
    TaintedChecker checker = new TaintedChecker();
    Annotation result = checker.getTaintedAnnotation(method);
    Assert.assertEquals(Tainted.class, result.annotationType());
    
    method = ArrayList.class.getDeclaredMethod("remove", Integer.TYPE);
    Assert.assertNotNull(method);
    
    result = checker.getTaintedAnnotation(method);
    Assert.assertEquals(Untainted.class, result.annotationType());
  }
}
