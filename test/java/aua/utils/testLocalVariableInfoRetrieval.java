/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;

import aua.analysis.Interprocedural;
import aua.analysis.reference.ReferenceAnalysisTestClasses.BasicDisjoint;

@SuppressWarnings("unchecked")
public class testLocalVariableInfoRetrieval {
  
  public static class SomeItem {
    int _f;
  }
  
  /**
   * This makes things extremely complicated! The compiler tries to reuse slots from local
   * variable in this case. So i and j will end up using the very some local variable slot!
   */
  public static class ConditionalLocal {
    int s = 10;
    
    public Object go() {
      if(s > 100) {
        SomeItem i = new SomeItem();
        i._f = 10;
        return i;
      } else {
        SomeItem j = new SomeItem();
        j._f = 100;
        return j;
      }
    }
  }
  
  // public static class ConditionalLocal {
  // int s = 10;
  //
  // public Object go() {
  // if(s > 100) {
  // int i = 10;
  // i++;
  // return i;
  // } else {
  // int j = 11;
  // j--;
  // return j;
  // }
  // }
  // }
  
  public static class ConditionalLocalResult {
    int s = 10;
    
    public Object go() {
      int result = 0;
      if(s > 100) {
        int i = 10;
        i++;
        result = i;
      } else {
        int j = 10;
        j--;
        result = j;
      }
      return result;
    }
  }
  
  private void localInfoEqual(LocalVariableNode expected, LocalVariableNode actual) {
    System.out.println("Checking '" + expected.name + "' against '" + actual.name + "'");
    Assert.assertEquals(expected.index, actual.index);
    Assert.assertEquals(expected.start, actual.start);
    Assert.assertEquals(expected.end, actual.end);
    // the type information is not easy to derive from the byte code. part of it is lost when
    // not compiled with debug information. not even the computed frames have the concrete type
    // information.
    // Assert.assertEquals(expected.desc, actual.desc);
  }
  
  @Test
  public void testNonBranch() throws Throwable {
    Interprocedural.printInstructions(BasicDisjoint.class.getName());
    MethodNode mn = Interprocedural.loadMethod(BasicDisjoint.class.getName(), "go", Object.class);
    // Frame[] frames =
    // AnalysisTestAccess.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()),
    // BasicDisjoint.class.getName(),
    // mn);
    List<LocalVariableNode> locals =
        ASMUtils.computeLocalVariableInfo(Type.getDescriptor(BasicDisjoint.class), mn);
    
    Assert.assertEquals(mn.localVariables.size(), locals.size());
    for(int i = 0; i < locals.size(); i++)
      localInfoEqual((LocalVariableNode) mn.localVariables.get(i), locals.get(i));
  }
  
  @Test
  public void testBranch1() throws Throwable {
    Interprocedural.printInstructions(ConditionalLocal.class.getName());
    MethodNode mn = Interprocedural.loadMethod(ConditionalLocal.class.getName(), "go", Object.class);
    List<LocalVariableNode> locals =
        ASMUtils.computeLocalVariableInfo(Type.getDescriptor(ConditionalLocal.class), mn);
    
    /*
     * We can only compare the "this" local here because we can not detect anymore that the
     * first local variable slot is essentially used for two locals defined on different
     * branches.
     */
    Assert.assertEquals(mn.localVariables.size(), locals.size() + 1);
    // for(int i = 0; i < locals.size(); i++)
    List<LocalVariableNode> sorted = ((List<LocalVariableNode>) mn.localVariables).stream().sorted((a, b) -> Integer.compare(a.index, b.index)).collect(Collectors.toList());
    localInfoEqual(sorted.get(0), locals.get(0));
  }
  
  @Test
  public void testBranch2() throws Throwable {
    Interprocedural.printInstructions(ConditionalLocalResult.class.getName());
    MethodNode mn = Interprocedural.loadMethod(ConditionalLocalResult.class.getName(), "go", Object.class);
    List<LocalVariableNode> locals =
        ASMUtils.computeLocalVariableInfo(Type.getDescriptor(ConditionalLocalResult.class), mn);
    /*
     * We can only compare the "this" local here because we can not detect anymore that the
     * first local variable slot is essentially used for two locals defined on different
     * branches.
     */
    Assert.assertEquals(mn.localVariables.size(), locals.size() + 1);
    // for(int i = 0; i < locals.size(); i++)
    List<LocalVariableNode> sorted = ((List<LocalVariableNode>) mn.localVariables).stream().sorted((a, b) -> Integer.compare(a.index, b.index)).collect(Collectors.toList());
    localInfoEqual(sorted.get(0), locals.get(0));
  }
}
