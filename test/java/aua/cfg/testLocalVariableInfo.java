/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.Frame;

import aua.analysis.Interprocedural;
import aua.cfg.testComplexCFG.Sending;
import aua.utils.ASMUtils;

public class testLocalVariableInfo {
  public static class ProductDetails {
    int _id = -1;
    double _buyPrice = 0.0;
    double _sellPrice = 0.0;
    
    public ProductDetails(int id, double buyPrice, double sellPrice) {
      _id = id;
      _buyPrice = buyPrice;
      _sellPrice = sellPrice;
    }
  }
  
  /**
   * This is totally tricky but it proves that local variables do not necessarily need to be
   * assigned consecutive indexes.
   * 
   * @throws Throwable
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testConstructor() throws Throwable {
    ClassNode cn = Interprocedural.charge(ProductDetails.class.getName());
    MethodNode mn =
        Interprocedural.loadMethod(ProductDetails.class.getName(),
                                   "<init>",
                                   void.class,
                                   int.class,
                                   double.class,
                                   double.class);
    
    List<LocalVariableNode> localVariables = ASMUtils.computeLocalVariableInfo(cn.name, mn, mn.localVariables);
    
    Assert.assertNotNull(localVariables.get(3));
    Assert.assertEquals("sellPrice", localVariables.get(3).name);
  }
  
  public static class Sending {
    
    public Object[] send(List<String> msg, Socket conn) {
      // Assertion.invariant(!conn.isClosed());
      
      try {
        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(conn.getOutputStream()));
        
        for(String msgLine : msg) {
          printWriter.println(msgLine);
        }
        // end the request with a blank line
        printWriter.println("");
        printWriter.flush();
        
      }
      catch(IOException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
      
      return new Object[] { conn };
    }
  }
  
  /**
   * Here the exception essentially occupies a slot that essentially is occupied already by
   * another variable. <br>
   * Here, the problem was not the overload but merely the fact that the call to convert the
   * index needs to be able to fail. Only then we can create local variable nodes for variables
   * that were created inside the function.
   * @throws Throwable
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testOverloadedCodeIndex() throws Throwable {
    ClassNode cn = Interprocedural.charge(Sending.class.getName());
    MethodNode mn =
        Interprocedural.loadMethod(Sending.class.getName(), "send", Object[].class, List.class, Socket.class);
    
    Frame[] fs = Interprocedural.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()), cn.name, mn);
    
    CFGVisualizer.CONDITION = Sending.class.getName() + "-send";
    CFGVisualizer.OUTPUT = "test-output/";
    CFGVisualizer.print(mn.instructions.toArray(), fs, Sending.class.getName(), mn.name);

    // mn.localVariables[3].index == 3 == mn.localVariables[5].index
    List<LocalVariableNode> localVariables = ASMUtils.computeLocalVariableInfo(cn.name, mn, mn.localVariables);
    
    Assert.assertEquals(9, localVariables.size());
    Assert.assertEquals(2, localVariables.stream().filter(a -> a.index == 3).count());

    // FIXME this method must take the instruction into account and check the definition range accordingly!
    ASMUtils.translateCodeIndexToParameterIndex(3, localVariables);
  }
  
}
