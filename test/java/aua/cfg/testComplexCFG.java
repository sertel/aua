/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.Frame;

import aua.analysis.Interprocedural;
import aua.cfg.CFGAnalyzer.BasicBlock;

public class testComplexCFG {
  
  public static class Assertion {
    public static void invariant(boolean assertion) {
      // nothing
    }
  }
  
  public static class Sending {
    public static boolean DEBUG = false;
    
    public Object[] send(List<String> msg, Socket conn) {
      Assertion.invariant(!conn.isClosed());
      
      try {
        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(conn.getOutputStream()));
        
        for(String msgLine : msg) {
          printWriter.println(msgLine);
        }
        // end the request with a blank line
        printWriter.println("blank");
        printWriter.flush();
      }
      catch(IOException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
      
      return new Object[] { conn };
    }
  }
  
  public static class Simple {
    public int simple() {
      try {
        System.out.println("Something");
        return 10;
      }
      catch(RuntimeException e) {
        e.printStackTrace();
        throw e;
      }
    }
  }
  
  @Test
  public void testTryCatch() throws Throwable {
    ClassNode cn = Interprocedural.charge(Sending.class.getName());
    MethodNode mn =
        Interprocedural.loadMethod(Sending.class.getName(), "send", Object[].class, List.class, Socket.class);
    
    Frame[] fs = Interprocedural.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()), cn.name, mn);
    
    CFGVisualizer.CONDITION = Sending.class.getName() + "-send";
    CFGVisualizer.OUTPUT = "test-output/";
    CFGVisualizer.print(mn.instructions.toArray(), fs, Sending.class.getName(), mn.name);
    
    BasicBlock[] blocks = CFGAnalyzer.getBasicBlocks(fs);
    Assert.assertEquals(11, blocks.length);
    List<BasicBlock> returnAndThrow =
        Arrays.stream(blocks).filter(a -> a._successors.isEmpty()).collect(Collectors.toList());
    Assert.assertEquals(2, returnAndThrow.size());
    returnAndThrow =
        returnAndThrow.stream().sorted((a, b) -> Integer.compare(a._predecessors.size(), b._predecessors.size())).collect(Collectors.toList());
    Assert.assertEquals(Opcodes.ARETURN, mn.instructions.toArray()[returnAndThrow.get(0)._end].getOpcode());
    Assert.assertEquals(1, returnAndThrow.get(0)._predecessors.size());
    Assert.assertEquals(Opcodes.ATHROW, mn.instructions.toArray()[returnAndThrow.get(1)._end].getOpcode());
    Assert.assertEquals(4, returnAndThrow.get(1)._predecessors.size());
    
    BasicBlock jumpBlock = returnAndThrow.get(0)._predecessors.iterator().next();
    Assert.assertEquals(Opcodes.GOTO, mn.instructions.toArray()[jumpBlock._end].getOpcode());
    Assert.assertEquals(1, jumpBlock._predecessors.size());
    Assert.assertEquals(1, jumpBlock._successors.size());

    BasicBlock jumpSource = jumpBlock._predecessors.iterator().next();
    Assert.assertEquals(2, jumpSource._successors.size());
    Assert.assertTrue(jumpSource._successors.contains(returnAndThrow.get(1)));
    
    // contains a redundant FNEW instruction at the last entry
    // System.out.println(mn.instructions.getLast().getOpcode());
    Assert.assertEquals(mn.instructions.size() - 1,
                        Arrays.stream(blocks).mapToInt(a -> a._end - a._start + 1).sum());
  }
  
  @Test
  public void testSimple() throws Throwable {
    ClassNode cn = Interprocedural.charge(Simple.class.getName());
    MethodNode mn = Interprocedural.loadMethod(Simple.class.getName(), "simple", int.class);
    
    Frame[] fs = Interprocedural.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()), cn.name, mn);
    
    CFGVisualizer.CONDITION = Simple.class.getName() + "-simple";
    CFGVisualizer.OUTPUT = "test-output/";
    CFGVisualizer.print(mn.instructions.toArray(), fs, Simple.class.getName(), mn.name);
    BasicBlock[] blocks = CFGAnalyzer.getBasicBlocks(fs);
    Assert.assertEquals(3, blocks.length);
    List<BasicBlock> returnAndThrow =
        Arrays.stream(blocks).filter(a -> a._successors.isEmpty()).sorted((a, b) -> Integer.compare(a._end
                                                                                                        - a._start,
                                                                                                    b._end
                                                                                                        - b._start)).collect(Collectors.toList());
    Assert.assertEquals(Opcodes.IRETURN, mn.instructions.toArray()[returnAndThrow.get(0)._end].getOpcode());
    Assert.assertEquals(1, returnAndThrow.get(0)._predecessors.size());
    Assert.assertEquals(Opcodes.ATHROW, mn.instructions.toArray()[returnAndThrow.get(1)._end].getOpcode());
    Assert.assertEquals(1, returnAndThrow.get(1)._predecessors.size());
    Assert.assertFalse(returnAndThrow.get(1).equals(returnAndThrow.get(1)._predecessors.iterator().next()));
    List<BasicBlock> tryBlock =
        Arrays.stream(blocks).filter(a -> !a._successors.isEmpty()).collect(Collectors.toList());
    Assert.assertEquals(1, tryBlock.size());
    // compiles differently for different Java compilers
//    Assert.assertEquals(Opcodes.INVOKEVIRTUAL, mn.instructions.toArray()[tryBlock.get(0)._end].getOpcode());
    // make sure we associate each instruction only with a single bb
    // System.out.println(mn.instructions.getLast().getPrevious().getOpcode());
    // contains a redundant FNEW instruction at the last entry
    Assert.assertEquals(mn.instructions.size() - 1,
                        (tryBlock.get(0)._end - tryBlock.get(0)._start + 1)
                            + (returnAndThrow.get(0)._end - returnAndThrow.get(0)._start + 1)
                            + (returnAndThrow.get(1)._end - returnAndThrow.get(1)._start + 1));
  }
  
  /**
   * A monitor is a bit different as here the catch block inserted actually creates arcs that
   * point to itself again.<br>
   * The CFG is actually a bit different here: the MONITORENTER is like a try-block.
   * 
   * @throws Throwable
   */
  @Test
  public void testMonitor() throws Throwable {
    ClassNode cn = Interprocedural.charge(Socket.class.getName());
    MethodNode mn = Interprocedural.loadMethod(Socket.class.getName(), "isClosed", boolean.class);
    
    Frame[] fs = Interprocedural.computeStackMapFrames(new CFGAnalyzer(new BasicInterpreter()), cn.name, mn);
    
    CFGVisualizer.CONDITION = Socket.class.getName() + "-isClosed";
    CFGVisualizer.OUTPUT = "test-output/";
    CFGVisualizer.print(mn.instructions.toArray(), fs, Socket.class.getName(), mn.name);
    BasicBlock[] blocks = CFGAnalyzer.getBasicBlocks(fs);
    Assert.assertEquals(5, blocks.length);
    
    List<BasicBlock> initialBlock =
        Arrays.stream(blocks).filter(a -> a._predecessors.isEmpty()).collect(Collectors.toList());
    Assert.assertEquals(1, initialBlock.size());
    Assert.assertEquals(Opcodes.MONITORENTER, mn.instructions.toArray()[initialBlock.get(0)._end].getOpcode());
    
    List<BasicBlock> throwBlock =
        Arrays.stream(blocks).filter(a -> Opcodes.ATHROW == mn.instructions.toArray()[a._end].getOpcode()).collect(Collectors.toList());
    Assert.assertEquals(1, throwBlock.size());
    Assert.assertEquals(1, throwBlock.get(0)._predecessors.size());
    BasicBlock throwMonitorBlock = throwBlock.get(0)._predecessors.iterator().next();
    Assert.assertEquals(2, throwMonitorBlock._predecessors.size());
    Assert.assertEquals(2, throwMonitorBlock._successors.size());
    Assert.assertEquals(Opcodes.MONITOREXIT, mn.instructions.toArray()[throwMonitorBlock._end].getOpcode());
    // points to itself. lock will always be released.
    Assert.assertTrue(throwMonitorBlock._predecessors.contains(throwMonitorBlock));
    Assert.assertTrue(throwMonitorBlock._successors.contains(throwMonitorBlock));
    
    List<BasicBlock> returnBlock =
        Arrays.stream(blocks).filter(a -> Opcodes.IRETURN == mn.instructions.toArray()[a._end].getOpcode()).collect(Collectors.toList());
    Assert.assertEquals(1, returnBlock.size());
    Assert.assertEquals(1, returnBlock.get(0)._predecessors.size());
    BasicBlock monitorBlock = returnBlock.get(0)._predecessors.iterator().next();
    Assert.assertEquals(1, monitorBlock._predecessors.size());
    Assert.assertEquals(2, monitorBlock._successors.size());
    Assert.assertEquals(Opcodes.MONITOREXIT, mn.instructions.toArray()[monitorBlock._end].getOpcode());
    
    Assert.assertEquals(mn.instructions.size(), Arrays.stream(blocks).mapToInt(a -> a._end - a._start + 1).sum());
  }
}
