/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;

import aua.analysis.AbstractOperatorAnalysis;
import aua.cfg.CFGAnalyzer.BasicBlock;

public class testControlFlowGraphConstruction {
  
  private
      BasicBlock[]
      analyzer(Class<?> clz, String methodName, Class<?> returnType, Class<?>... args)
                                                                                      throws FileNotFoundException,
                                                                                      IOException
  {
    Frame[] frames = new AbstractOperatorAnalysis() {
      protected Analyzer getAnalyzer(Interpreter interpreter) {
        return new CFGAnalyzer(interpreter);
      }
      
      // a little trick from functional programming here to increase the visibility of charge()
      // and being able to call it without having a concrete type!
      public Frame[] charge() throws IOException {
        super.charge(clz.getName());
        printInstructions(clz.getName());
        MethodNode algMethod = super.loadMethod(methodName, super.constructMethodDescriptor(returnType, args));
        Frame[] f = super.computeStackMapFrames(algMethod);
        return f;
      }
      
    }.charge();
    return CFGAnalyzer.getBasicBlocks(frames);
  }
  
  @Test
  public void testSimpleBasicBlock() throws Throwable {
    BasicBlock[] basicBlocks = analyzer(CFGTestClasses.SimpleBasicBlock.class, "go", Object.class, int.class);
    Assert.assertEquals(1, basicBlocks.length);
    // make sure the appropriate arcs are set
    Assert.assertEquals(0,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._successors.size()).sum());
    Assert.assertEquals(0,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._predecessors.size()).sum());
    // make sure the boundaries are defined properly
    long validatedStart =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._start).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(1, validatedStart);
    long validatedEnd =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._end).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(1, validatedEnd);
  }
  
  @Test
  public void testSimpleBranching() throws Throwable {
    BasicBlock[] basicBlocks = analyzer(CFGTestClasses.SimpleBranching.class, "go", Object.class, int.class);
    Assert.assertEquals(4, basicBlocks.length);
    // make sure the appropriate arcs are set
    Assert.assertEquals(4,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._successors.size()).sum());
    Assert.assertEquals(4,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._predecessors.size()).sum());
    // make sure the boundaries are defined properly
    long validatedStart =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._start).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(4, validatedStart);
    long validatedEnd =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._end).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(4, validatedEnd);
  }
  
  @Test
  public void testSimpleLoop() throws Throwable {
    BasicBlock[] basicBlocks = analyzer(CFGTestClasses.SimpleLoop.class, "go", Object.class, int.class);
    /**
     * The resolution here is very interesting! One would assume that the compiler maps this
     * down to a byte code that inhibits 3 basic blocks: before the loop, inner loop, after the
     * loop.<br>
     * What actually happens is that the compiler produces the following byte code structure:<br>
     * L1 before the loop<br>
     * GOTO L3<br>
     * L2 inner loop<br>
     * L3 prepare the loop condition<br>
     * evaluate loop and conditionally GOTO L2<br>
     * after the loop<br>
     * Therefore, there are essentially 4 basic blocks.
     */
    Assert.assertEquals(4, basicBlocks.length);
    Assert.assertEquals(4,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._successors.size()).sum());
    Assert.assertEquals(4,
                        Arrays.stream(basicBlocks).mapToInt(bb -> bb._predecessors.size()).sum());
    // make sure the boundaries are defined properly
    long validatedStart =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._start).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(4, validatedStart);
    long validatedEnd =
        Arrays.stream(basicBlocks).mapToInt(bb -> bb._end).filter(s -> s != -1).distinct().count();
    Assert.assertEquals(4, validatedEnd);
  }
  
  @Test
  public void testUnbalancedConditional() throws Throwable {
    BasicBlock[] basicBlocks = analyzer(CFGTestClasses.UnbalancedConditional.class, "go", int.class, int.class);
    Assert.assertEquals(3, basicBlocks.length);
  }
  
  @Test
  public void testMultipleElseBranches() throws Throwable {
    BasicBlock[] basicBlocks = analyzer(CFGTestClasses.MultipleElseBranches.class, "go", Object.class, int.class);
    // 1 before the conditionals, 1 before the second conditional, 1 after the conditionals, 3
    // for the conditional branches
    Assert.assertEquals(6, basicBlocks.length);
    // the last basic blocks has three predecessors (the 3 branches)
    List<BasicBlock> sortedBlocks = Arrays.stream(basicBlocks).sorted(new Comparator<BasicBlock>(){
      @Override
      public int compare(BasicBlock o1, BasicBlock o2) {
        return o1._predecessors.size() - o2._predecessors.size();
      }}).collect(Collectors.toList());
    
    Assert.assertEquals(0, sortedBlocks.get(0)._predecessors.size());
    for(int i =1; i<5; i++)
      Assert.assertEquals(1, sortedBlocks.get(i)._predecessors.size());
    Assert.assertEquals(3, sortedBlocks.get(5)._predecessors.size());
  }
}
