/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;

import aua.analysis.AbstractOperatorAnalysis;

public class testCFGGraphical {
  
  private
      Object[]
      analyzer(Class<?> clz, String methodName, Class<?> returnType, Class<?>... args)
                                                                                      throws FileNotFoundException,
                                                                                      IOException
  {
    return new AbstractOperatorAnalysis() {
      protected Analyzer getAnalyzer(Interpreter interpreter) {
        return new CFGAnalyzer(interpreter);
      }
      
      // a little trick from functional programming here to increase the visibility of charge()
      // and being able to call it without having a concrete type!
      public Object[] charge() throws IOException {
        super.charge(clz.getName());
        printInstructions(clz.getName());
        MethodNode algMethod = super.loadMethod(methodName, super.constructMethodDescriptor(returnType, args));
        Frame[] f = super.computeStackMapFrames(algMethod);
        return new Object[] { algMethod.instructions.toArray(),
                             f };
      }
      
    }.charge();
  }
  
  private void cleanup(String name){
    File f = new File("test-output/com.ohua.tests.lang.testCFGGraphical/" + name);
    f.delete();
    f.mkdirs();
  }
  
  @Test
  public void testSimpleBasicBlock() throws Throwable {
    cleanup("testSimpleBasicBlock");
    Object[] o = analyzer(CFGTestClasses.SimpleBasicBlock.class, "go", Object.class, int.class);
    CFGVisualizer.print((AbstractInsnNode[]) o[0], (Frame[]) o[1], "test-output/com.ohua.tests.lang.testCFGGraphical/testSimpleBasicBlock/SimpleBasicBlock_go");
  }
  
  @Test
  public void testSimpleBranching() throws Throwable {
    cleanup("testSimpleBranching");
    Object[] o = analyzer(CFGTestClasses.SimpleBranching.class, "go", Object.class, int.class);
    CFGVisualizer.print((AbstractInsnNode[]) o[0], (Frame[]) o[1], "test-output/com.ohua.tests.lang.testCFGGraphical/testSimpleBranching/SimpleBranching_go");
  }
  
  @Test
  public void testSimpleLoop() throws Throwable {
    cleanup("testSimpleLoop");
    Object[] o = analyzer(CFGTestClasses.SimpleLoop.class, "go", Object.class, int.class);
    CFGVisualizer.print((AbstractInsnNode[]) o[0], (Frame[]) o[1], "test-output/com.ohua.tests.lang.testCFGGraphical/testSimpleLoop/SimpleLoop_go");
  }
}
