/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.cfg;

public abstract class CFGTestClasses {
  
  public static class SimpleBasicBlock {
    public Object go(int input) {
      int s = 2 + input;
      return s;
    }
  }
  
  /**
   * The function should resolve into 4 basic blocks.
   */
  public static class SimpleBranching {
    public Object go(int input) {
      int s = 2 + input;
      if(s < 100) s = 10;
      else s = 100;
      return s;
    }
  }
  
  public static class SimpleLoop {
    public Object go(int input) {
      int s = 2 + input;
      while(s < 100) {
        s++;
      }
      return s;
    }
  }
  
  public static class UnbalancedConditional {
    public int go(int input) {
      int s = 2 + input;
      if(s < 100) {
        s++;
      }
      // loads "s" in the final basic block
      return s;
    }
  }
  
  public static class MultipleElseBranches {
    public Object go(int input) {
      int s = 2 + input;
      if(s < 100) s = 10;
      else if(s > 1000) s = 100;
      else s = 20;
      return s;
    }
  }
  
}
