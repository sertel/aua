/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.soot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import soot.ArrayType;
import soot.Local;
import soot.PackManager;
import soot.PatchingChain;
import soot.PointsToAnalysis;
import soot.PointsToSet;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import soot.options.Options;
import soot.util.Chain;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;

public class SootReferenceAnalysis {
  
  public void initialize() {
    // FIXME this seems to be os dependent and therefore really needs to be more elaborate.
    // instead of checking the os version it would be better to locate these files via the new
    // nio mechanisms.
    StringBuffer sb = new StringBuffer();
    sb.append(System.getProperty("java.class.path") + File.pathSeparator);
    sb.append(System.getProperty("java.home"));
    sb.append(File.separator);
    sb.append("lib");
    sb.append(File.separator);
    sb.append("rt.jar");
    sb.append(File.pathSeparator + System.getProperty("java.home") + File.separator + "lib" + File.separator
              + "jce.jar");
    
    // configure soot (it tries to do something sophisticated in Scene.getDefaultClasspath for
    // MAC OS X and fails)
    Options.v().set_soot_classpath(sb.toString());
    Options.v().set_whole_program(true); // apparently needed
    Options.v().setPhaseOption("cg.spark", "on");
//    Options.v().setPhaseOption("cg.spark", "geom-pta:true");
    Options.v().set_output_format(Options.output_format_jimple); // produce jimple
  }
  
  public Set<String> analyze(Method m) throws FileNotFoundException, IOException, LeakDetectionException {
    SootMethod goMethod = chargeSoot(m);
    List<ValueBox> containedRefs = collectElements(goMethod);
    performLeakDetection(goMethod, containedRefs);
    
    return Collections.emptySet();
  }
  
  private void
      performLeakDetection(SootMethod goMethod, List<ValueBox> containedRefs) throws LeakDetectionException
  {
    PointsToAnalysis analysis = Scene.v().getPointsToAnalysis();
    
    // make sure the top level items are not reference copies
    for(int i = 0; i < containedRefs.size() - 1; i++) {
      for(int j = 1; j < containedRefs.size(); j++) {
        PointsToSet one = analysis.reachingObjects((Local) containedRefs.get(i).getValue());
        PointsToSet two = analysis.reachingObjects((Local) containedRefs.get(j).getValue());
        boolean notDisjoint = one.hasNonEmptyIntersection(two);
        if(notDisjoint) {
          LeakDetectionException lde = new LeakDetectionException();
          lde._fieldLeaked = containedRefs.get(i).getValue().toString();
          // System.out.println("Leak detected:");
          // System.out.println(goMethod.getActiveBody());
          throw lde;
        }
      }
    }
    
    // check the reference trees
    List<List<PointsToSet>> referenceTrees = new ArrayList<>(containedRefs.size());
    for(ValueBox containedRef : containedRefs) {
      PointsToSet one = analysis.reachingObjects((Local) containedRef.getValue());
      referenceTrees.add(collect(analysis, one, containedRef.getValue().getType()));
    }
    
    for(int i = 0; i < referenceTrees.size() - 1; i++) {
      for(int j = 1; j < referenceTrees.size(); j++) {
        for(int k = 0; k < referenceTrees.size(); k++) {
          for(int l = 0; l < referenceTrees.size(); l++) {
            PointsToSet one = referenceTrees.get(i).get(k);
            PointsToSet two = referenceTrees.get(j).get(l);
            boolean notDisjoint = one.hasNonEmptyIntersection(two);
            if(notDisjoint) {
              LeakDetectionException lde = new LeakDetectionException();
              lde._fieldLeaked = containedRefs.get(i).getValue().toString();
              one.hasNonEmptyIntersection(two);
              //
              //
              //
              // System.out.println("Leak detected:");
              // System.out.println(goMethod.getActiveBody());
              throw lde;
            }
          }
        }
      }
    }
    
  }
  
  private List<PointsToSet> collect(PointsToAnalysis analysis, PointsToSet pts, Type l) {
    List<PointsToSet> result = new ArrayList<>();
    Type t = l instanceof ArrayType ? ((ArrayType) l).getElementType() : l;
    if(t instanceof RefType) {
      SootClass s = ((RefType) t).getSootClass();
      Chain<SootField> fields = s.getFields();
      for(SootField f : fields) {
        PointsToSet newPts = analysis.reachingObjects(pts, f);
        result.add(newPts);
        result.addAll(collect(analysis, newPts, f.getType()));
      }
    } else {
      // no need to compare that thing as it is a primitive type and hence immutable
    }
    return result;
  }
  
  private List<ValueBox> collectElements(SootMethod goMethod) {
    ValueBox resultArray = ((ReturnStmt) goMethod.getActiveBody().getUnits().getLast()).getOpBox();
    // walk through the body and check where the elements come from
    PatchingChain<Unit> body = goMethod.getActiveBody().getUnits();
    List<ValueBox> containedRefs = new ArrayList<>();
    for(Unit unit : body) {
      Stmt stmt = (Stmt) unit; // necessary down cast
      if(stmt.containsArrayRef() && stmt.getArrayRef().getBaseBox().getValue().equals(resultArray.getValue())
         && stmt instanceof AssignStmt)
      {
        containedRefs.add(((AssignStmt) stmt).getRightOpBox());
      }
    }
    return containedRefs;
  }
  
  private SootMethod chargeSoot(Method m) {
    // load class to be inspected into Soot
    SootClass c = Scene.v().forceResolve(m.getDeclaringClass().getName(), SootClass.BODIES);
    c.setApplicationClass();
    Scene.v().loadNecessaryClasses();
    SootMethod method = findMethod(c, m);
    List<SootMethod> entryPoints = new ArrayList<>();
    entryPoints.add(method);
    Scene.v().setEntryPoints(entryPoints);
    PackManager.v().runPacks();
    return method;
  }
  
  private SootMethod findMethod(SootClass c, Method m) {
    // convert the parameters to Soot types
    List<soot.Type> params = new ArrayList<>(m.getParameterCount());
    for(Class<?> jt : m.getParameterTypes())
      params.add(Scene.v().getSootClass(jt.getName()).getType());
    try {
      return c.getMethod(m.getName(), params);
    }
    catch(Throwable t) {
      assert false;
    }
    return null;
  }
}
