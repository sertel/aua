/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package aua.soot;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import soot.Local;
import soot.PackManager;
import soot.PatchingChain;
import soot.PointsToAnalysis;
import soot.PointsToSet;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import soot.options.Options;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.reference.ReferenceAnalysisLibTestClasses.ArrayListAdd;
import aua.analysis.reference.ReferenceAnalysisLibTestClasses.ArrayListSet;
import aua.analysis.reference.ReferenceAnalysisLibTestClasses.HashMapGet;
import aua.analysis.reference.ReferenceAnalysisLibTestClasses.HashMapPut;
import aua.analysis.reference.ReferenceAnalysisTestClasses.ContextInsensitive1;
import aua.analysis.reference.ReferenceAnalysisTestClasses.ContextInsensitive2;
import aua.analysis.reference.ReferenceAnalysisTestClasses.ContextInsensitive3;
import aua.analysis.reference.ReferenceAnalysisTestClasses.Duplicate;
import aua.analysis.reference.ReferenceAnalysisTestClasses.HiddenShared;

public class SootTests {
  
  /*
   * Example taken from the Soot survivors guide.
   */
  public static class Item {
    Object data;
  }
  
  public static class Container {
    private Item item = new Item();
    
    void setItem(Item item) {
      this.item = item;
    }
    
    Item getItem() {
      return this.item;
    }
  }
  
  public static class MyContainer {
    public Object go() {
      Container c1 = new Container();
      Item i1 = new Item();
      c1.setItem(i1);
      Container c2 = new Container();
      Item i2 = new Item();
      c2.setItem(i2);
      return new Object[] { c1,
                           c2 };
    }
  }
  
  @Test
  public void testSpark() throws Throwable {
    StringBuffer sb = new StringBuffer();
    sb.append(System.getProperty("java.class.path") + File.pathSeparator);
    sb.append(System.getProperty("java.home"));
    sb.append(File.separator);
    sb.append("lib");
    sb.append(File.separator);
    sb.append("rt.jar");
    sb.append(File.pathSeparator + System.getProperty("java.home") + File.separator + "lib" + File.separator
              + "jce.jar");
    
    // configure soot (it tries to do something sophisticated in Scene.getDefaultClasspath for
    // MAC OS X and fails)
    Options.v().set_soot_classpath(sb.toString());
    Options.v().set_whole_program(true); // apparently needed
    Options.v().setPhaseOption("cg.spark", "on");
    // Options.v().setPhaseOption("cg.spark", "geom-pta:true");
    // Options.v().setPhaseOption("cg.spark", "on");
    // cg.spark geom-pta:true
    Options.v().set_output_format(Options.output_format_jimple); // produce jimple
    
    SootClass c = Scene.v().forceResolve(MyContainer.class.getName(), SootClass.BODIES);
    c.setApplicationClass();
    Scene.v().loadNecessaryClasses();
    SootMethod method = c.getMethodByName("go");
    List<SootMethod> entryPoints = new ArrayList<>();
    entryPoints.add(method);
    Scene.v().setEntryPoints(entryPoints);
    PackManager.v().runPacks();
    
    SootMethod goMethod = c.getMethodByName("go");
    ValueBox resultArray = ((ReturnStmt) goMethod.getActiveBody().getUnits().getLast()).getOpBox();
    // System.out.println("Result array: " + resultArray.getValue());
    // walk through the body and check where the elements come from
    PatchingChain<Unit> body = goMethod.getActiveBody().getUnits();
    List<ValueBox> containedRefs = new ArrayList<>();
    for(Unit unit : body) {
      // System.out.println("Unit: " + unit.getClass());
      Stmt stmt = (Stmt) unit;
      if(stmt.containsArrayRef()) {
        // System.out.println("Array ref: " + stmt.getArrayRef());
        // System.out.println("Array ref base box: " + stmt.getArrayRef().getBaseBox());
        if(stmt.getArrayRef().getBaseBox().getValue().equals(resultArray.getValue())) {
          // System.out.println("HIT");
          if(stmt instanceof AssignStmt) {
            // System.out.println("Value: " + ((AssignStmt)
            // stmt).getRightOpBox().getValue().getClass());
            containedRefs.add(((AssignStmt) stmt).getRightOpBox());
          }
        }
      }
    }
    
    System.out.println("Found references: " + containedRefs);
    
    PointsToSet pts =
        Scene.v().getPointsToAnalysis().reachingObjects((Local) ((ReturnStmt) goMethod.getActiveBody().getUnits().getLast()).getOp());
    System.out.println(pts.getClass());
    for(int i = 0; i < containedRefs.size() - 1; i++) {
      for(int j = 1; j < containedRefs.size(); j++) {
        PointsToAnalysis analysis = Scene.v().getPointsToAnalysis();
        PointsToSet one = analysis.reachingObjects((Local) containedRefs.get(i).getValue());
        PointsToSet two = analysis.reachingObjects((Local) containedRefs.get(j).getValue());
        boolean notDisjoint = one.hasNonEmptyIntersection(two);
        if(notDisjoint) System.out.println("Leak!");
        // throw new RuntimeException("Leak detected!");
        else System.out.println("All good.");
      }
    }
  }
  
  @Test
  public void testAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(MyContainer.class.getMethod("go"));
      Assert.fail("Although the sets are disjunct the exception should appear because SPARK is context-insensitive and therefore just does not understand the different contexts to the setItem calls.");
    }
    catch(LeakDetectionException lde) {
      
    }
  }
  
  @Test
  public void testNonDisjoint() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(Duplicate.class.getMethod("go"));
      Assert.fail("Should have discovered that these references point to the same container.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test
  public void testHiddenSharedReference() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(HiddenShared.class.getMethod("go"));
      Assert.fail("Should have discovered the shared reference for i1.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test
  public void testContextInsensitive1() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(ContextInsensitive1.class.getMethod("go"));
      Assert.fail("Should have discovered the shared reference for i1.");
    }
    catch(LeakDetectionException lde) {
      // success
    }
  }
  
  @Test
  public void testContextInsensitive2() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(ContextInsensitive2.class.getMethod("go"));
      Assert.fail("There is no shared reference here! "
                  + "However, the context-insensitivity of the analysis still flags an error here because the same Container class is used. "
                  + "The function really does not matter.");
    }
    catch(LeakDetectionException lde) {
      //
    }
  }
  
  @Test
  public void testContextInsensitive3() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    try {
      analysis.analyze(ContextInsensitive3.class.getMethod("go"));
    }
    catch(LeakDetectionException lde) {
      Assert.fail("There is no shared reference here!");
    }
  }
  
  @Test
  public void testSootListAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    analysis.analyze(ArrayList.class.getMethod("add", Object.class));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }

  @Test
  public void testSootListAddAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    // did not work
    analysis.analyze(ArrayListAdd.class.getMethod("go"));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }
  
  @Test
  public void testMultiple() throws Throwable{
    for(int i=0;i<10;i++){
//      testSootHashMapPutDirectAnalysis();
    }
  }

  @Test
  public void testSootListSetAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    // did not work
    analysis.analyze(ArrayListSet.class.getMethod("go"));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }

  @Test
  public void testSootHashMapPutDirectAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    analysis.analyze(HashMap.class.getMethod("put", Object.class, Object.class));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }

  @Test
  public void testSootHashMapPutAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    // did not work
    analysis.analyze(HashMapPut.class.getMethod("go"));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }

  @Test
  public void testSootHashMapGetAnalysis() throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize();
    long start = System.currentTimeMillis();
    analysis.analyze(HashMapGet.class.getMethod("go"));
    System.out.println("Time needed" + (System.currentTimeMillis() - start));
  }
}
